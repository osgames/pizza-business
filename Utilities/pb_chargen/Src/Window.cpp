#include "window.h"
/*----------------------------------------------------------------------------------------------*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	Window *pWindow = GetPointer(hWnd);
	if(pWindow == 0)
	{
		if(iMessage == WM_CREATE)
		{
			LPCREATESTRUCT lpcs;
			lpcs = (LPCREATESTRUCT)lParam;
			pWindow = (Window *)lpcs->lpCreateParams;
			SetPointer(hWnd, pWindow);
			return pWindow->WndProc(iMessage, wParam, lParam);
		}
		else
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
		}
	else
		return pWindow->WndProc(iMessage, wParam, lParam);
}
/*----------------------------------------------------------------------------------------------*/