#include "main.h"
/*----------------------------------------------------------------------------------------------*/
HINSTANCE Main::hInstance = 0;
HINSTANCE Main::hPrevInstance = 0;
int Main::nCmdShow = 0;
/*----------------------------------------------------------------------------------------------*/
int Main::MessageLoop()
{
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}
/*----------------------------------------------------------------------------------------------*/