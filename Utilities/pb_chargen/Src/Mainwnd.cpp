#include "mainwnd.h"
#include <commctrl.h>
#include <stdio.h>
/*----------------------------------------------------------------------------------------------*/
#define IDC_MAINWND_TABCTRL	101
#define IDC_MAINWND_SAVE	201
#define IDC_MAINWND_QUIT	202
/*----------------------------------------------------------------------------------------------*/
#define ID_PAGE_FIRSTNAME	0
#define ID_PAGE_LASTNAME	1
#define ID_PAGE_COMPETENCY	2
#define ID_PAGE_AGE			3
#define ID_PAGE_DESC		4
#define ID_PAGE_CHAIR		5
#define ID_PAGE_OVEN		6
#define ID_PAGE_TABLE		7
#define ID_PAGE_AD			8
#define ID_PAGE_INGREDIENT	9
#define ID_PAGE_RECIPE		10
#define ID_PAGE_OPTIONS		11
#define ID_PAGE_ABOUT		12
/*----------------------------------------------------------------------------------------------*/
MainWindow::MainWindow(BOOL sizeable) : hwndCurPage(NULL)
{
	InitCommonControls();

	DWORD dwStyle;
	if(sizeable == TRUE)
		dwStyle = WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_CLIPCHILDREN|WS_SIZEBOX;
	else
		dwStyle = WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_CLIPCHILDREN;

	hWnd = CreateWindow(szClassName, szClassName,
		dwStyle, 
		//CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
		CW_USEDEFAULT, 0, 535, 405,
		NULL, NULL, Main::hInstance, (LPSTR)this);
	
	if(!hWnd)
		exit(FALSE);

	GetPrivateProfileString("Options", "savefile", "entity.pbd", this->filename, MAX_PATH, "editor.ini");

	char text[2048];
	sprintf(text, "Pizza Business Entity Editor: \"%s\"", filename);
	SetWindowText(hWnd, text);
	
	// create the font for the window
	hWindowFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	// create the tab control
	hwndTab = CreateWindowEx(NULL, WC_TABCONTROL, "Catagories",
		WS_VISIBLE|WS_CHILD|TCS_FIXEDWIDTH,
		0, 0, 10, 10,
		hWnd, (HMENU)IDC_MAINWND_TABCTRL, Main::hInstance, NULL);

	hwndBtnSave = CreateWindow("button", "Save",
		WS_VISIBLE|WS_CHILD|BS_PUSHBUTTON,
		0, 0, 10, 10,
		hWnd, (HMENU)IDC_MAINWND_SAVE, Main::hInstance, NULL);

	hwndBtnQuit = CreateWindow("button", "Quit",
		WS_VISIBLE|WS_CHILD|BS_PUSHBUTTON,
		0, 0, 10, 10,
		hWnd, (HMENU)IDC_MAINWND_QUIT, Main::hInstance, NULL);

	SendMessage(hwndTab, WM_SETFONT, (WPARAM)hWindowFont, MAKELPARAM(TRUE, 0));
	SendMessage(hwndBtnSave, WM_SETFONT, (WPARAM)hWindowFont, MAKELPARAM(TRUE, 0));
	SendMessage(hwndBtnQuit, WM_SETFONT, (WPARAM)hWindowFont, MAKELPARAM(TRUE, 0));

	// add the catagories (buttons) to tab control
	TCITEM tci;
	ZeroMemory(&tci, sizeof(TCITEM));
	tci.mask = TCIF_TEXT;
	tci.pszText = "Firstnames";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_FIRSTNAME, &tci);
	tci.pszText = "Lastnames";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_LASTNAME, &tci);
	tci.pszText = "Competency";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_COMPETENCY, &tci);
	tci.pszText = "Age";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_AGE, &tci);
	tci.pszText = "Descriptions";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_DESC, &tci);
	tci.pszText = "Chairs";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_CHAIR, &tci);
	tci.pszText = "Ovens";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_OVEN, &tci);
	tci.pszText = "Tables";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_TABLE, &tci);
	tci.pszText = "Ingredients";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_INGREDIENT, &tci);
	tci.pszText = "Advertisements";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_AD, &tci);
	tci.pszText = "Recipes";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_RECIPE, &tci);
	tci.pszText = "Options";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_OPTIONS, &tci);
	tci.pszText = "About";
	TabCtrl_InsertItem(hwndTab, ID_PAGE_ABOUT, &tci);
	
	// setup the pb data structure
	pbd.Header.app_version = 0.95;
	pbd.Header.format_version = 0.1;

	pbd.Age.min = 0;
	pbd.Age.max = 0;

	pbd.Competency.min = 0;
	pbd.Competency.max = 0;

	pbd.Chair.price = 0,0;

	Entities_LoadFile(this->filename, this->pbd);
	
	// show the first page
	int firstpage = ID_PAGE_FIRSTNAME;
	TabCtrl_SetCurSel(hwndTab, firstpage);
	this->ShowTabPage(firstpage);

	// display window
	Show(Main::nCmdShow);
	Update();
}
/*----------------------------------------------------------------------------------------------*/
MainWindow::~MainWindow()
{
	if(IDYES == MessageBox(hWnd, "Save data to file?", "Save", MB_YESNO))
		Entities_SaveFile(this->filename, this->pbd);

	DeleteObject(hWindowFont);
}
/*----------------------------------------------------------------------------------------------*/
char MainWindow::szClassName[] = "pbEntities";
/*----------------------------------------------------------------------------------------------*/
void MainWindow::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case IDC_MAINWND_SAVE:
			Entities_SaveFile(this->filename, this->pbd);
			MessageBox(hWnd, "File was saved successfully.", "Message", MB_OK);
			break;

		case IDC_MAINWND_QUIT:
			SendMessage(hWnd, WM_CLOSE, NULL, NULL);
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::OnClose()
{
	int reply = MessageBox(hWnd, "Save data to file?", "Save", MB_YESNOCANCEL);
	switch(reply)
	{
		case IDYES:
			Entities_SaveFile(this->filename, this->pbd);
			DestroyWindow(hWnd);
			break;
		case IDNO:
			DestroyWindow(hWnd);
			break;
		default:
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::OnNotify(WPARAM wParam, LPARAM lParam)
{
	LPNMHDR lphdr = (LPNMHDR) lParam;

    switch (lphdr->code) 
	{ 
        case TCN_SELCHANGE:
			{
				int iPage = TabCtrl_GetCurSel(hwndTab);
				this->ShowTabPage(iPage);
            } 
            break; 
    }  
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::OnPaint()
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SetMapMode(hdc, MM_TEXT);

	EndPaint(hWnd, &ps);
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::OnSize(WPARAM wParam, LPARAM lParam)
{
	int width = LOWORD(lParam);
	int height = HIWORD(lParam);

	DWORD dwDlgBase = GetDialogBaseUnits();
	int cxMargin = LOWORD(dwDlgBase) / 4; 
    int cyMargin = HIWORD(dwDlgBase) / 8; 

	// resize the tab control
	MoveWindow(hwndTab, cxMargin, cyMargin, width-cxMargin*2, height-45, TRUE);
	this->ResizeTabPage();

	// adjust the position of buttons
	const int button_width = 80;
	MoveWindow(hwndBtnQuit, width-(button_width+5), height-30, button_width, 25, TRUE);
	MoveWindow(hwndBtnSave, width-(button_width+5)-(button_width+10), height-30, button_width, 25, TRUE);
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::Register()
{
	WNDCLASS wndclass;
	wndclass.style	       = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc   = ::WndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = sizeof(MainWindow *);
	wndclass.hInstance	   = Main::hInstance;
	wndclass.hIcon         = LoadIcon(Main::hInstance, "APPICON");
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	wndclass.lpszMenuName   = NULL;
	wndclass.lpszClassName = szClassName;
	if(!RegisterClass(&wndclass))
		exit(FALSE);
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::ResizeTabPage()
{
	if(hwndCurPage == NULL)
		return;

	DWORD dwDlgBase = GetDialogBaseUnits();
	int cxMargin = LOWORD(dwDlgBase) / 4; 
    int cyMargin = HIWORD(dwDlgBase) / 8; 

	// Calculate the display rectangle
	RECT rcTab;
    GetWindowRect(hwndCurPage, &rcTab);
	TabCtrl_AdjustRect(hwndTab, TRUE, &rcTab); 
    OffsetRect(&rcTab, cxMargin - rcTab.left, cyMargin - rcTab.top);
    TabCtrl_AdjustRect(hwndTab, FALSE, &rcTab);
	MoveWindow(hwndCurPage, rcTab.left, rcTab.top, 
		rcTab.right-rcTab.left, rcTab.bottom-rcTab.top, TRUE);
}
/*----------------------------------------------------------------------------------------------*/
void MainWindow::ShowTabPage(int iPage)
{
	// destroy the current page window
	if(hwndCurPage)
	{
		DestroyWindow(hwndCurPage);
		hwndCurPage = NULL;
	}

	switch(iPage)
	{
		case ID_PAGE_FIRSTNAME:	// firstname page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_NAME", hwndTab, 
				FirstNameProc, (LPARAM) &pbd.FirstnameList);
			break;
		case ID_PAGE_LASTNAME:	// lastname page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_NAME", hwndTab,
				FirstNameProc, (LPARAM) &pbd.LastnameList);
			break;
		case ID_PAGE_COMPETENCY:	// compentency page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_MINMAX", hwndTab,
				MinMaxProc, (LPARAM) &pbd.Competency);
			break;
		case ID_PAGE_AGE: // age page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_MINMAX", hwndTab,
				MinMaxProc, (LPARAM) &pbd.Age);
			break;
		case ID_PAGE_DESC:	// description page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_DESC", hwndTab,
				DescriptionProc, (LPARAM) &pbd.DescList);
			break;
		case ID_PAGE_CHAIR: // age page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_CHAIR", hwndTab,
				ChairProc, (LPARAM) &pbd.Chair);
			break;
		case ID_PAGE_OVEN:	// oven page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_OVEN", hwndTab,
				OvenProc, (LPARAM) &pbd.OvenList);
			break;
		case ID_PAGE_TABLE:	// description page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_TABLE", hwndTab,
				TableProc, (LPARAM) &pbd.TableList);
			break;
		case ID_PAGE_AD:
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_ADS", hwndTab,
				AdvertisementProc, (LPARAM) &pbd.AdCatList);
			break;
		case ID_PAGE_INGREDIENT:
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_INGREDS", hwndTab,
				IngredientProc, (LPARAM)&pbd.IngCatList);
			break;
		case ID_PAGE_RECIPE:	// recipes page
			{
				RecipeDlgData *pDlgData = new RecipeDlgData;
				pDlgData->pIngCatList = &pbd.IngCatList;
				pDlgData->pRecipeList = &pbd.RecipeList;
				hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_RECIPES", hwndTab,
					RecipesProc, (LPARAM)pDlgData);
			}
			break;
		case ID_PAGE_OPTIONS:	// options page
			hwndCurPage = CreateDialogParam(Main::hInstance, "DLG_OPTIONS", hwndTab,
				OptionsProc, (LPARAM)this->filename);
			break;
		case ID_PAGE_ABOUT:	// about page
			hwndCurPage = CreateDialog(Main::hInstance, "DLG_ABOUT", hwndTab,
				AboutProc);
			break;
	}

	this->ResizeTabPage();
}
/*----------------------------------------------------------------------------------------------*/
long MainWindow::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

		case WM_NOTIFY:
			this->OnNotify(wParam, lParam);
			break;

		case WM_PAINT:
			this->OnPaint();
			break;

		case WM_SIZE:
			this->OnSize(wParam, lParam);
			break;

		case WM_CLOSE:
			this->OnClose();
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/