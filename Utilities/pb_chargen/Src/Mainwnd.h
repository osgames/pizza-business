#ifndef _JS_MAINWND_H
#define _JS_MAINWND_H
/*----------------------------------------------------------------------------------------------*/
#include <windows.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------------------------*/
#include "main.h"
#include "window.h"
#include "wndprocs.h"
#include "data.h"
/*----------------------------------------------------------------------------------------------*/
class MainWindow : public Window
{
public:
	MainWindow(BOOL sizeable);
	~MainWindow();

public:
	static void Register();
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);
	
private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnClose();
	void OnNotify(WPARAM wParam, LPARAM lParam);
	void OnPaint();
	void OnSize(WPARAM wParam, LPARAM lParam);

private:
	void ResizeTabPage();
	void ShowTabPage(int iPage);

private:
	static char szClassName[41];
	char filename[MAX_PATH];

	HWND hwndTab;
	HWND hwndCurPage;
	HWND hwndBtnSave;
	HWND hwndBtnQuit;

	HFONT hWindowFont;

	PBD_DATA pbd;
};
/*----------------------------------------------------------------------------------------------*/
#endif