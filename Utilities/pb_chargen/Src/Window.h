#ifndef _JS_WINDOW_H
#define _JS_WINDOW_H
/*----------------------------------------------------------------------------------------------*/
#include <windows.h>
/*----------------------------------------------------------------------------------------------*/
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
/*----------------------------------------------------------------------------------------------*/
class Window
{
public:
	// window functions
	HWND GetHandle() { return hWnd; }
	BOOL Show(int nCmdShow) { return ShowWindow(hWnd, nCmdShow); }
	void Update() { UpdateWindow(hWnd); }
	virtual long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam) = 0;
protected:
	HWND hWnd;
};
/*----------------------------------------------------------------------------------------------*/
inline Window *GetPointer( HWND hWnd )
{
	 return (Window *) GetWindowLong( hWnd, 0 );
}
inline void SetPointer( HWND hWnd, Window *pWindow )
{
	 SetWindowLong( hWnd, 0, (LONG) pWindow );
}
/*----------------------------------------------------------------------------------------------*/
#endif
