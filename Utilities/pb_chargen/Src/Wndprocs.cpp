#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <string.h>
#include "wndprocs.h"
#include "main.h"
/*----------------------------------------------------------------------------------------------*/
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
#define IDC_FNAMES_LIST			101
#define IDC_FNAMES_EDITBOX		102
#define IDC_FNAMES_ADD			103
#define IDC_FNAMES_EDIT			104
#define IDC_FNAMES_DELETE		105

#define IDC_MINMAX_TEXT			101
#define IDC_MINMAX_MIN			103
#define IDC_MINMAX_UDMIN		104
#define IDC_MINMAX_MAX			106
#define IDC_MINMAX_UDMAX		107

#define IDC_DESC_LIST			101
#define IDC_DESC_EDITBOX		102
#define IDC_DESC_ADD			103
#define IDC_DESC_EDIT			104
#define IDC_DESC_DELETE			105
#define IDC_DESC_FACTOR			106	
#define IDC_DESC_UPDOWN			107

#define IDC_TABLE_LIST			101
#define IDC_TABLE_NAME			102
#define IDC_TABLE_PRICE			103
#define IDC_TABLE_CHAIRS		104
#define IDC_TABLE_ADD			105
#define IDC_TABLE_EDIT			106
#define IDC_TABLE_DELETE		107

#define IDC_OVEN_LIST			101
#define IDC_OVEN_ADD			102
#define IDC_OVEN_EDIT			103
#define IDC_OVEN_DELETE			104
#define IDC_OVEN_NAME			105
#define IDC_OVEN_PRICE			106
#define IDC_OVEN_PIZZAS			107
#define IDC_OVEN_TIME			109

#define IDC_OPT_FILENAME		101
#define IDC_OPT_NAMECHANGE		102

#define IDC_CHAIR_PRICE			101

#define IDC_ADS_LIST			101
#define IDC_ADS_ITEM_NEW		102
#define IDC_ADS_ITEM_EDIT		103
#define IDC_ADS_ITEM_DELETE		104
#define IDC_ADS_CAT_NAME		105
#define IDC_ADS_CAT_DELETE		106
#define IDC_ADS_CAT_EDIT		107
#define IDC_ADS_CAT_ADD			108

#define IDC_AD_NAME				101
#define IDC_AD_PRICE			103
#define IDC_AD_FACTOR			105

#define IDC_INGS_LIST			101
#define IDC_INGS_ITEM_NEW		102
#define IDC_INGS_ITEM_EDIT		103
#define IDC_INGS_ITEM_DELETE	104
#define IDC_INGS_CAT_NAME		105
#define IDC_INGS_CAT_DELETE		106
#define IDC_INGS_CAT_EDIT		107
#define IDC_INGS_CAT_ADD		108

#define IDC_ING_NAME			101
#define IDC_ING_PRICE			102
#define IDC_ING_QUALITY			105
#define IDC_ING_WEIGHT			107

#define IDC_RECIPES_LIST		101
#define IDC_RECIPES_ADD			102
#define IDC_RECIPES_EDIT		103
#define IDC_RECIPES_DELETE		104

#define IDC_RECIPE_LIST			101
#define IDC_RECIPE_INGREDIENTS	102
#define IDC_RECIPE_NAME			103
#define IDC_RECIPE_ADD_ING		105
#define IDC_RECIPE_REM_ING		107
/*----------------------------------------------------------------------------------------------*/
// function stolen from msdn library
HTREEITEM AddItemToTree(HWND hwndTV, LPSTR lpszItem, int nLevel, LPARAM lParam, HTREEITEM hParent = NULL)
{ 
    TVITEM tvi; 
    TVINSERTSTRUCT tvins; 
    static HTREEITEM hPrev = (HTREEITEM) TVI_FIRST; 
    static HTREEITEM hPrevRootItem = NULL; 
    static HTREEITEM hPrevLev2Item = NULL; 
    HTREEITEM hti; 
 
    tvi.mask = TVIF_TEXT | TVIF_PARAM; 
 
    // Set the text of the item.
	//if(lpszItem == NULL)
	//{
		tvi.pszText = LPSTR_TEXTCALLBACK;
		tvi.cchTextMax = 0;
	//}
	//else
	//{
	//	tvi.pszText = lpszItem; 
	//	tvi.cchTextMax = lstrlen(lpszItem); 
	//}
 
 
    // Save the heading level in the item's application-defined 
    // data area. 
    tvi.lParam = lParam; 
 
    tvins.item = tvi; 
    tvins.hInsertAfter = hPrev; 
 
    // Set the parent item based on the specified level. 
	if(hParent == NULL)
	{
		if (nLevel == 1) 
			tvins.hParent = TVI_ROOT; 
		else if (nLevel == 2) 
			tvins.hParent = hPrevRootItem; 
		else 
			tvins.hParent = hPrevLev2Item; 
	}
	else
		tvins.hParent = hParent;
 
    // Add the item to the tree view control. 
    hPrev = (HTREEITEM) SendMessage(hwndTV, TVM_INSERTITEM, 0, 
         (LPARAM) (LPTVINSERTSTRUCT) &tvins); 
 
    // Save the handle to the item. 
    if (nLevel == 1) 
        hPrevRootItem = hPrev; 
    else if (nLevel == 2) 
        hPrevLev2Item = hPrev; 
 
    return hPrev; 
} 
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK AboutProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{

			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{

			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK AdProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				AdData *pData = (AdData*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pData);

				char price[8];
				sprintf(price, "%.2f", pData->price);

				SetDlgItemText(hDlg, IDC_AD_NAME, pData->name);
				SetDlgItemText(hDlg, IDC_AD_PRICE, price);
				SetDlgItemInt(hDlg, IDC_AD_FACTOR, pData->factor, TRUE);
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case 1:
					{
						AdData *pData = (AdData*)GetWindowLong(hDlg, DWL_USER);
						// set the price
						char price[8];
						GetDlgItemText(hDlg, IDC_AD_PRICE, price, 7);
						pData->price = atof(price);
						// set the name
						GetDlgItemText(hDlg, IDC_AD_NAME, pData->name, 255);
						// set the factor
						pData->factor = GetDlgItemInt(hDlg, IDC_AD_FACTOR, FALSE, TRUE);

						EndDialog(hDlg, TRUE);
					}
					break;
				case 2:
					EndDialog(hDlg, FALSE);
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK AdvertisementProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == 0) break;

				List<AdCategory> *pAdCats = (List<AdCategory>*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pAdCats);

				// add the items to the treeview control
				for(unsigned int c = 1 ; c <= pAdCats->GetCount() ; c++)
				{
					AdCategory *pCat = pAdCats->GetItem(c);
					AddItemToTree(GetDlgItem(hDlg, IDC_ADS_LIST), pCat->name, 1, (LPARAM)pCat);
					for(unsigned int i = 1 ; i <= pCat->Ads.GetCount() ; i++)
					{
						AdData *pData = pCat->Ads.GetItem(i);

						AddItemToTree(GetDlgItem(hDlg, IDC_ADS_LIST), NULL, 2, (LPARAM)pData);
					}	
				}

				// set the focus
				SetFocus(GetDlgItem(hDlg, IDC_ADS_CAT_NAME));
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_ADS_CAT_ADD:
					{
						char text[256];
						GetDlgItemText(hDlg, IDC_ADS_CAT_NAME, text, 255);
						if(strlen(text) < 1)
							break;

						AdCategory *pCat = new AdCategory;
						strcpy(pCat->name, text);
						
						// add category to list
						List<AdCategory> *pAdCats = (List<AdCategory>*)GetWindowLong(hDlg, DWL_USER);
						pAdCats->AddItem(pCat);

						// add item to treeview control
						AddItemToTree(GetDlgItem(hDlg, IDC_ADS_LIST), NULL, 1, (LPARAM)pCat);

						// clear edit box; set focus
						SetDlgItemText(hDlg, IDC_ADS_CAT_NAME, "");
						SetFocus(GetDlgItem(hDlg, IDC_ADS_CAT_NAME));
					}
					break;

				case IDC_ADS_CAT_EDIT:
					{
						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(GetDlgItem(hDlg, IDC_ADS_LIST));
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_ADS_LIST), &tvi);

						AdCategory *pCat = (AdCategory*)tvi.lParam;

						char text[256];
						GetDlgItemText(hDlg, IDC_ADS_CAT_NAME, text, 255);

						if(strlen(text) > 0)
							strcpy(pCat->name, text);

						InvalidateRect(GetDlgItem(hDlg, IDC_ADS_LIST), NULL, FALSE);
					}
					break;

				case IDC_ADS_CAT_DELETE:
					{
						HWND hwndTV = GetDlgItem(hDlg, IDC_ADS_LIST);

						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(hwndTV);
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(hwndTV, &tvi);

						AdCategory *pCat = (AdCategory*)tvi.lParam;
						List<AdCategory> *pAdCats = (List<AdCategory>*)GetWindowLong(hDlg, DWL_USER);

						// find the item; delete it from the list
						for(unsigned int c = 1 ; c <= pAdCats->GetCount() ; c++)
						{
							if(pCat == pAdCats->GetItem(c))
							{
								pAdCats->DeleteItem(c);
								break;
							}
						}

						// delete the item from the treeview control
						TreeView_DeleteItem(hwndTV, tvi.hItem);

						// disable all buttons (except add) if no items exist
						if(TreeView_GetCount(hwndTV) < 1)
						{
							EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_NEW), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_EDIT), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_DELETE), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_ADS_CAT_EDIT), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_ADS_CAT_DELETE), FALSE);
						}
					}
					break;

				case IDC_ADS_ITEM_NEW:
					{
						List<AdCategory> *pAdCats = (List<AdCategory>*)GetWindowLong(hDlg, DWL_USER);

						AdData *pNewAd = new AdData;
						strcpy(pNewAd->name, "");
						pNewAd->price = 0.0f;
						pNewAd->factor = 0;
						pNewAd->ad_id = GetNextAvailableAdID(pAdCats);

						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(GetDlgItem(hDlg, IDC_ADS_LIST));
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_ADS_LIST), &tvi);
						

						int result = DialogBoxParam(Main::hInstance, "DLG_AD", hDlg, AdProc, (LPARAM) pNewAd);
						// dialogbox should return TRUE if ok; FALSE if cancel
						if(result == TRUE)
						{
							// add item to the list
							AdCategory *pCat = (AdCategory*)tvi.lParam;
							pCat->Ads.AddItem(pNewAd);
							
							AddItemToTree(GetDlgItem(hDlg, IDC_ADS_LIST), NULL, 2, (LPARAM)pNewAd, tvi.hItem);
							InvalidateRect(GetDlgItem(hDlg, IDC_ADS_LIST), NULL, FALSE);
						}
						else
						{
							delete pNewAd;
						}
					}
					break;

				case IDC_ADS_ITEM_EDIT:
					{
						// just pass the AdData object pointer to the dialog proc
						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(GetDlgItem(hDlg, IDC_ADS_LIST));
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_ADS_LIST), &tvi);
						

						int result = DialogBoxParam(Main::hInstance, "DLG_AD", hDlg, AdProc, (LPARAM) tvi.lParam);
						InvalidateRect(GetDlgItem(hDlg, IDC_ADS_LIST), NULL, FALSE);
					}
					break;

				case IDC_ADS_ITEM_DELETE:
					{
						HWND hwndTV = GetDlgItem(hDlg, IDC_ADS_LIST);
						
						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(hwndTV);
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_ADS_LIST), &tvi);
						
						TVITEM tvi_parent;
						tvi_parent.hItem = TreeView_GetParent(hwndTV, tvi.hItem);
						tvi_parent.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_ADS_LIST), &tvi_parent);

						// find and delete item in the list
						AdCategory *pCat = (AdCategory *)tvi_parent.lParam;
						for(unsigned int c = 1 ; pCat->Ads.GetCount() ; c++)
						{
							AdData *pData = pCat->Ads.GetItem(c);
							if(pData == (AdData *)tvi.lParam)
							{
								pCat->Ads.DeleteItem(c);
								break;
							}
						}

						// delete item from treeview control
						TreeView_DeleteItem(hwndTV, tvi.hItem);
					}
					break;
			}
			break;

		case WM_NOTIFY:
			{
				switch (((LPNMHDR) lParam)->code) 
				{
					case TVN_GETDISPINFO:
						{
							LPNMTVDISPINFO info = (LPNMTVDISPINFO) lParam;

							if(info->item.mask & TVIF_TEXT)
							{
								if(TreeView_GetParent(GetDlgItem(hDlg, IDC_ADS_LIST), info->item.hItem) == NULL)
								{
									AdCategory *pCat = (AdCategory*)info->item.lParam;
									strcpy(info->item.pszText, pCat->name);
								}
								else
								{
									AdData *pData = (AdData*)info->item.lParam;
									sprintf(info->item.pszText, "%s (%d)", pData->name, pData->ad_id);
								}
							}
						}
						break;

					case TVN_SELCHANGED:
						{
							LPNMTREEVIEW pnmtv = (LPNMTREEVIEW) lParam;

							// set the editbox text
							if(TreeView_GetParent(GetDlgItem(hDlg, IDC_ADS_LIST), pnmtv->itemNew.hItem) == NULL)
							{
								// a catagory was selected
								AdCategory *pCat = (AdCategory*)pnmtv->itemNew.lParam;
								SetDlgItemText(hDlg, IDC_ADS_CAT_NAME, pCat->name);

								// enable/disable buttons
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_NEW), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_EDIT), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_DELETE), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_CAT_EDIT), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_CAT_DELETE), TRUE);
							}
							else
							{
								// a catagory item was selected

								// enable/disable buttons
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_NEW), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_EDIT), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_ITEM_DELETE), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_CAT_EDIT), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_ADS_CAT_DELETE), FALSE);
							}
						}
						break;

				}
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK ChairProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				ChairPrice *pChair = (ChairPrice*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pChair);

				char text[256];
				sprintf(text, "%0.2f", pChair->price);
				SetDlgItemText(hDlg, IDC_CHAIR_PRICE, text);
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_CHAIR_PRICE:
					{
						if(HIWORD(wParam) == EN_CHANGE)
						{
							char text[256];
							GetDlgItemText(hDlg, IDC_CHAIR_PRICE, text, 255);

							ChairPrice *pChair = (ChairPrice*)GetWindowLong(hDlg, DWL_USER);
							pChair->price = atof(text);
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK DescriptionProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				SendDlgItemMessage(hDlg, IDC_DESC_UPDOWN, UDM_SETRANGE, NULL, MAKELONG(10, -10));

				if(lParam == NULL) break;

				List<CharDesc> *pDescList = (List<CharDesc> *)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pDescList);

				// add each item to the listbox
				for(unsigned int c = 1 ; c <= pDescList->GetCount() ; c++)
				{
					char text[1048];
					CharDesc *pItem = pDescList->GetItem(c);
					
					sprintf(text, "%d - %s", pItem->factor, pItem->name);

					int index = SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)text);
					SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_SETITEMDATA, index, (LPARAM)pItem);
				}

				SetFocus(GetDlgItem(hDlg, IDC_DESC_EDITBOX));
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_DESC_LIST:
					{
						if(HIWORD(wParam) == LBN_SELCHANGE)
						{
							// get the selected index
							int index = SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_GETCURSEL, 0, 0);
							if(index == LB_ERR)
								break;
							// get the item data of the selected item
							CharDesc *pItem = (CharDesc*)SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_GETITEMDATA, 
								index, NULL);
							// set the editbox text
							SetDlgItemText(hDlg, IDC_DESC_EDITBOX, pItem->name);
							SetDlgItemInt(hDlg, IDC_DESC_FACTOR, pItem->factor, TRUE);
						}
					}
					break;

				case IDC_DESC_ADD:
					{
						// get text/numbers from editboxes
						char desc_text[1024], text[1048];
						GetDlgItemText(hDlg, IDC_DESC_EDITBOX, desc_text, 1023);
						int factor = GetDlgItemInt(hDlg, IDC_DESC_FACTOR, NULL, TRUE);

						// create new item; set vars
						CharDesc *pItem = new CharDesc;
						strcpy(pItem->name, desc_text);
						pItem->factor = factor;

						// add item to list
						List<CharDesc> *pDescList = (List<CharDesc> *)GetWindowLong(hDlg, DWL_USER);
						pDescList->AddItem(pItem);

						// add item to listbox
						sprintf(text, "%d - %s", factor, desc_text);
						int index = SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)text);
						SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_SETITEMDATA, index, (LPARAM)pItem);
						SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_SETCURSEL, index, NULL);

						// reset editboxes and set focus
						SetDlgItemText(hDlg, IDC_DESC_EDITBOX, "");
						SetDlgItemInt(hDlg, IDC_DESC_FACTOR, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_DESC_EDITBOX));
					}
					break;

				case IDC_DESC_EDIT:
					{
						// get the selected index
						int index = SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						// get the item data of the selected item
						CharDesc *pItem = (CharDesc*)SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_GETITEMDATA, 
							index, NULL);

						// get text/numbers from editboxes
						char desc_text[1024], text[1048];
						GetDlgItemText(hDlg, IDC_DESC_EDITBOX, desc_text, 1023);
						int factor = GetDlgItemInt(hDlg, IDC_DESC_FACTOR, NULL, TRUE);
						sprintf(text, "%d - %s", factor, desc_text);

						// find the item in the list; replace with new data
						List<CharDesc> *pDescList = (List<CharDesc> *)GetWindowLong(hDlg, DWL_USER);
						for(unsigned int c = 1 ; c <= pDescList->GetCount() ; c++)
						{
							CharDesc *pTestItem = pDescList->GetItem(c);
							if(strcmp(pTestItem->name, pItem->name) == 0)
							{
								if(pTestItem->factor == pItem->factor)
								{
									// delete the item from list
									strcpy(pTestItem->name, desc_text);
									pTestItem->factor = factor;
									break;
								}
							}
						}

						// delete item from listbox; add new one
						SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_DELETESTRING, index, NULL);
						index = SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)text);
						SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_SETITEMDATA, index, (LPARAM)pItem);
						SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_SETCURSEL, index, NULL);


						// reset editboxes and set focus
						SetDlgItemText(hDlg, IDC_DESC_EDITBOX, "");
						SetDlgItemInt(hDlg, IDC_DESC_FACTOR, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_DESC_EDITBOX));
					}
					break;

				case IDC_DESC_DELETE:
					{
						// get the selected index
						int index = SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						// get the item data of the selected item
						CharDesc *pItem = (CharDesc*)SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_GETITEMDATA, 
							index, NULL);

						// delete the item from listbox
						SendDlgItemMessage(hDlg, IDC_DESC_LIST, LB_DELETESTRING, index, NULL);

						// find the item in the list; delete it
						List<CharDesc> *pDescList = (List<CharDesc> *)GetWindowLong(hDlg, DWL_USER);
						for(unsigned int c = 1 ; c <= pDescList->GetCount() ; c++)
						{
							CharDesc *pTestItem = pDescList->GetItem(c);
							if(strcmp(pTestItem->name, pItem->name) == 0)
							{
								if(pTestItem->factor == pItem->factor)
								{
									// delete the item from list
									pDescList->DeleteItem(c);
									break;
								}
							}
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK FirstNameProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				List<CharName> *pList = (List<CharName> *) lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pList);

				// add all the items to the list
				for(unsigned int c = 1 ; c <= pList->GetCount() ; c++)
				{
					CharName *pItem = pList->GetItem(c);

					SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_ADDSTRING, 
						NULL, (LPARAM)(LPCSTR)pItem->name);
				}

				// set focus
				SetFocus(GetDlgItem(hDlg, IDC_FNAMES_EDITBOX));
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_FNAMES_LIST:
					{
						if(HIWORD(wParam) == LBN_SELCHANGE)
						{
							// get the selected index
							int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETCURSEL, 0, 0);
							if(index == LB_ERR)
								break;
							// get the text of selected item
							char text[256];
							SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETTEXT, 
								index, (LPARAM)(LPCSTR) text);
							// set the editbox text
							SetDlgItemText(hDlg, IDC_FNAMES_EDITBOX, text);
						}
					}
					break;

				case IDC_FNAMES_ADD:
					{
						char text[256];
						GetDlgItemText(hDlg, IDC_FNAMES_EDITBOX, text, 255);

						// be sure name not already in listbox
						if(LB_ERR == SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_FINDSTRING, 
							-1, (LPARAM)(LPCSTR)text))
						{

							// create new item
							CharName *pItem = new CharName;
							strcpy(pItem->name, text);

							// add item to listbox
							int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_ADDSTRING, 
								NULL, (LPARAM)(LPCSTR)pItem->name);
							SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_SETCURSEL, index, NULL);

							// add item to list
							List<CharName> *pList = (List<CharName> *)GetWindowLong(hDlg, DWL_USER);
							pList->AddItem(pItem);

							// clear editbox
							SetDlgItemText(hDlg, IDC_FNAMES_EDITBOX, "");
						}
						else
							MessageBox(hDlg, "That name is already in the list.", "Try Again", MB_OK);

						// set focus
						SetFocus(GetDlgItem(hDlg, IDC_FNAMES_EDITBOX));
					}
					break;

				case IDC_FNAMES_EDIT:
					{
						// get the index of selected listbox item
						int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						
						// get the text
						char list_text[256], edit_text[256];
						GetDlgItemText(hDlg, IDC_FNAMES_EDITBOX, edit_text, 255);
						SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETTEXT, 
							index, (LPARAM)(LPCSTR) list_text);
						// delete selected item from listbox; add new one
						SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_DELETESTRING, 
								index, NULL);
						index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_ADDSTRING, 
								NULL, (LPARAM)(LPCSTR)edit_text);
						SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_SETCURSEL, index, NULL);
						
						// search list for name; replace with new name
						List<CharName> *pList = (List<CharName> *)GetWindowLong(hDlg, DWL_USER);
						for(unsigned int c = 1 ; c <= pList->GetCount() ; c++)
						{
							CharName *pItem = pList->GetItem(c);
							if(strcmp(pItem->name, list_text) == 0)
							{
								strcpy(pItem->name, edit_text);

								// clear editbox; set focus
								SetDlgItemText(hDlg, IDC_FNAMES_EDITBOX, "");
								SetFocus(GetDlgItem(hDlg, IDC_FNAMES_EDITBOX));
								break;
							}
						}

					}
					break;
				
				case IDC_FNAMES_DELETE:
					{
						// get the index of selected listbox item
						int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;

						// get the text
						char list_text[256];
						SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETTEXT, 
							index, (LPARAM)(LPCSTR) list_text);
	
						// delete selected item from listbox; add new one
						SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_DELETESTRING, index, NULL);

						// search list for name; delete it
						List<CharName> *pList = (List<CharName> *)GetWindowLong(hDlg, DWL_USER);
						for(unsigned int c = 1 ; c <= pList->GetCount() ; c++)
						{
							CharName *pItem = pList->GetItem(c);
							if(strcmp(pItem->name, list_text) == 0)
							{
								pList->DeleteItem(c);
								break;
							}
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK IngredProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				IngData *pData = (IngData*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pData);

				// set the name
				SetDlgItemText(hDlg, IDC_ING_NAME, pData->name);
				// set the price
				char price[256];
				sprintf(price, "%.2f", pData->price);
				SetDlgItemText(hDlg, IDC_ING_PRICE, price);
				// set the quality
				SetDlgItemInt(hDlg, IDC_ING_QUALITY, pData->quality, FALSE);
				// set the weight
				SetDlgItemInt(hDlg, IDC_ING_WEIGHT, pData->weight, FALSE);
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case 1:
					{
						IngData *pData = (IngData*)GetWindowLong(hDlg, DWL_USER);
						
						// set the name
						GetDlgItemText(hDlg, IDC_ING_NAME, pData->name, 255);
						// set the price
						char price[256];
						GetDlgItemText(hDlg, IDC_ING_PRICE, price, 255);
						pData->price = (float)atof(price);
						// set the quality
						pData->quality = GetDlgItemInt(hDlg, IDC_ING_QUALITY, NULL, FALSE);
						// set the weight
						pData->weight = GetDlgItemInt(hDlg, IDC_ING_QUALITY, NULL, FALSE);

						EndDialog(hDlg, TRUE);
					}
					break;
				case 2:
					EndDialog(hDlg, FALSE);
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK IngredientProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == 0) break;

				List<IngCategory> *pIngCats = (List<IngCategory>*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pIngCats);

				// add the items to the treeview control
				for(unsigned int c = 1 ; c <= pIngCats->GetCount() ; c++)
				{
					IngCategory *pCat = pIngCats->GetItem(c);
					AddItemToTree(GetDlgItem(hDlg, IDC_INGS_LIST), pCat->name, 1, (LPARAM)pCat);
					for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
					{
						IngData *pData = pCat->Ingredients.GetItem(i);

						AddItemToTree(GetDlgItem(hDlg, IDC_INGS_LIST), NULL, 2, (LPARAM)pData);
					}	
				}

				// set the focus
				SetFocus(GetDlgItem(hDlg, IDC_ADS_CAT_NAME));
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_INGS_CAT_ADD:
					{
						char text[256];
						GetDlgItemText(hDlg, IDC_INGS_CAT_NAME, text, 255);
						if(strlen(text) < 1)
							break;

						IngCategory *pCat = new IngCategory;
						strcpy(pCat->name, text);
						
						// add category to list
						List<IngCategory> *pIngCats = (List<IngCategory>*)GetWindowLong(hDlg, DWL_USER);
						pIngCats->AddItem(pCat);

						// add item to treeview control
						AddItemToTree(GetDlgItem(hDlg, IDC_INGS_LIST), NULL, 1, (LPARAM)pCat);

						// clear edit box; set focus
						SetDlgItemText(hDlg, IDC_INGS_CAT_NAME, "");
						SetFocus(GetDlgItem(hDlg, IDC_INGS_CAT_NAME));
					}
					break;

				case IDC_INGS_CAT_EDIT:
					{
						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(GetDlgItem(hDlg, IDC_INGS_LIST));
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_INGS_LIST), &tvi);

						IngCategory *pCat = (IngCategory*)tvi.lParam;

						char text[256];
						GetDlgItemText(hDlg, IDC_INGS_CAT_NAME, text, 255);

						if(strlen(text) > 0)
							strcpy(pCat->name, text);

						InvalidateRect(GetDlgItem(hDlg, IDC_INGS_LIST), NULL, FALSE);
					}
					break;

				case IDC_INGS_CAT_DELETE:
					{
						HWND hwndTV = GetDlgItem(hDlg, IDC_INGS_LIST);

						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(hwndTV);
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(hwndTV, &tvi);

						IngCategory *pCat = (IngCategory*)tvi.lParam;
						List<IngCategory> *pIngCats = (List<IngCategory>*)GetWindowLong(hDlg, DWL_USER);

						// find the item; delete it from the list
						for(unsigned int c = 1 ; c <= pIngCats->GetCount() ; c++)
						{
							if(pCat == pIngCats->GetItem(c))
							{
								pIngCats->DeleteItem(c);
								break;
							}
						}

						// delete the item from the treeview control
						TreeView_DeleteItem(hwndTV, tvi.hItem);

						// disable all buttons (except add) if no items exist
						if(TreeView_GetCount(hwndTV) < 1)
						{
							EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_NEW), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_EDIT), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_DELETE), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_INGS_CAT_EDIT), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_INGS_CAT_DELETE), FALSE);
						}
					}
					break;

				case IDC_INGS_ITEM_NEW:
					{
						List<IngCategory> *pIngCats = (List<IngCategory>*)GetWindowLong(hDlg, DWL_USER);

						IngData *pNewIng = new IngData;
						strcpy(pNewIng->name, "");
						pNewIng->price = 0.0f;
						pNewIng->quality = 0;
						pNewIng->weight = 0;
						pNewIng->ingredient_id = GetNextAvailableIngID(pIngCats);

						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(GetDlgItem(hDlg, IDC_INGS_LIST));
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_INGS_LIST), &tvi);
						

						int result = DialogBoxParam(Main::hInstance, "DLG_INGRED", hDlg, IngredProc, (LPARAM) pNewIng);
						// dialogbox should return TRUE if ok; FALSE if cancel
						if(result == TRUE)
						{
							// add item to the list
							IngCategory *pCat = (IngCategory*)tvi.lParam;
							pCat->Ingredients.AddItem(pNewIng);
							
							AddItemToTree(GetDlgItem(hDlg, IDC_INGS_LIST), NULL, 2, (LPARAM)pNewIng, tvi.hItem);
							InvalidateRect(GetDlgItem(hDlg, IDC_INGS_LIST), NULL, FALSE);
						}
						else
						{
							delete pNewIng;
						}
					}
					break;

				case IDC_INGS_ITEM_EDIT:
					{
						// just pass the AdData object pointer to the dialog proc
						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(GetDlgItem(hDlg, IDC_INGS_LIST));
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_INGS_LIST), &tvi);
						

						int result = DialogBoxParam(Main::hInstance, "DLG_INGRED", hDlg, IngredProc, (LPARAM) tvi.lParam);
						InvalidateRect(GetDlgItem(hDlg, IDC_INGS_LIST), NULL, FALSE);
					}
					break;

				case IDC_INGS_ITEM_DELETE:
					{
						HWND hwndTV = GetDlgItem(hDlg, IDC_INGS_LIST);
						
						TVITEM tvi;
						tvi.hItem = TreeView_GetSelection(hwndTV);
						tvi.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_INGS_LIST), &tvi);
						
						TVITEM tvi_parent;
						tvi_parent.hItem = TreeView_GetParent(hwndTV, tvi.hItem);
						tvi_parent.mask = TVIF_PARAM;
						TreeView_GetItem(GetDlgItem(hDlg, IDC_INGS_LIST), &tvi_parent);

						// find and delete item in the list
						IngCategory *pCat = (IngCategory *)tvi_parent.lParam;
						for(unsigned int c = 1 ; pCat->Ingredients.GetCount() ; c++)
						{
							IngData *pData = pCat->Ingredients.GetItem(c);
							if(pData == (IngData *)tvi.lParam)
							{
								pCat->Ingredients.DeleteItem(c);
								break;
							}
						}

						// delete item from treeview control
						TreeView_DeleteItem(hwndTV, tvi.hItem);
					}
					break;

			}
			break;

		case WM_NOTIFY:
			{
				switch (((LPNMHDR) lParam)->code) 
				{
					case TVN_GETDISPINFO:
						{
							LPNMTVDISPINFO info = (LPNMTVDISPINFO) lParam;

							if(info->item.mask & TVIF_TEXT)
							{
								if(TreeView_GetParent(GetDlgItem(hDlg, IDC_INGS_LIST), info->item.hItem) == NULL)
								{
									IngCategory *pCat = (IngCategory*)info->item.lParam;
									strcpy(info->item.pszText, pCat->name);
								}
								else
								{
									IngData *pData = (IngData*)info->item.lParam;
									sprintf(info->item.pszText, "%s (%d)", pData->name, pData->ingredient_id);
								}
							}
						}
						break;

					case TVN_SELCHANGED:
						{
							LPNMTREEVIEW pnmtv = (LPNMTREEVIEW) lParam;

							// set the editbox text
							if(TreeView_GetParent(GetDlgItem(hDlg, IDC_INGS_LIST), pnmtv->itemNew.hItem) == NULL)
							{
								// a catagory was selected
								IngCategory *pCat = (IngCategory*)pnmtv->itemNew.lParam;
								SetDlgItemText(hDlg, IDC_INGS_CAT_NAME, pCat->name);

								// enable/disable buttons
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_NEW), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_EDIT), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_DELETE), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_CAT_EDIT), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_CAT_DELETE), TRUE);
							}
							else
							{
								// a catagory item was selected

								// enable/disable buttons
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_NEW), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_EDIT), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_ITEM_DELETE), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_CAT_EDIT), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_INGS_CAT_DELETE), FALSE);
							}
						}
						break;

				}
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK OptionsProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				char *filename = (char*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)filename);

				SetDlgItemText(hDlg, IDC_OPT_FILENAME, filename);
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_OPT_NAMECHANGE:
					{
						char new_name[MAX_PATH];
						GetDlgItemText(hDlg, IDC_OPT_FILENAME, new_name, MAX_PATH-1);

						if(strlen(new_name) < 1)
						{
							MessageBox(hDlg, "Invalid filename", "Error", MB_OK);
						}
						else
						{
							char* filename = (char*)GetWindowLong(hDlg, DWL_USER);
							strcpy(filename, new_name);

							WritePrivateProfileString("Options", "savefile", filename, "editor.ini");

							char text[2048];
							sprintf(text, "Pizza Business Entity Editor: \"%s\"", filename);
							SetWindowText(GetParent(GetParent(hDlg)), text);
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK OvenProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				List<OvenData> *pOvenList = (List<OvenData>*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pOvenList);

				// add all the items to the listbox
				for(unsigned int c = 1 ; c <= pOvenList->GetCount() ; c++)
				{
					OvenData *pData = pOvenList->GetItem(c);

					char lb_text[288];
					sprintf(lb_text, "%d  |  %d  |  $%0.2f  |  %s", pData->pizza_support,
						pData->cooking_time, pData->price, pData->name);
					int index = SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_ADDSTRING, 
						NULL, (LPARAM)(LPCSTR)lb_text);
					SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_SETITEMDATA, index, (LPARAM)pData);

				}
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_OVEN_LIST:
					{
						if(HIWORD(wParam) == LBN_SELCHANGE)
						{
							// get the selected index
							int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETCURSEL, 0, 0);
							if(index == LB_ERR)
								break;
							// get the oven data
							OvenData *pData = (OvenData*)SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_GETITEMDATA,
								index, NULL);
							// set the editboxes' text
							SetDlgItemText(hDlg, IDC_OVEN_NAME, pData->name);
							SetDlgItemInt(hDlg, IDC_OVEN_PIZZAS, pData->pizza_support, FALSE);
							SetDlgItemInt(hDlg, IDC_OVEN_TIME, pData->cooking_time, FALSE);

							char text[10];
							sprintf(text, "%.2f", pData->price);
							SetDlgItemText(hDlg, IDC_OVEN_PRICE, text);
						}
					}
					break;

				case IDC_OVEN_ADD:
					{
						OvenData *pData = new OvenData;
					
						GetDlgItemText(hDlg, IDC_OVEN_NAME, pData->name, 255);

						if(strlen(pData->name) < 1)
						{
							delete pData;
							MessageBox(hDlg, "Invalid oven name", "Error", MB_OK);
							break;
						}

						pData->pizza_support = GetDlgItemInt(hDlg, IDC_OVEN_PIZZAS, NULL, FALSE);
						pData->cooking_time = GetDlgItemInt(hDlg, IDC_OVEN_TIME, NULL, FALSE);

						char text[10];
						GetDlgItemText(hDlg, IDC_OVEN_PRICE, text, 9);
						pData->price = atof(text);

						// add item to the list
						List<OvenData> *pOvenList = (List<OvenData>*)GetWindowLong(hDlg, DWL_USER);
						pOvenList->AddItem(pData);

						// add item to the listbox
						char lb_text[288];
						sprintf(lb_text, "%d  |  %d  |  $%0.2f  |  %s", pData->pizza_support,
							pData->cooking_time, pData->price, pData->name);
						int index = SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_ADDSTRING, 
							NULL, (LPARAM)(LPCSTR)lb_text);
						SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_SETITEMDATA, index, (LPARAM)pData);

						// clear the editboxes; set focus
						SetDlgItemText(hDlg, IDC_OVEN_NAME, "");
						SetDlgItemText(hDlg, IDC_OVEN_PRICE, "");
						SetDlgItemInt(hDlg, IDC_OVEN_PIZZAS, 0, FALSE);
						SetDlgItemInt(hDlg, IDC_OVEN_TIME, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_OVEN_NAME));
					}
					break;

				case IDC_OVEN_EDIT:
					{
						// get the selected index
						int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						// get the oven data
						OvenData *pData = (OvenData*)SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_GETITEMDATA,
							index, NULL);

						// modify the data
						GetDlgItemText(hDlg, IDC_OVEN_NAME, pData->name, 255);
						if(strlen(pData->name) < 1)
						{
							delete pData;
							MessageBox(hDlg, "Invalid oven name", "Error", MB_OK);
							break;
						}

						pData->pizza_support = GetDlgItemInt(hDlg, IDC_OVEN_PIZZAS, NULL, FALSE);
						pData->cooking_time = GetDlgItemInt(hDlg, IDC_OVEN_TIME, NULL, FALSE);

						char text[10];
						GetDlgItemText(hDlg, IDC_OVEN_PRICE, text, 9);
						pData->price = atof(text);

						// delete the item in the listbox
						SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_DELETESTRING, index, NULL);

						// add item to the listbox
						char lb_text[288];
						sprintf(lb_text, "%d  |  %d  |  $%0.2f  |  %s", pData->pizza_support,
							pData->cooking_time, pData->price, pData->name);
						index = SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_ADDSTRING, 
							NULL, (LPARAM)(LPCSTR)lb_text);
						SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_SETITEMDATA, index, (LPARAM)pData);
						SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_SETCURSEL, index, NULL);

						// clear the editboxes; set focus
						SetDlgItemText(hDlg, IDC_OVEN_NAME, "");
						SetDlgItemText(hDlg, IDC_OVEN_PRICE, "");
						SetDlgItemInt(hDlg, IDC_OVEN_PIZZAS, 0, FALSE);
						SetDlgItemInt(hDlg, IDC_OVEN_TIME, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_OVEN_NAME));
					}
					break;

				case IDC_OVEN_DELETE:
					{
						// get the selected index
						int index = SendDlgItemMessage(hDlg, IDC_FNAMES_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						// get the oven data
						OvenData *pData = (OvenData*)SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_GETITEMDATA,
							index, NULL);

						// delete the item in the listbox
						SendDlgItemMessage(hDlg, IDC_OVEN_LIST, LB_DELETESTRING, index, NULL);
						
						// find object in list; delete it
						List<OvenData> *pOvenList = (List<OvenData>*)GetWindowLong(hDlg, DWL_USER);
						for(unsigned int c = 1 ; c <= pOvenList->GetCount() ; c++)
						{
							if(pOvenList->GetItem(c) == pData)
							{
								pOvenList->DeleteItem(c);
								break;
							}
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK MinMaxProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				SendDlgItemMessage(hDlg, IDC_MINMAX_UDMIN, UDM_SETRANGE, NULL, MAKELONG(1000, -1000));
				SendDlgItemMessage(hDlg, IDC_MINMAX_UDMAX, UDM_SETRANGE, NULL, MAKELONG(1000, -1000));

				if(lParam == NULL) break;

				MinMax *pMinMax = (MinMax*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pMinMax);

				SetDlgItemInt(hDlg, IDC_MINMAX_MIN, pMinMax->min, FALSE);
				SetDlgItemInt(hDlg, IDC_MINMAX_MAX, pMinMax->max, FALSE);
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_MINMAX_MIN:
					{
						if(HIWORD(wParam) == EN_CHANGE)
						{
							BOOL pass;
							int new_min = GetDlgItemInt(hDlg, IDC_MINMAX_MIN, &pass, FALSE);
							MinMax *pMinMax = (MinMax*)GetWindowLong(hDlg, DWL_USER);
							if(pMinMax)
								pMinMax->min = new_min;
						}
					}
					break;
				case IDC_MINMAX_MAX:
					{
						if(HIWORD(wParam) == EN_CHANGE)
						{
							BOOL pass;
							int new_max = GetDlgItemInt(hDlg, IDC_MINMAX_MAX, &pass, FALSE);
							MinMax *pMinMax = (MinMax*)GetWindowLong(hDlg, DWL_USER);
							if(pMinMax)
								pMinMax->max = new_max;
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK PizzaBuilderProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				RecipeDataEdit *pDataEdit = (RecipeDataEdit*) lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pDataEdit);

				// add the ingredients to the combobox
				for(unsigned int c = 1 ; c <= pDataEdit->pIngCatList->GetCount() ; c++)
				{
					IngCategory *pCat = pDataEdit->pIngCatList->GetItem(c);
					for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
					{
						IngData *pData = pCat->Ingredients.GetItem(i);
						
						char text[480];
						sprintf(text, "%s: %s", pCat->name, pData->name);

						IngCtrlData *pCtrlData = new IngCtrlData;
						pCtrlData->pCat = pCat;
						pCtrlData->pIngData = pData;

						short index = SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_ADDSTRING, NULL, (LPARAM)text);
						SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_SETITEMDATA, index, (LPARAM)pCtrlData);
					}
				}

				if(lParam == NULL) break;

				// set the name for the recipe
				SetDlgItemText(hDlg, IDC_RECIPE_NAME, pDataEdit->pRecipeData->name);
				// add the ingredients to the listbox
				for(unsigned int k = 1 ; k <= pDataEdit->pRecipeData->Ingredients.GetCount() ; k++)
				{
					short id = *pDataEdit->pRecipeData->Ingredients.GetItem(k);
					
					// search the combo box for ingredient_id
					int count = SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_GETCOUNT, 0, 0);
					for(int d = 0 ; d < count ; d++)
					{
						IngCtrlData *pCtrlData = (IngCtrlData*)SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_GETITEMDATA, d, 0);
						if(pCtrlData->pIngData->ingredient_id == id)
						{
							// remove the item from the comboxbox
							SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_SETITEMDATA, d, (LPARAM)0);
							SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_DELETESTRING, d, 0);
							// add item to the listbox
							char text[480];
							sprintf(text, "%s: %s", pCtrlData->pCat->name, pCtrlData->pIngData->name);
							int index = SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_ADDSTRING, NULL, (LPARAM)text);
							SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_SETITEMDATA, index, (LPARAM)pCtrlData);
							break;
						}
					}
				}
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_RECIPE_LIST:
					{
						if(HIWORD(wParam) == LBN_SELCHANGE)
						{
							int index = SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, CB_GETCURSEL, 0, 0);
							if(LB_ERR == index)
							{
								EnableWindow(GetDlgItem(hDlg, IDC_RECIPE_REM_ING), FALSE);
							}
							else
							{
								EnableWindow(GetDlgItem(hDlg, IDC_RECIPE_REM_ING), TRUE);
							}
						}
					}
					break;

				case IDC_RECIPE_ADD_ING:
					{
						RecipeDataEdit *pDataEdit = (RecipeDataEdit*)GetWindowLong(hDlg, DWL_USER);
						// get the ingredient selected in combobox
						int del_index = SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_GETCURSEL, 0, 0);
						if(CB_ERR == del_index)
						{
							//MessageBox(hDlg, "Invalid ingredient selection.", "Error", MB_OK);
							break;
						}
						IngCtrlData *pCtrlData = (IngCtrlData*)SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_GETITEMDATA, del_index, 0);
						// remove the item from the comboxbox
						SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_SETITEMDATA, del_index, (LPARAM)0);
						SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_DELETESTRING, del_index, 0);
						// add ingredient to the listbox control
						char text[480];
						sprintf(text, "%s: %s", pCtrlData->pCat->name, pCtrlData->pIngData->name);
						int index = SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_ADDSTRING, NULL, (LPARAM)text);
						SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_SETITEMDATA, index, (LPARAM)pCtrlData);
					}
					break;
				
				case IDC_RECIPE_REM_ING:
					{
						RecipeDataEdit *pDataEdit = (RecipeDataEdit*)GetWindowLong(hDlg, DWL_USER);
						// get the ingredient selected in combobox
						int del_index = SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_GETCURSEL, 0, 0);
						if(LB_ERR == del_index)
							break;
						IngCtrlData *pCtrlData = (IngCtrlData*)SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_GETITEMDATA, del_index, 0);
						// remove item from listbox
						SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_SETITEMDATA, del_index, (LPARAM)0);						
						SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_DELETESTRING, del_index, 0);
						if(SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_GETCOUNT, 0, 0) == 0)
							EnableWindow(GetDlgItem(hDlg, IDC_RECIPE_REM_ING), FALSE);
						// add the item from the comboxbox
						char text[480];
						sprintf(text, "%s: %s", pCtrlData->pCat->name, pCtrlData->pIngData->name);
						int index = SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_ADDSTRING, NULL, (LPARAM)text);
						SendDlgItemMessage(hDlg, IDC_RECIPE_INGREDIENTS, CB_SETITEMDATA, index, (LPARAM)pCtrlData);						
					}
					break;

				case 1:
					{
						RecipeDataEdit *pDataEdit = (RecipeDataEdit*)GetWindowLong(hDlg, DWL_USER);
						// add ingredients to the recipe's ingredient list
						pDataEdit->pRecipeData->Ingredients.DeleteAllItems();
						int count = SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_GETCOUNT, 0, 0);
						for(int c = 0 ; c < count ; c++)
						{
							IngCtrlData *pCtrlData = (IngCtrlData*)SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, LB_GETITEMDATA, c, 0);
							pDataEdit->pRecipeData->Ingredients.AddItem(new short(pCtrlData->pIngData->ingredient_id));
						}
						// set the recipe name
						GetDlgItemText(hDlg, IDC_RECIPE_NAME, pDataEdit->pRecipeData->name, 255);
						EndDialog(hDlg, TRUE);
					}
					break;

				case 2:
					EndDialog(hDlg, FALSE);
					break;
			}
			break;

		case WM_DELETEITEM:
			{
				DELETEITEMSTRUCT *pdis = (DELETEITEMSTRUCT*) lParam;
				IngCtrlData *pCtrlData = (IngCtrlData*)pdis->itemData;
				if(pCtrlData)
					delete pCtrlData;
			}	
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK RecipesProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL)
					break;

				RecipeDlgData *pDlgData = (RecipeDlgData*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pDlgData);


				for(unsigned int c = 1 ; c <= pDlgData->pRecipeList->GetCount() ; c++)
				{
					RecipeData *pData = pDlgData->pRecipeList->GetItem(c);

					int index = SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_ADDSTRING, 0, (LPARAM)pData->name);
					SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_SETITEMDATA, index, (LPARAM)pData);
				}
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_RECIPES_LIST:
					{
						if(HIWORD(wParam) == LBN_SELCHANGE)
						{
							int index = SendDlgItemMessage(hDlg, IDC_RECIPE_LIST, CB_GETCURSEL, 0, 0);
							if(LB_ERR == index)
							{
								EnableWindow(GetDlgItem(hDlg, IDC_RECIPES_EDIT), FALSE);
								EnableWindow(GetDlgItem(hDlg, IDC_RECIPES_DELETE), FALSE);
							}
							else
							{
								EnableWindow(GetDlgItem(hDlg, IDC_RECIPES_EDIT), TRUE);
								EnableWindow(GetDlgItem(hDlg, IDC_RECIPES_DELETE), TRUE);
							}
						}
					}
					break;

				case IDC_RECIPES_ADD:
					{
						RecipeDlgData *pDlgData = (RecipeDlgData*)GetWindowLong(hDlg, DWL_USER);

						RecipeData *pData = new RecipeData;
						strcpy(pData->name, "");

						RecipeDataEdit *pDataEdit = new RecipeDataEdit;
						pDataEdit->pIngCatList = pDlgData->pIngCatList;
						pDataEdit->pRecipeData = pData;
						
						int result = DialogBoxParam(Main::hInstance, "DLG_RECIPE", hDlg, PizzaBuilderProc, (LPARAM) pDataEdit);
						if(result == FALSE)
						{
							delete pData;
						}
						else
						{
							int index = SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_ADDSTRING, 0, (LPARAM)pDataEdit->pRecipeData->name);
							SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_SETITEMDATA, index, (LPARAM)pDataEdit->pRecipeData);
							pDlgData->pRecipeList->AddItem(pData);
						}
						delete pDataEdit;
					}
					break;

				case IDC_RECIPES_EDIT:
					{
						RecipeDlgData *pDlgData = (RecipeDlgData*)GetWindowLong(hDlg, DWL_USER);

						int index = SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_GETCURSEL, 0, 0);
						if(LB_ERR == index)
							break;
						RecipeData *pRecipe = (RecipeData*)SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_GETITEMDATA, index, 0);
						
						RecipeDataEdit *pDataEdit = new RecipeDataEdit;
						pDataEdit->pIngCatList = pDlgData->pIngCatList;
						pDataEdit->pRecipeData = pRecipe;

						if(DialogBoxParam(Main::hInstance, "DLG_RECIPE", hDlg, PizzaBuilderProc, (LPARAM) pDataEdit) == TRUE)
						{
							// delete the listbox item
							SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_DELETESTRING, index, 0);
							// add a new one
							index = SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_ADDSTRING, 0, (LPARAM)pDataEdit->pRecipeData->name);
							SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_SETITEMDATA, index, (LPARAM)pDataEdit->pRecipeData);
						}
						delete pDataEdit;
					}
					break;

				case IDC_RECIPES_DELETE:
					{
						RecipeDlgData *pDlgData = (RecipeDlgData*)GetWindowLong(hDlg, DWL_USER);

						int index = SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_GETCURSEL, 0, 0);
						if(LB_ERR == index)
							break;
						RecipeData *pRecipe = (RecipeData*)SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_GETITEMDATA, index, 0);

						// delete item from list
						for(unsigned int c = 1 ; c <= pDlgData->pRecipeList->GetCount() ; c++)
						{
							RecipeData *pTest = pDlgData->pRecipeList->GetItem(c);
							if(pTest == pRecipe)
							{
								pDlgData->pRecipeList->DeleteItem(c);
								SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_DELETESTRING, index, 0);
							}
						}

						if(SendDlgItemMessage(hDlg, IDC_RECIPES_LIST, LB_GETCOUNT, 0, 0) == 0)
						{
							EnableWindow(GetDlgItem(hDlg, IDC_RECIPES_EDIT), FALSE);
							EnableWindow(GetDlgItem(hDlg, IDC_RECIPES_DELETE), FALSE);
						}
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
BOOL CALLBACK TableProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			{
				if(lParam == NULL) break;

				List<TableData> *pTableList = (List<TableData>*)lParam;
				SetWindowLong(hDlg, DWL_USER, (LONG)pTableList);

				// add all the items to the list
				for(unsigned int c = 1 ; c <= pTableList->GetCount() ; c++)
				{
					TableData *pData = pTableList->GetItem(c);

					char text[448];
					sprintf(text, "%d  |  $%.2f  |  %s", pData->chair_support, pData->price, pData->name);
					int index = SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_ADDSTRING, 
						NULL, (LPARAM)(LPCSTR)text);
					SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_SETITEMDATA, index, (LPARAM)pData);

				}

				SetFocus(GetDlgItem(hDlg, IDC_TABLE_NAME));
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_TABLE_LIST:
					{
						if(HIWORD(wParam) == LBN_SELCHANGE)
						{
							// get the selected index
							int index = SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_GETCURSEL, 0, 0);
							if(index == LB_ERR)
								break;
							// get the item data of the selected item
							TableData *pItem = (TableData*)SendDlgItemMessage(hDlg, IDC_TABLE_LIST,
								LB_GETITEMDATA, index, NULL);

							// set the editboxes' text
							SetDlgItemText(hDlg, IDC_TABLE_NAME, pItem->name);
							SetDlgItemInt(hDlg, IDC_TABLE_CHAIRS, pItem->chair_support, FALSE);

							char text[10];
							sprintf(text, "%.2f", pItem->price);
							SetDlgItemText(hDlg, IDC_TABLE_PRICE, text);
						}
					}
					break;

				case IDC_TABLE_ADD:
					{
						TableData *pData = new TableData;

						char name[256];
						GetDlgItemText(hDlg, IDC_TABLE_NAME, pData->name, 255);
						if(strlen(pData->name) < 1)
						{
							MessageBox(hDlg, "Invalid table name", "Error", MB_OK);
							delete pData;
							break;
						}
						
						char text[10];
						GetDlgItemText(hDlg, IDC_TABLE_PRICE, text, 9);
						pData->price = (float)atof(text);

						pData->chair_support = (short)GetDlgItemInt(hDlg, IDC_TABLE_CHAIRS, NULL, FALSE);

						// add item to the list
						List<TableData> *pTableList = (List<TableData>*)GetWindowLong(hDlg, DWL_USER);
						pTableList->AddItem(pData);

						// add item to the list box
						char lb_text[288];
						sprintf(text, "%d  |  $%.2f  |  %s", pData->chair_support, pData->price, pData->name);
						int index = SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_ADDSTRING, NULL, (LPARAM)(LPCSTR)text);
						SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_SETITEMDATA, index, (LPARAM)pData);
						SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_SETCURSEL, index, NULL);
						
						// clear editboxes; set focus
						SetDlgItemText(hDlg, IDC_TABLE_NAME, "");
						SetDlgItemText(hDlg, IDC_TABLE_PRICE, "");
						SetDlgItemInt(hDlg, IDC_TABLE_CHAIRS, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_TABLE_NAME));
					}
					break;

				case IDC_TABLE_EDIT:
					{
						// get the selected index
						int index = SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						// get the item data of the selected item
						TableData *pItem = (TableData*)SendDlgItemMessage(hDlg, IDC_TABLE_LIST,
							LB_GETITEMDATA, index, NULL);

						char name[256];
						GetDlgItemText(hDlg, IDC_TABLE_NAME, name, 255);
						if(strlen(name) < 1)
						{
							MessageBox(hDlg, "Invalid table name", "Error", MB_OK);
							break;
						}

						strcpy(pItem->name, name);
						
						char text[10];
						GetDlgItemText(hDlg, IDC_TABLE_PRICE, text, 9);
						pItem->price = (float)atof(text);

						pItem->chair_support = (short)GetDlgItemInt(hDlg, IDC_TABLE_CHAIRS, NULL, FALSE);

						// delete item from listbox
						SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_DELETESTRING, index, NULL);

						// add item to the list box
						char lb_text[288];
						sprintf(text, "%d  |  $%.2f  |  %s", pItem->chair_support, pItem->price, pItem->name);
						index = SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_ADDSTRING, NULL, (LPARAM)(LPCSTR)text);
						SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_SETITEMDATA, index, (LPARAM)pItem);
						SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_SETCURSEL, index, NULL);
						
						// clear editboxes; set focus
						SetDlgItemText(hDlg, IDC_TABLE_NAME, "");
						SetDlgItemText(hDlg, IDC_TABLE_PRICE, "");
						SetDlgItemInt(hDlg, IDC_TABLE_CHAIRS, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_TABLE_NAME));
					}
					break;

				case IDC_TABLE_DELETE:
					{
						// get the selected index
						int index = SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_GETCURSEL, 0, 0);
						if(index == LB_ERR)
							break;
						// get the item data of the selected item
						TableData *pItem = (TableData*)SendDlgItemMessage(hDlg, IDC_TABLE_LIST,
							LB_GETITEMDATA, index, NULL);

						// delete item from listbox
						SendDlgItemMessage(hDlg, IDC_TABLE_LIST, LB_DELETESTRING, index, NULL);

						// find table in list; delete it
						List<TableData> *pTableList = (List<TableData>*)GetWindowLong(hDlg, DWL_USER);
						for(unsigned int c = 1 ; c <= pTableList->GetCount() ; c++)
						{
							if(pTableList->GetItem(c) == pItem)
							{
								pTableList->DeleteItem(c);
								break;
							}
						}

						// clear editboxes; set focus
						SetDlgItemText(hDlg, IDC_TABLE_NAME, "");
						SetDlgItemText(hDlg, IDC_TABLE_PRICE, "");
						SetDlgItemInt(hDlg, IDC_TABLE_CHAIRS, 0, FALSE);
						SetFocus(GetDlgItem(hDlg, IDC_TABLE_NAME));
					}
					break;
			}
			break;

		case WM_DESTROY:
			break;

		default:
			return DefWindowProc(hDlg, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/