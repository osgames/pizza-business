#ifndef _JS_MAIN_H
#define _JS_MAIN_H
/*----------------------------------------------------------------------------------------------*/
#include <windows.h>
/*----------------------------------------------------------------------------------------------*/
class Main
{
public:
	static HINSTANCE hInstance;
	static HINSTANCE hPrevInstance;
	static int nCmdShow;
	static int MessageLoop();
};
/*----------------------------------------------------------------------------------------------*/
#endif