/*------------------------------------------------------------------------------------------------

	Title: Pizza Business Character DataFile Modifier
	Version: Beta 1
	Author: Johnny Salazar
	Date: June 17, 2002

------------------------------------------------------------------------------------------------*/
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------------------------*/
#include "mainwnd.h"
/*----------------------------------------------------------------------------------------------*/
int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	Main::hInstance = hInstance;
	Main::hPrevInstance = hPrevInstance;
	Main::nCmdShow = nCmdShow;

	if(!Main::hPrevInstance)
		MainWindow::Register();

	BOOL sizeable = FALSE;

	// Find the parameters
	while (*lpszCmdLine != 0)
	{          
		// Eat white space
		if (*lpszCmdLine == ' ')
		{
			lpszCmdLine++;
			continue;
		}
		
		// Do we have a dash? If not, just exit the loop
		if (*lpszCmdLine++ != '-')
			break;
		
		switch (*lpszCmdLine++)
		{
			case 's':
			case 'S':
				sizeable = TRUE;
				break;
		}
	}

	MainWindow MainWnd(sizeable);

	return Main::MessageLoop();
}
/*----------------------------------------------------------------------------------------------*/