Pizza Business Character DataFile Modifier Beta 1

This application is designed for the sole purpose of creating one file named "chargen.pbd".
Within the file is a number of binary structures that describe the traits available to the
character generator to construct dynamic characters upon the start of the game.

Take notice that this application was developed without an initial design nor user interface
considerations. Therefore, this program is not very user-friendly. There is no documentation
for the use of this application; it was developed primarily for the use of the development
team. The source code is messy and was thrown together in no more than 4-6 hours. It is not
the best looking application, but it works and appears to be stable. However, it's not 100
percent, so this initial version will be considered Beta 1.

-- Johnny Salazar