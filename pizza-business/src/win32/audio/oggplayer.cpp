#include "oggplayer.h"
/*----------------------------------------------------------------------------------------------*/
#define BLOCK_SIZE 8192
#define BLOCK_COUNT 20
/*----------------------------------------------------------------------------------------------*/
COggPlayer::COggPlayer() :
m_File(0), m_hThread(NULL), m_bStop(false), m_bLoop(false)
{
	this->SetFilename("");
	this->SetPlayerStatus(status_nofile);
}
/*----------------------------------------------------------------------------------------------*/
COggPlayer::~COggPlayer()
{
	this->CloseFile();
}
/*----------------------------------------------------------------------------------------------*/
bool COggPlayer::OpenFile(char* filename)
{
	// set filename; open file for reading
	this->SetFilename(filename);
	this->m_File = fopen(filename, "rb");
	if(m_File == NULL)
		return false;
	
	// change the player status
	this->SetPlayerStatus(status_stopped);
	m_bStop = false;

	// create the stream in paused mode
	this->m_hThread = CreateThread(NULL, NULL, OggPlayerThreadProc, (LPVOID)this,
		CREATE_SUSPENDED, &m_ThreadId);

	if(m_hThread == NULL)
		return false;

	return true;
}
/*----------------------------------------------------------------------------------------------*/
void COggPlayer::CloseFile()
{
	// stop the thread if it is running
	if(m_hThread)
	{
		this->Stop();
		CloseHandle(this->m_hThread);
		m_hThread = NULL;
	}

	// close the file
	if(this->m_File)
	{
		fclose(this->m_File);
		this->m_File = 0;
	}
	
	// set the player status
	this->SetPlayerStatus(status_nofile);
}
/*----------------------------------------------------------------------------------------------*/
bool COggPlayer::IsFileOpen()
{
	if(this->m_File)
		return true;
	else
		return false;
}
/*----------------------------------------------------------------------------------------------*/
void COggPlayer::OnNotify(UINT iMessage)
{
	//printf("notfiy\n");

	if(iMessage != WOM_DONE)
		return;
    
	//EnterCriticalSection(&waveCriticalSection);
    this->waveFreeBlockCount++;
    //LeaveCriticalSection(&waveCriticalSection);
}
/*----------------------------------------------------------------------------------------------*/
bool COggPlayer::Pause()
{
	if(m_hThread)
	{
		SuspendThread(m_hThread);
		return true;
	}

	return false;
}
/*----------------------------------------------------------------------------------------------*/
bool COggPlayer::Play(bool bLoop)
{
	if(m_hThread)
	{
		this->m_bLoop = bLoop;
		ResumeThread(m_hThread);
		return true;
	}

	return false;
}
/*----------------------------------------------------------------------------------------------*/
bool COggPlayer::Stop()
{
	this->m_bStop = true;

	return true;
}
/*----------------------------------------------------------------------------------------------*/
DWORD COggPlayer::ThreadProc()
{
	OggVorbis_File vf;
	int eof=0;
	int current_section;

	if(ov_open(m_File, &vf, NULL, 0) < 0)
	{
		//fprintf(stderr,"Input does not appear to be an Ogg bitstream.\n");
		MessageBox(0, "Input does not appear to be an Ogg bitstream.", "Error", MB_OK);
		this->CloseFile();
	}

	// Throw the comments plus a few lines about the bitstream we're
	// decoding
	char **ptr=ov_comment(&vf,-1)->user_comments;
	vorbis_info *vi=ov_info(&vf,-1);
	while(*ptr)
	{
	  //fprintf(stderr,"%s\n",*ptr);
	  ++ptr;
	}

	//ov_time_seek(&vf, (4.5*60));

	waveBlocks = allocateBlocks(BLOCK_SIZE, BLOCK_COUNT);
	waveFreeBlockCount = BLOCK_COUNT;
	waveCurrentBlock= 0;
	InitializeCriticalSection(&waveCriticalSection);

	WAVEFORMATEX wf;
	wf.cbSize = 0;
	wf.wFormatTag = WAVE_FORMAT_PCM;
	wf.nChannels = vi->channels;
	wf.nSamplesPerSec = vi->rate;
	wf.wBitsPerSample = 16;
	wf.nBlockAlign = wf.nChannels * (wf.wBitsPerSample >> 3);
	wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;

	// open the waveOut device
	HWAVEOUT waveout;
	waveOutOpen(&waveout, WAVE_MAPPER, &wf, (DWORD_PTR)waveOutProc,
		(DWORD_PTR)this, CALLBACK_FUNCTION);

	char pcmout[4096];

	do
	{
		while(!eof)
		{
			if(this->m_bStop == true)
			{
				this->m_bLoop = false; // to break from the playloop loop
				break;
			}

			long ret = ov_read(&vf,pcmout,sizeof(pcmout),0,2,1,&current_section);

			if (ret == 0) 
			{
				// EOF
				eof=1;
			} 
			else if (ret < 0)
			{
				// error in the stream.  Not a problem, just reporting it in
				//case we (the app) cares.  In this case, we don't.
				//printf("erro in stream\n");
			} 
			else 
			{
				// we don't bother dealing with sample rate changes, etc, but
				// you'll have to
				writeAudio(waveout, pcmout, ret);
			}
		}

		ov_time_seek(&vf, 0);
		eof = 0;
	}while(this->IsLooping() == true);

	while(waveFreeBlockCount < BLOCK_COUNT)
		Sleep(10);
	/*
	* unprepare any blocks that are still prepared
	*/
	for(int i = 0; i < waveFreeBlockCount; i++) 
		if(waveBlocks[i].dwFlags & WHDR_PREPARED)
			waveOutUnprepareHeader(waveout, &waveBlocks[i], sizeof(WAVEHDR));

	DeleteCriticalSection(&waveCriticalSection);
	freeBlocks(waveBlocks);

	waveOutClose(waveout);

	/* cleanup */
	ov_clear(&vf);

		//if(fseek(this->m_File, 0L, SEEK_SET) != 0)
		//	MessageBox(0, "fseek() failed", "error", MB_OK);
	
	// close the file
	this->CloseFile();

	ExitThread(0);
	MessageBox(0, "End of Thread", "Note", MB_OK);

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
DWORD WINAPI OggPlayerThreadProc(LPVOID lpParameter)
{
	COggPlayer* pPlayer = (COggPlayer*)lpParameter;
	return pPlayer->ThreadProc();
}
/*----------------------------------------------------------------------------------------------*/
WAVEHDR* allocateBlocks(int size, int count)
{
    unsigned char* buffer;
    int i;
    WAVEHDR* blocks;
    DWORD totalBufferSize = (size + sizeof(WAVEHDR)) * count;
    /*
    * allocate memory for the entire set in one go
    */
    if((buffer = (unsigned char*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, totalBufferSize)) == NULL)
	{
        //fprintf(stderr, "Memory allocation error\n");
        ExitProcess(1);
    }

    /*
    * and set up the pointers to each bit
    */
    blocks = (WAVEHDR*)buffer;
    buffer += sizeof(WAVEHDR) * count;


        for(i = 0; i < count; i++) {
        blocks[i].dwBufferLength = size;
        blocks[i].lpData = (char*)buffer;
        buffer += size;
    }

    return blocks;
}
/*----------------------------------------------------------------------------------------------*/
void freeBlocks(WAVEHDR* blockArray)
{
    /* 
    * and this is why allocateBlocks works the way it does
    */ 
    HeapFree(GetProcessHeap(), 0, blockArray);
}
/*----------------------------------------------------------------------------------------------*/
void COggPlayer::writeAudio(HWAVEOUT hWaveOut, LPSTR data, int size)
{
    WAVEHDR* current;
    int remain;
    current = &waveBlocks[waveCurrentBlock];


    while(size > 0)
	{
		/* 
		* first make sure the header we're going to use is unprepared
		*/
		if(current->dwFlags & WHDR_PREPARED) 
			waveOutUnprepareHeader(hWaveOut, current, sizeof(WAVEHDR));


        if(size < (int)(BLOCK_SIZE - current->dwUser)) 
		{
			memcpy(current->lpData + current->dwUser, data, size);
			current->dwUser += size;
			break;
		}

        remain = BLOCK_SIZE - current->dwUser;
        memcpy(current->lpData + current->dwUser, data, remain);
        size -= remain;
        data += remain;
        current->dwBufferLength = BLOCK_SIZE;
        
		waveOutPrepareHeader(hWaveOut, current, sizeof(WAVEHDR));
        waveOutWrite(hWaveOut, current, sizeof(WAVEHDR));
        
		EnterCriticalSection(&waveCriticalSection);
        waveFreeBlockCount--;
        LeaveCriticalSection(&waveCriticalSection);
        
		/*
        * wait for a block to become free
        */
        while(!waveFreeBlockCount)
			Sleep(10);
        /*
        * point to the next block
        */
        waveCurrentBlock++;
        waveCurrentBlock %= BLOCK_COUNT;
        current = &waveBlocks[waveCurrentBlock];
        current->dwUser = 0;
    }
}
/*----------------------------------------------------------------------------------------------*/
extern COggPlayer *pPlayer;
void CALLBACK waveOutProc(HWAVEOUT hWaveOut, UINT uMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2)
{
    /*
    * pointer to free block counter
    */
    COggPlayer* pPlayer = (COggPlayer*)dwInstance;
	pPlayer->OnNotify(uMsg);
}