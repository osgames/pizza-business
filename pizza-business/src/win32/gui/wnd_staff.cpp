#include "wnd_staff.h"
#include <commctrl.h>
/*----------------------------------------------------------------------------------------------*/
#define IDC_HIRE_HIRE		1
#define IDC_HIRE_CANCEL		2
#define IDC_HIRE_PEOPLELIST	101
#define IDC_HIRE_COOK		102
#define IDC_HIRE_MANAGER	103
#define IDC_HIRE_WAITER		104

#define IDC_STAFF_OK		1
#define IDC_STAFF_CANCEL	2
#define IDC_STAFF_ALL		101
#define IDC_STAFF_CHEF		102
#define IDC_STAFF_MANAGER	103
#define IDC_STAFF_WAITER	104
#define IDC_STAFF_STAFFLIST	105
#define IDC_STAFF_INFO		106
#define IDC_STAFF_HIRE		107
#define IDC_STAFF_FIRE		108			
/*----------------------------------------------------------------------------------------------*/
CHireDlg::CHireDlg()
{
	this->m_hireList.SetDeleteFlag(false);
}
/*----------------------------------------------------------------------------------------------*/
void CHireDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case IDC_HIRE_HIRE:
			this->ListView_GetSelectEmployees();
			EndDialog(hWnd, TRUE);
			break;

		case IDC_HIRE_CANCEL:
			EndDialog(hWnd, FALSE);
			break;

		case IDC_HIRE_COOK:
			this->m_title = TITLE_CHEF;
			this->RecalcSalaries();
			break;

		case IDC_HIRE_MANAGER:
			this->m_title = TITLE_MANAGER;
			this->RecalcSalaries();
			break;

		case IDC_HIRE_WAITER:
			this->m_title = TITLE_WAITER;
			this->RecalcSalaries();
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CHireDlg::OnInitDialog()
{
	SetWindowText(hWnd, "Hire Staff");

	// select the first radio button
	SendDlgItemMessage(hWnd, IDC_HIRE_COOK, BM_SETCHECK, BST_CHECKED, NULL);
	this->m_title = TITLE_CHEF;

	// customize the listview control
	ListView_SetExtendedListViewStyleEx(GetDlgItem(hWnd, IDC_HIRE_PEOPLELIST),
		LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

	// create the columns for the listview control
	LVCOLUMN lvc;
	lvc.mask = LVCF_TEXT|LVCF_WIDTH|LVCF_FMT;
	
	lvc.pszText = "Name";
	lvc.cx = 200;
	lvc.fmt = LVCFMT_LEFT;
	ListView_InsertColumn(GetDlgItem(hWnd, IDC_HIRE_PEOPLELIST), 0, &lvc);

	lvc.pszText = "Salary";
	lvc.cx = 75;
	lvc.fmt = LVCFMT_RIGHT;
	ListView_InsertColumn(GetDlgItem(hWnd, IDC_HIRE_PEOPLELIST), 1, &lvc);

	// add employees to hire to the listview control
	for(unsigned int c = 1 ; c <= m_pPeople->GetCount() ; c++)
	{
		CEmployee *pEmployee = m_pPeople->GetItem(c);
		if(pEmployee->m_EmployedAt == NULL)
			this->ListView_AddEmployee(pEmployee);
	}

	this->RecalcSalaries();
}
/*----------------------------------------------------------------------------------------------*/
void CHireDlg::ListView_AddEmployee(CEmployee *pEmployee)
{
	char name[256];
	pEmployee->GetName(name);

	LVITEM lvi;
	lvi.mask = LVIF_TEXT|LVIF_PARAM;
	lvi.iItem = 0;
	lvi.iSubItem = 0;
	lvi.lParam = (LPARAM)pEmployee;

	lvi.pszText = name;
	ListView_InsertItem(GetDlgItem(hWnd, IDC_HIRE_PEOPLELIST), &lvi);
}
/*----------------------------------------------------------------------------------------------*/
void CHireDlg::ListView_GetSelectEmployees()
{
	HWND hwndLV = GetDlgItem(hWnd, IDC_HIRE_PEOPLELIST);
	// iterate through the listview items
	int count = ListView_GetItemCount(hwndLV);
	for(int index = 0 ; index < count ; index++)
	{
		// add selected employees to the list
		if(ListView_GetItemState(hwndLV, index, LVIS_SELECTED) & LVIS_SELECTED)
		{
			LVITEM lvi;
			lvi.iItem = index;
			lvi.mask = LVIF_PARAM;
			ListView_GetItem(hwndLV, &lvi);

			this->m_hireList.AddItem((CEmployee*)lvi.lParam);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CHireDlg::RecalcSalaries()
{
	HWND hwndLV = GetDlgItem(hWnd, IDC_HIRE_PEOPLELIST);
	// iterate through the listview items
	int count = ListView_GetItemCount(hwndLV);
	for(int index = 0 ; index < count ; index++)
	{
		// claculate the salary of each person
		LVITEM lvi;
		lvi.iItem = index;
		lvi.mask = LVIF_PARAM;
		ListView_GetItem(hwndLV, &lvi);

		CEmployee *pEmployee = (CEmployee*)lvi.lParam;

		int competency = pEmployee->GetCompetency();
		short age = pEmployee->GetAge();
		float salary = ((((float)competency)/2.0) * 0.8) + (((float)age) * 0.2);
		salary += salary * (((float)this->m_title) * 0.4);
		pEmployee->SetSalary(salary);

		char str_salary[10];
		sprintf(str_salary, "$%.2f", salary);
		ListView_SetItemText(hwndLV, index, 1, str_salary);
	}
}
/*----------------------------------------------------------------------------------------------*/
long CHireDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------*/
void CStaffDlg::FireSelectedEmployees()
{
	HWND hwndLV = GetDlgItem(hWnd, IDC_STAFF_STAFFLIST);
	// iterate through the listview items
	int count = ListView_GetItemCount(hwndLV);
	for(int index = 0 ; index < count ; index++)
	{
		// add selected employees to the list
		if(ListView_GetItemState(hwndLV, index, LVIS_SELECTED) & LVIS_SELECTED)
		{
			LVITEM lvi;
			lvi.iItem = index;
			lvi.mask = LVIF_PARAM;
			ListView_GetItem(hwndLV, &lvi);

			this->m_pRestaurant->FireEmployee((CEmployee*)lvi.lParam);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CStaffDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case IDC_STAFF_OK:
			EndDialog(hWnd, TRUE);
			break;

		case IDC_STAFF_CANCEL:
			EndDialog(hWnd, FALSE);
			break;

		case IDC_STAFF_HIRE:
			{
				CHireDlg dlg;
				dlg.SetPeopleList(this->m_pPeople);
				if(dlg.DoModalDlg("STAFF_HIRE", hWnd, this->m_hInstance) == TRUE)
				{
					Emp_Title title = dlg.GetJobTitle();
					List<CEmployee> *pHireList = dlg.GetHireList();
					for(unsigned int c = 1 ; c <= pHireList->GetCount() ; c++)
						this->m_pRestaurant->HireEmployee(pHireList->GetItem(c), title);
				}
				this->RefreshListView();
			}
			break;

		case IDC_STAFF_FIRE:
			if(IDYES == MessageBox(hWnd, "Are you sure you want to terminate the selected"
				" employee.", "Just checking", MB_YESNO))
			{
				this->FireSelectedEmployees();
				this->RefreshListView();
			}
			break;

		case IDC_STAFF_ALL:
		case IDC_STAFF_CHEF:
		case IDC_STAFF_MANAGER:
		case IDC_STAFF_WAITER:
			this->RefreshListView();
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CStaffDlg::OnInitDialog()
{
	SetWindowText(hWnd, "Restaurant Staff");

	// set the first radio button
	SendDlgItemMessage(hWnd, IDC_STAFF_ALL, BM_SETCHECK, BST_CHECKED, NULL);

	// customize the listview control
	ListView_SetExtendedListViewStyleEx(GetDlgItem(hWnd, IDC_STAFF_STAFFLIST),
		LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

	// create the columns for the listview control
	LVCOLUMN lvc;
	lvc.mask = LVCF_TEXT|LVCF_WIDTH|LVCF_FMT;

	lvc.pszText = "Name";
	lvc.cx = 200;
	lvc.fmt = LVCFMT_LEFT;
	ListView_InsertColumn(GetDlgItem(hWnd, IDC_STAFF_STAFFLIST), 0, &lvc);

	lvc.pszText = "Job";
	lvc.cx = 100;
	lvc.fmt = LVCFMT_LEFT;
	ListView_InsertColumn(GetDlgItem(hWnd, IDC_STAFF_STAFFLIST), 1, &lvc);

	lvc.pszText = "Salary";
	lvc.cx = 75;
	lvc.fmt = LVCFMT_RIGHT;
	ListView_InsertColumn(GetDlgItem(hWnd, IDC_STAFF_STAFFLIST), 2, &lvc);

	// now update the listview control
	this->RefreshListView();
}
/*----------------------------------------------------------------------------------------------*/
void CStaffDlg::ListView_AddEmployee(CEmployee *pEmployee)
{
	if(!pEmployee) return;
	HWND hwndLV = GetDlgItem(hWnd, IDC_STAFF_STAFFLIST);

	// get the data
	char name[256];
	pEmployee->GetName(name);

	Emp_Title title = pEmployee->GetTitle();
	float salary = pEmployee->GetSalary();

	// add the employee to the listview control
	LVITEM lvi;
	lvi.mask = LVIF_TEXT|LVIF_PARAM;
	lvi.iItem = 0;
	lvi.iSubItem = 0;
	lvi.lParam = (LPARAM)pEmployee;

	lvi.pszText = name;
	int index = ListView_InsertItem(hwndLV, &lvi);

	// set the text of the subitems
	char str_title[10], str_salary[10];
	
	if(title == TITLE_CHEF)
		strcpy(str_title, "Cook");
	else if(title == TITLE_MANAGER)
		strcpy(str_title, "Manager");
	else if(title == TITLE_WAITER)
		strcpy(str_title, "Waiter");
	ListView_SetItemText(hwndLV, index, 1, str_title);

	sprintf(str_salary, "$%.2f", salary);
	ListView_SetItemText(hwndLV, index, 2, str_salary);
}
/*----------------------------------------------------------------------------------------------*/
void CStaffDlg::RefreshListView()
{
	bool bShowAll = false;
	Emp_Title title;

	if(SendDlgItemMessage(hWnd, IDC_STAFF_CHEF, BM_GETCHECK, NULL, NULL) == BST_CHECKED)
		title = TITLE_CHEF;
	else if(SendDlgItemMessage(hWnd, IDC_STAFF_MANAGER, BM_GETCHECK, NULL, NULL) == BST_CHECKED)
		title = TITLE_MANAGER;
	else if(SendDlgItemMessage(hWnd, IDC_STAFF_WAITER, BM_GETCHECK, NULL, NULL) == BST_CHECKED)
		title = TITLE_WAITER;
	else
		bShowAll = true;
		

	ListView_DeleteAllItems(GetDlgItem(hWnd, IDC_STAFF_STAFFLIST));
	// add the hired employees to the listview control
	List<CEmployee> *pHiredList = &this->m_pRestaurant->m_EmpList;
	for(unsigned int c = 1 ; c <= pHiredList->GetCount() ; c++)
	{
		CEmployee *pEmployee = pHiredList->GetItem(c);
		
		if(bShowAll == true || pEmployee->GetTitle() == title)
			this->ListView_AddEmployee(pEmployee);
	}
}
/*----------------------------------------------------------------------------------------------*/
long CStaffDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/