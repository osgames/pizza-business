#include "mainwnd.h"
#include "wnd_buy.h"
#include "wnd_pizza.h"
#include "wnd_staff.h"
#include "wnd_stats.h"
#include "wnd_bank.h"
/*----------------------------------------------------------------------------------------------*/
#define IDC_STATISTICS	200
#define IDC_STAFF		201
#define IDC_BUYSELL		202
#define IDC_PIZZAS		203
#define IDC_BANK		204
#define IDC_ENDTURN		205
#define IDC_MAINMENU	206
/*----------------------------------------------------------------------------------------------*/
CMainWindow::CMainWindow(HINSTANCE hInstance, CApplication *pApplication) :
bShowFace(FALSE), m_pStaffMenu(0), m_pBtnMenu(0)
{
	m_navbar_height = 48;
	m_navbar_width = 128;

	m_navbtn_width = 100;
	m_navbtn_height = 30;

	this->m_hInstance = hInstance;
	this->SetApplication(pApplication);

	// create our main window
	this->CreateMainWindow();

	// show the initial menu window
	this->DisplayMenuWindow();

	// we can now show the main user interface
	bShowFace = TRUE;
	InvalidateRect(hWnd, NULL, FALSE);

	this->CreateNavButtons();

	if(m_OggPlayer.OpenFile("test.ogg") == true)
		m_OggPlayer.Play(true); // loop background music

	// create the menus
	/*m_pStaffMenu = new CMenu(hWnd, this->m_hInstance);
	m_pStaffMenu->SetCaption("Staff");
	m_pStaffMenu->Add_MenuItem("Hire", 1);
	m_pStaffMenu->Add_MenuItem("Fire", 2);
	m_pStaffMenu->Add_MenuItem("More Info...", 3);*/
}
/*----------------------------------------------------------------------------------------------*/
CMainWindow::~CMainWindow()
{
	// cleanup GDI resources
	DeleteObject(hFontBudget);
	DeleteObject(hbrBlack);
	SelectObject(memDC1, oldBmp1);
	DeleteObject(hBmpBground);
	DeleteDC(memDC1);

	if(m_pStaffMenu)
		delete m_pStaffMenu;

	if(m_pBtnMenu)
		delete m_pBtnMenu;

	if(m_pBtnStats)
		delete m_pBtnStats;

	if(m_pBtnBuySell)
		delete m_pBtnBuySell;

	if(m_pBtnPizzas)
		delete m_pBtnPizzas;

	if(m_pBtnBank)
		delete m_pBtnBank;

	if(m_pBtnEndTurn)
		delete m_pBtnEndTurn;

	DestroyWindow(hWnd);
}
/*----------------------------------------------------------------------------------------------*/
char CMainWindow::szClassName[] = "Pizza Business";
bool CMainWindow::registered = false;
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::CreateNavButtons()
{
	// create the buttons for the navigation bar

	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	const short btn_spacing = 10;
	short b = 0, y = 0;
	// create the five top buttons
	for(b = 0, y = btn_spacing*3 ; b < 7 ; b++, y += btn_spacing + m_navbtn_height)
	{
		//--temp--
		/*if(b == 5) 
		{
			y -= btn_spacing + m_navbtn_height;
			continue;
		}*/
		//--temp--

		if(b == 5) y += btn_spacing * 2; // end turn button

		if(b == 6) // budget text area
		{
			y += btn_spacing * 3;
			// this is to calculate the rect for the budget text
			m_rcBudget.left = rcClient.right-(m_navbar_width/2)-(m_navbtn_width/2);
			m_rcBudget.top = y;
			m_rcBudget.right = m_rcBudget.left + m_navbtn_width;
			m_rcBudget.bottom = m_rcBudget.top + 100;
			continue;
		}

		CHoverButton *pButton = new CHoverButton(hWnd, (200+b), WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
			"Main Menu", this->m_hInstance,
			rcClient.right-(m_navbar_width/2)-(m_navbtn_width/2), y,
			m_navbtn_width, m_navbtn_height);

		switch(b)
		{
		case 0:
			m_pBtnStats = pButton;
			break;
		case 1:
			m_pBtnStaff = pButton;
			break;
		case 2:
			m_pBtnBuySell = pButton;
			break;
		case 3:
			m_pBtnPizzas = pButton;
			break;
		case 4:
			m_pBtnBank = pButton;
			break;
		case 5:
			m_pBtnEndTurn = pButton;
			break;
		}
	}
	// set the text of the buttons
	SetWindowText(GetDlgItem(hWnd, 200), "Statistics");
	SetWindowText(GetDlgItem(hWnd, 201), "Staff");
	SetWindowText(GetDlgItem(hWnd, 202), "Buy/Sell");
	SetWindowText(GetDlgItem(hWnd, 203), "Ads");
	SetWindowText(GetDlgItem(hWnd, 204), "Bank");
	SetWindowText(GetDlgItem(hWnd, 205), "End Turn");

	// create the menu button
	for(b = 0, y = rcClient.bottom - m_navbar_height-m_navbtn_height ; b < 1 ; b++, y -= btn_spacing + m_navbtn_height)
	{
		if(b == 0)
		{
			m_pBtnMenu = new CHoverButton(hWnd, 206, WS_CHILD|WS_VISIBLE|BS_OWNERDRAW, "Main Menu",
				this->m_hInstance,
				rcClient.right-(m_navbar_width/2)-(m_navbtn_width/2), y,
				m_navbtn_width, m_navbtn_height);
		}

	}
	SetWindowText(GetDlgItem(hWnd, 206), "Main Menu");
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::CreateMainWindow()
{
	if(this->registered == false)
		this->Register();

	// get the dimensions for the desktop window
	RECT rcDesktop;
	GetClientRect(GetDesktopWindow(), &rcDesktop);

	// create main window (center it in the screen)
	hWnd = ::CreateWindow(szClassName, szClassName,
		WS_OVERLAPPEDWINDOW^WS_MAXIMIZEBOX^WS_SIZEBOX, 
		(rcDesktop.right/2)-(806/2), (rcDesktop.bottom/2)-(625/2), 806, 625,
		NULL, NULL, this->m_hInstance, (LPSTR)this);
	
	if(!hWnd)
		exit(FALSE);

	// setup our graphic tools
	this->InitializeGDI();

	// show the window now
	//Show(SW_MAXIMIZE);
	Show(SW_SHOWNORMAL);
	Update();
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::DisplayMenuWindow()
{
	bool bResetCurRest = false;
	// show the initial menu window
	do
	{
		CMenuWindow MenuWindow(this->m_hInstance, this->m_pApplication->m_Simulator.IsGameInSession());
		int iResult = MenuWindow.DoModalDialog(hWnd);
		switch(iResult)
		{
			case IDC_NEWGAME:
				this->m_pApplication->m_Simulator.StartNewGame();
				bResetCurRest = true;
				break;

			case IDC_LOADGAME:
				bResetCurRest = this->OnCommand_LoadGame();
				break;

			case IDC_SAVEGAME:
				bResetCurRest = this->OnCommand_SaveGame();
				break;

			case IDC_EXIT:
				bResetCurRest = true;
				//PostQuitMessage(0);
				DestroyWindow(hWnd);
				return;

			case IDC_RESUME:
			default:
				bResetCurRest = true;
		}
	}while(bResetCurRest == false);

	// set the interface's current restaurant pointer
	COwner *pPlayer = this->m_pApplication->m_Simulator.GetHumanPlayer();
	CRestaurant *pRestaurant = pPlayer->GetRestaurant(1);
	this->m_pApplication->m_pInterface->SetCurrentRestaurant(pRestaurant);
}
/*----------------------------------------------------------------------------------------------*/
CRestaurant* CMainWindow::GetCurrentRestaurant()
{
	// return the pointer of the restaurant
	// that is kept in the application's interface
	return this->m_pApplication->m_pInterface->GetCurrentRestaurant();
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::InitializeGDI()
{
	// initialize painting stuff
	hbrBlack = CreateSolidBrush(RGB(0,0,0));
	hBmpBground = (HBITMAP)LoadImage(this->m_hInstance, "mainwnd.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	HDC hdc = GetDC(hWnd);
	memDC1 = CreateCompatibleDC(hdc);
	ReleaseDC(hWnd, hdc);

	hFontBudget = CreateFont(24,0,0,0,0,0,0,0,0,0,0,0,0, "Comic Sans MS");

	oldBmp1 = (HBITMAP)SelectObject(memDC1, hBmpBground);
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
	case IDC_MAINMENU:
		this->DisplayMenuWindow();
		break;
		
	case IDC_STAFF:
		{
			//RECT rc;
			//GetWindowRect(GetDlgItem(hWnd, IDC_STAFF), &rc);
			//MapWindowPoints((HWND)HIWORD(wParam), HWND_DESKTOP, (LPPOINT)&rc, 2);
			//this->m_pStaffMenu->ShowMenu(rc.left, rc.top, false);

			CStaffDlg dlg;
			dlg.SetRestaurant(GetCurrentRestaurant());
			dlg.SetPeopleList(&this->m_pApplication->m_Simulator.m_DataPool.m_CanList);
			dlg.DoModalDlg("STAFF_MAIN", hWnd, this->m_hInstance);
		}
		break;

	case IDC_BUYSELL:
		{
			CBuySellDlg dlg;
			CDataPool *pDataPool = &this->m_pApplication->m_Simulator.m_DataPool;
			dlg.SetChairPrice(pDataPool->m_Chair_price);
			dlg.SetAdsList(pDataPool->m_AdList);
			dlg.SetIngList(pDataPool->m_IngList);
			dlg.SetTableList(pDataPool->m_TableList);
			dlg.SetOvenList(pDataPool->m_OvenList);
			dlg.SetRestaurant(GetCurrentRestaurant());
			dlg.DoModalDlg("FURN_OWN1", hWnd, this->m_hInstance);
		}
		break;

	case IDC_PIZZAS:
		{
			CPizzasDlg dlg;
			dlg.SetRestaurant(GetCurrentRestaurant());
			dlg.DoModalDlg("PIZZA_MAIN", hWnd, this->m_hInstance);
		}
		break;

	case IDC_BANK:
		{
			CBankDlg dlg(this->GetCurrentRestaurant());
			dlg.DoModalDlg("BANK_MAIN", hWnd, this->m_hInstance);
		}
		break;

	case IDC_ENDTURN:
		{
			this->m_pApplication->m_Simulator.SimulateTurn();
			// as long as the player still has a restaurant
			// the game will continue
			if(this->m_pApplication->m_Simulator.GetHumanPlayer()->GetRestaurantCount() < 1)
			{
				this->m_pApplication->m_pInterface->SetCurrentRestaurant(0);
				MessageBox(hWnd, "Game Over\nYou can no longer continue the game with such horrible debt.",
					"Game Over", MB_OK);
				PostMessage(hWnd, WM_COMMAND, IDC_MAINMENU, NULL);
				break;
			}
		}
		// NOTE: break intentionally left out
	case IDC_STATISTICS:
		// display the statistics dialog
		{
			CRestaurant *pRestaurant = GetCurrentRestaurant();
			DayStats *pStats = pRestaurant->m_StatsList.GetItem(1);
			if(pStats)
			{
				CDayStatsDlg dlg;
				dlg.SetDayStats(pStats);
				dlg.DoModalDlg("STATISTICS", hWnd, this->m_hInstance);
			}
		}
		break;
	}

	// paint the window again (only the budget rect)
	InvalidateRect(hWnd, &this->m_rcBudget, FALSE);
}
/*----------------------------------------------------------------------------------------------*/
bool CMainWindow::OnCommand_LoadGame()
{
	char szFile[MAX_PATH] = {'\0'};

	OPENFILENAME of;
	ZeroMemory(&of, sizeof(OPENFILENAME));
	of.lStructSize = sizeof(OPENFILENAME);
	of.hInstance = this->m_hInstance;
	of.hwndOwner = this->GetHandle();
	of.lpstrFilter = "Pizza Business Game\0*.INI\0";
	of.nFilterIndex = 1;
	of.lpstrTitle = "Load game";
	of.Flags = OFN_FILEMUSTEXIST;
	of.lpstrFile = szFile;
	of.nMaxFile = MAX_PATH-1;

	if(GetOpenFileName(&of))
	{
		this->m_pApplication->m_Simulator.LoadGame(of.lpstrFile);
		return true;
	}
	else
		return false;
}
/*----------------------------------------------------------------------------------------------*/
bool CMainWindow::OnCommand_SaveGame()
{
	char szFile[MAX_PATH] = {'\0'};

	OPENFILENAME of;
	ZeroMemory(&of, sizeof(OPENFILENAME));
	of.lStructSize = sizeof(OPENFILENAME);
	of.hInstance = this->m_hInstance;
	of.hwndOwner = this->GetHandle();
	of.lpstrFilter = "Pizza Business Game\0*.INI\0";
	of.nFilterIndex = 1;
	of.lpstrTitle = "Save game";
	of.Flags = OFN_PATHMUSTEXIST;
	of.lpstrFile = szFile;
	of.nMaxFile = MAX_PATH-1;

	if(GetSaveFileName(&of))
	{
		this->m_pApplication->m_Simulator.SaveGame(szFile);
		return true;
	}
	else
		return false;
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::OnDrawItem(LPDRAWITEMSTRUCT lpdis)
{
	int x, y;
	short button_width = lpdis->rcItem.right;
	short button_height = lpdis->rcItem.bottom;

	// calculate the button's rect on the main window
	RECT rcWindow, rcButton;
	GetWindowRect(hWnd, &rcWindow);
	GetWindowRect(lpdis->hwndItem, &rcButton);
	rcButton.left = rcButton.left-rcWindow.left;
	rcButton.top = rcButton.top-rcWindow.top;
	rcButton.right = 100;
	rcButton.right = 30;

	CHoverButton *pButton = 0;
	switch(lpdis->CtlID)
	{
	case IDC_MAINMENU:
		y = 93;
		pButton = this->m_pBtnMenu;	
		break;
	case IDC_STATISTICS:
		y = 186;
		pButton = this->m_pBtnStats;
		break;
	case IDC_STAFF:
		y = 155;
		pButton = this->m_pBtnStaff;
		break;
	case IDC_BUYSELL:
		y = 31;
		pButton = this->m_pBtnBuySell;
		break;
	case IDC_PIZZAS:
		y = 0;
		pButton = this->m_pBtnPizzas;
		break;
	case IDC_BANK:
		y = 124;
		pButton = this->m_pBtnBank;
		break;
	case IDC_ENDTURN:
		y = 62;
		pButton = this->m_pBtnEndTurn;
		break;
	}

	if(pButton) {
		if(pButton->IsHover() == FALSE)
		{
			if(lpdis->itemState & ODS_SELECTED)
				x = 0;
			else
				x = 202;
		}
		else
		{
			if(lpdis->itemState & ODS_SELECTED)
				x = 0;
			else
				x = 101;
		}
	}

	SelectObject(this->memDC1, this->hBmpBground);
	// blit the part of the nav bar background
	BitBlt(lpdis->hDC, 0, 0, lpdis->rcItem.right, lpdis->rcItem.bottom, memDC1,
		rcButton.left, rcButton.top, SRCCOPY);
	// apply the mask
	//BitBlt(lpdis->hDC, 0, 0, button_width, button_height, memDC1, x, 217, SRCAND);
	// apply the mask
	//BitBlt(lpdis->hDC, 0, 0, button_width, button_height, memDC1, x, 217, SRCPAINT);
	BitBlt(lpdis->hDC, 0, 0, button_width, button_height, memDC1, x, 217, SRCAND);
	// apply the image
	BitBlt(lpdis->hDC, 0, 0, button_width, button_height, memDC1, x, y, SRCPAINT);
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::OnPaint()
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SetMapMode(hdc, MM_TEXT);
	SetBkMode(hdc, TRANSPARENT); 

	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	// if the initial menu screen has not be closed
	// then show the black and gray interlaced background
	if(this->bShowFace == FALSE)
	{
		HBITMAP hBmp = (HBITMAP)LoadImage(this->m_hInstance, "bground.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		HBRUSH hBkBrush = CreatePatternBrush(hBmp);
		FillRect(hdc, &rcClient, hBkBrush);
		DeleteObject(hBmp);
	}
	else
	{
		// fill the background with the clor black
		SetTextColor(hdc, RGB(255,255,255));
		FillRect(hdc, &rcClient, hbrBlack);

		// draw the right side of the nav bar
		BitBlt(hdc, rcClient.right-m_navbar_width, 0, m_navbar_width, rcClient.bottom, memDC1,
			rcClient.right-m_navbar_width, 0, SRCCOPY);
		// draw the bottom of the nav bar
		BitBlt(hdc, 0, rcClient.bottom-m_navbar_height, rcClient.right-m_navbar_width, m_navbar_height, memDC1,
			0, rcClient.bottom-m_navbar_height, SRCCOPY);

		// paint the budget below the "end turn" button
		char budget[15];
		CRestaurant *pRestaurant = this->GetCurrentRestaurant();
		if(pRestaurant)
		{
			double rest_budget = pRestaurant->GetBudget();

			sprintf(budget, "$%.2f", rest_budget);
			HFONT oldFont = (HFONT)SelectObject(hdc, this->hFontBudget);
			DrawText(hdc, budget, strlen(budget), &m_rcBudget, DT_CENTER);
			SelectObject(hdc, oldFont);	
		}
		else
			this->m_pLogger->WriteError("Invalid Restaurant (UI Current) pointer");
	}
	
	EndPaint(hWnd, &ps);
}
/*----------------------------------------------------------------------------------------------*/
void CMainWindow::Register()
{	
	// register the class with WinOS
	WNDCLASS wndclass;
	wndclass.style		   = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc   = ::WndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = sizeof(CMainWindow *);
	wndclass.hInstance	   = this->m_hInstance;
	wndclass.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName   = NULL;
	wndclass.lpszClassName = szClassName;
	if(!RegisterClass(&wndclass))
		exit(FALSE);

	registered = true;
}
/*----------------------------------------------------------------------------------------------*/
long CMainWindow::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_PAINT:
			this->OnPaint();
			break;

		case WM_DRAWITEM:
			this->OnDrawItem((LPDRAWITEMSTRUCT)lParam);
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/