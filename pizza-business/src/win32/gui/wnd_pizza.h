#ifndef _JS_WND_PIZZA_H
#define _JS_WND_PIZZA_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "..\..\pizzas.h"
/*----------------------------------------------------------------------------------------------*/
class CPizzasDlg : public CDialog
{
public:
	void SetRestaurant(CRestaurant *pRestaurant) { m_pRestaurant = pRestaurant; }

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

public:
	CPizzaObject* GetSelectedPizza();
	void RefreshPizzaListBox();

protected:
	CRestaurant *m_pRestaurant;

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();
};
/*----------------------------------------------------------------------------------------------*/
class CPizzaEditDlg : public CDialog
{
public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

public:
	void SetPizza(CPizzaObject *pPizzaObject) { m_pPizza = pPizzaObject; }
	void SetRestaurant(CRestaurant *pRestaurant) { m_pRestaurant = pRestaurant; }

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();

private:
	CPizzaObject *m_pPizza;
	CRestaurant *m_pRestaurant;
};
/*----------------------------------------------------------------------------------------------*/
#endif