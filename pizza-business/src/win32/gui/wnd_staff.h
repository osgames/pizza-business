#ifndef _JS_WNDSTAFF_H
#define _JS_WNDSTAFF_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "..\..\datarest.h"
#include "..\..\staff.h"
/*----------------------------------------------------------------------------------------------*/
class CHireDlg : public CDialog
{
public:
	CHireDlg();

public:
	List<CEmployee>* GetHireList() { return &m_hireList; }
	Emp_Title GetJobTitle() { return m_title; }

	void SetPeopleList(List<CEmployee> *pList) {
		m_pPeople = pList;
	}

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();

private:
	void ListView_AddEmployee(CEmployee *pEmployee);
	void ListView_GetSelectEmployees();
	void RecalcSalaries();

private:
	List<CEmployee> *m_pPeople;
	List<CEmployee> m_hireList;
	Emp_Title m_title;
};
/*----------------------------------------------------------------------------------------------*/
class CStaffDlg : public CDialog
{
public:
	void SetPeopleList(List<CEmployee> *pList) {
		m_pPeople = pList;
	}
	void SetRestaurant(CRestaurant *pRestaurant) {
		m_pRestaurant = pRestaurant;
	}

public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();

private:
	void RefreshListView();
	void ListView_AddEmployee(CEmployee *pEmployee);
	void FireSelectedEmployees();

private:
	CRestaurant *m_pRestaurant;
	List<CEmployee> *m_pPeople;
};
/*----------------------------------------------------------------------------------------------*/
#endif