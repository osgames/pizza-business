#ifndef _JS_HOVERBTN_H
#define _JS_HOVERBTN_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
/*----------------------------------------------------------------------------------------------*/
class CHoverButton : public CWindow
{
public:
	CHoverButton(HWND hParent, short command_id, DWORD dwStyle, LPCSTR text,
		HINSTANCE hInstance, short x, short y, short width, short height);
	~CHoverButton();

public:
	BOOL IsHover() { return m_bHover; }
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	void OnMouseHover();
	void OnMouseLeave();
	void OnMouseMove();

private:
	WNDPROC m_OrginalProc;
	BOOL m_bTracking;
	BOOL m_bHover;
};
/*----------------------------------------------------------------------------------------------*/
#endif