#ifndef _JS_WINDOW_H
#define _JS_WINDOW_H
/*----------------------------------------------------------------------------------------------*/
#include <windows.h>
#include "..\..\main.h"
/*----------------------------------------------------------------------------------------------*/
LRESULT CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
/*----------------------------------------------------------------------------------------------*/
class CWindow
{
public:
	CWindow() : m_pApplication(0) {}

public:	// window functions
	BOOL Show(int nCmdShow) { return ShowWindow(hWnd, nCmdShow); }
	void Update() { UpdateWindow(hWnd); }

public:	// accessor functions
	HWND GetHandle() { return hWnd; }
	HWND SetHandle(HWND hwnd) {
		HWND oldwnd = hWnd;
		hWnd = hwnd;
		return oldwnd;
	}
	CApplication* GetApplication() { return m_pApplication; }
	void SetApplication(CApplication *pApp) {
		m_pApplication = pApp;
		m_pLogger = &pApp->m_LogFile;
	}

public:
	virtual long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam) = 0;

protected:
	HWND hWnd;
	HINSTANCE m_hInstance;
	CApplication *m_pApplication;
	CLogFile *m_pLogger;
};
/*----------------------------------------------------------------------------------------------*/
class CDialog : public CWindow
{
public:
	INT DoModal();
	INT DoModalDlg(char *resource_name, HWND hParent, HINSTANCE hInstance);

protected:
	void EndModel(INT result) { fDone = TRUE ; nResult = result; }

private:
	BOOL fDone;
	INT nResult;
};
/*----------------------------------------------------------------------------------------------*/
inline CWindow *GetPointer( HWND hWnd )
{
	 return (CWindow *) GetWindowLong( hWnd, 0 );
}
inline void SetPointer( HWND hWnd, CWindow *pWindow )
{
	 SetWindowLong( hWnd, 0, (LONG) pWindow );
}
/*----------------------------------------------------------------------------------------------*/
#endif
