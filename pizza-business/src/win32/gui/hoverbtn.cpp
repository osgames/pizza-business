#include "hoverbtn.h"
#include "commctrl.h"
/*----------------------------------------------------------------------------------------------*/
LONG SubClassProcButton(HWND hwnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	CHoverButton *pButton = (CHoverButton*)GetProp(hwnd, "HoverClass");
	return pButton->WndProc(iMessage, wParam, lParam);
}
/*----------------------------------------------------------------------------------------------*/
CHoverButton::CHoverButton(HWND hParent, short command_id, DWORD dwStyle, LPCSTR text,
						   HINSTANCE hInstance, short x, short y, short width, short height) :
m_bHover(FALSE), m_bTracking(FALSE)
{
	hWnd = CreateWindow("button", text,
		dwStyle,
		x, y, width, height,
		hParent, (HMENU)command_id, hInstance, NULL);

	SetProp(hWnd, "HoverClass", (HANDLE)this);

	m_OrginalProc = (WNDPROC)SetWindowLong(hWnd, GWL_WNDPROC, (LONG)SubClassProcButton);
}
/*----------------------------------------------------------------------------------------------*/
CHoverButton::~CHoverButton()
{
	RemoveProp(hWnd, "HoverClass");
	DestroyWindow(hWnd);
}
/*----------------------------------------------------------------------------------------------*/
void CHoverButton::OnMouseHover()
{
	m_bHover = TRUE;
	InvalidateRect(hWnd, 0, 0);
}
/*----------------------------------------------------------------------------------------------*/
void CHoverButton::OnMouseLeave()
{
	m_bTracking = FALSE;
	m_bHover = FALSE;
	InvalidateRect(hWnd, 0, 0);
}
/*----------------------------------------------------------------------------------------------*/
void CHoverButton::OnMouseMove()
{
	if (!m_bTracking)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = hWnd;
		tme.dwFlags = TME_LEAVE|TME_HOVER;
		tme.dwHoverTime = 1;
		m_bTracking = _TrackMouseEvent(&tme);
	}
}
/*----------------------------------------------------------------------------------------------*/
long CHoverButton::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
	case WM_MOUSEMOVE:
		this->OnMouseMove();
		break;

	case WM_MOUSEHOVER:
		this->OnMouseHover();
		break;

	case WM_MOUSELEAVE:
		this->OnMouseLeave();
		break;
	}
	return CallWindowProc(this->m_OrginalProc, hWnd, iMessage, wParam, lParam);
}
/*----------------------------------------------------------------------------------------------*/
