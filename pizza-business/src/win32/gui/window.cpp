#include "window.h"
/*----------------------------------------------------------------------------------------------*/
LRESULT CALLBACK DlgProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	CWindow *pWindow = (CWindow*)GetWindowLong(hWnd, DWL_USER);
	if(pWindow == 0)
	{
		if(iMessage == WM_INITDIALOG)
		{
			// add support for the dialogs
			pWindow = (CWindow*)lParam;
			pWindow->SetHandle(hWnd);
			SetWindowLong(hWnd, DWL_USER, (LONG)lParam);
			return pWindow->WndProc(iMessage, wParam, lParam);
		}
		else
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	else
		return pWindow->WndProc(iMessage, wParam, lParam);
}
/*----------------------------------------------------------------------------------------------*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	CWindow *pWindow = GetPointer(hWnd);
	if(pWindow == 0)
	{
		if(iMessage == WM_CREATE)
		{
			LPCREATESTRUCT lpcs;
			lpcs = (LPCREATESTRUCT)lParam;
			pWindow = (CWindow *)lpcs->lpCreateParams;
			SetPointer(hWnd, pWindow);
			return pWindow->WndProc(iMessage, wParam, lParam);
		}
		else
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	else
		return pWindow->WndProc(iMessage, wParam, lParam);
}
/*----------------------------------------------------------------------------------------------*/
INT CDialog::DoModal()
{
	HWND hwndParent = GetParent(this->GetHandle());

	if(hwndParent != NULL)
		EnableWindow(hwndParent,FALSE);

	MSG msg;
	for(fDone = FALSE ; !fDone ; WaitMessage())
	{
		while(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{
				fDone = TRUE;
				PostMessage(NULL,WM_QUIT,0,0);
				break;
			}

			if(!IsDialogMessage(this->GetHandle(), &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	if(hwndParent != NULL)
		EnableWindow(hwndParent,TRUE);

	DestroyWindow(hWnd);
	return nResult;
}
/*----------------------------------------------------------------------------------------------*/
INT CDialog::DoModalDlg(char *resource_name, HWND hParent, HINSTANCE hInstance)
{
	this->m_hInstance = hInstance;
	return DialogBoxParam(hInstance, resource_name, hParent, (DLGPROC)::DlgProc, (LPARAM)(CWindow*)this);
}
/*----------------------------------------------------------------------------------------------*/