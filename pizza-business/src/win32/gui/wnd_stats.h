#ifndef _JS_WND_STATS_H
#define _JS_WND_STATS_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "..\..\datarest.h"
/*----------------------------------------------------------------------------------------------*/
class CDayStatsDlg : public CDialog
{
public:
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

public:
	void SetDayStats(DayStats *pStats) { m_pStats = pStats; }

private:
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnInitDialog();

private:
	DayStats *m_pStats;
};
/*----------------------------------------------------------------------------------------------*/
#endif