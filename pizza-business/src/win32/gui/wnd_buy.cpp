#include "wnd_buy.h"
#include <commctrl.h>
/*----------------------------------------------------------------------------------------------*/
#define IDC_BUYSELL_OK		1
#define IDC_BUYSELL_CANCEL	2
#define IDC_BUYSELL_TAB		101
#define IDC_BUYSELL_LIST	102
#define IDC_BUYSELL_BUY		103
#define IDC_BUYSELL_SELL	104

#define INDEX_INGREDIENTS	0
#define INDEX_CHAIRS		1
#define INDEX_OVENS			2
#define INDEX_TABLES		3
#define INDEX_ADS			4

#define IDC_CATALOG_BUY		1
#define IDC_CATALOG_CANCEL	2
#define IDC_CATALOG_LIST	101
#define IDC_CATALOG_COUNT	102
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpAdvertisements()
{
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_AdsList.GetCount() ; c++)
	{
		AdItem *pItem = this->m_pRestaurant->m_AdsList.GetItem(c);

		char text[280], desc[256];
		pItem->pAd->GetDescription(desc);
		sprintf(text, "(%d) %s", pItem->quantity, desc);

		int index = SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_ADDSTRING, NULL,
			(LPARAM)(LPCSTR)text);
		SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_SETITEMDATA, index, (LPARAM)pItem);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpChairs()
{
	// NOTE: there is only type of chair, so we are just going
	// to display the number of chairs at the restaurant
	unsigned int chair_count = this->m_pRestaurant->GetChairCount();
	if(chair_count < 1)
		return;

	char text[256];
	sprintf(text, "(%d) Generic Chair", chair_count);

	int index = SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_ADDSTRING, NULL,
			(LPARAM)(LPCSTR)text);
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpIngredients()
{
	// dump all the ingredients into the listbox
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_pRestaurant->m_IngList.GetItem(c);
		for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIng = pIngCat->ingredients.GetItem(i);

			unsigned int quantity = pIng->IngList.GetCount();
			if(quantity < 1)
				continue;
			
			// add the ingredient to the listbox
			char text[280], ing_name[256];
			pIng->pIngredient->GetName(ing_name);
			sprintf(text, "(%d) %s", quantity, ing_name);

			int index = SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_ADDSTRING, NULL,
				(LPARAM)(LPCSTR)text);
			SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_SETITEMDATA, index, (LPARAM)pIng);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpOvens()
{
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_OvenList.GetCount() ; c++)
	{
		COven *pOven = this->m_pRestaurant->m_OvenList.GetItem(c);
		
		char name[256];
		pOven->GetName(name);

		int index = SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_ADDSTRING, NULL,
				(LPARAM)(LPCSTR)name);
			SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_SETITEMDATA, index, (LPARAM)pOven);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::LB_DumpTables()
{
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_TableList.GetCount() ; c++)
	{
		CTable *pTable = this->m_pRestaurant->m_TableList.GetItem(c);
		
		char name[256];
		pTable->GetName(name);

		int index = SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_ADDSTRING, NULL,
				(LPARAM)(LPCSTR)name);
			SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_SETITEMDATA, index, (LPARAM)pTable);
	}
}
/*----------------------------------------------------------------------------------------------*/
LONG CBuySellDlg::GetSelItemData()
{
	HWND hwndLB = GetDlgItem(hWnd, IDC_BUYSELL_LIST);
	
	// get the index to the currently selected item
	// in the listbox
	int iSel = SendMessage(hwndLB, LB_GETCURSEL, NULL, NULL);
	if(iSel == LB_ERR)
		return 0;

	// get the data associated with that item
	LONG data = (LONG)SendMessage(hwndLB, LB_GETITEMDATA, iSel, NULL);
	
	if(data == LB_ERR)
		return 0;
	else
		return data;
}
/*----------------------------------------------------------------------------------------------*/
short CBuySellDlg::GetTabIndex()
{
	return TabCtrl_GetCurSel(GetDlgItem(hWnd, IDC_BUYSELL_TAB));
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case IDC_BUYSELL_OK:
		case IDC_BUYSELL_CANCEL:
			EndDialog(hWnd, FALSE);
			break;

		case IDC_BUYSELL_BUY:
			{
				CCatalogDlg *pDlg = 0;
				short id = this->GetTabIndex();
				switch(id)
				{
					case INDEX_OVENS:
						{
							COvenCatDlg dlg(*this->m_pOvenList);
							pDlg = &dlg;
						}
						break;
					case INDEX_ADS:
						{
							CAdsCatDlg dlg(*this->m_pAdsList);
							pDlg = &dlg;
						}
						break;
					case INDEX_TABLES:
						{
							CTableCatDlg dlg(*this->m_pTableList);
							pDlg = &dlg;
						}
						break;
					case INDEX_INGREDIENTS:
						{
							CIngCatDlg dlg(*this->m_pIngList);
							pDlg = &dlg;
						}
						break;
					case INDEX_CHAIRS:
						{
							CChairCatDlg dlg(this->m_chair_price);
							pDlg = &dlg;
						}
						break;
				}
				if(pDlg)
				{
					pDlg->SetRestaurant(this->m_pRestaurant);
					pDlg->DoModalDlg("CAT_BUY", hWnd, this->m_hInstance);
					this->RefreshListBox(); // update the listbox
					this->RefreshCaption();
				}
			}
			break;

		case IDC_BUYSELL_SELL:
			this->OnCommand_Sell();
			this->RefreshListBox(); // update the listbox
			this->RefreshCaption();
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnCommand_Sell()
{
	short iSel = this->GetTabIndex();
	switch(iSel)
	{
	case INDEX_INGREDIENTS:
		{
			CSellEditDlg dlg;

			Ingredient *pIng = (Ingredient*)this->GetSelItemData();
			if(pIng)
			{
				// construct and set the description
				char text[256], ing_name[256];
				unsigned int count = pIng->IngList.GetCount();
				pIng->pIngredient->GetName(ing_name);
				sprintf(text, "Enter the number of %s to sell. You have a total of %d %s.",
					ing_name, count, ing_name);
				dlg.SetDescription(text);

				// TRUE if use has pressed the "Sell" (Ok) button
				if(dlg.DoModalDlg("CAT_SELL_ED", hWnd, this->m_hInstance) == TRUE)
				{
					int number = dlg.GetNumber();
					this->m_pRestaurant->Ingredient_Sell(pIng->pIngredient, number);
				}
			}
		}
		break;

	case INDEX_CHAIRS:
		{
			CSellEditDlg dlg;

			// construct and set the description
			char text[256];
			sprintf(text, "Enter the number of chairs to sell. You have a total of %d chairs.", 
				this->m_pRestaurant->GetChairCount());
			dlg.SetDescription(text);
			// TRUE if use has pressed the "Sell" (Ok) button
			if(dlg.DoModalDlg("CAT_SELL_ED", hWnd, this->m_hInstance) == TRUE)
			{
				int number = dlg.GetNumber();
				this->m_pRestaurant->SellChair(number, this->m_chair_price);
			}
		}
		break;

	case INDEX_OVENS:
		{
			COven *pOven = (COven*)this->GetSelItemData();
			if(pOven)
				this->m_pRestaurant->SellOven(pOven);
		}
		break;

	case INDEX_TABLES:
		{
			CTable *pTable = (CTable*)this->GetSelItemData();
			if(pTable)
				this->m_pRestaurant->SellTable(pTable);
		}
		break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnInitDialog()
{
	SetWindowText(hWnd, "Still needs a good caption");

	if(!this->m_pOvenList || !this->m_pTableList || !this->m_pRestaurant)
	{
		EndDialog(hWnd, FALSE);
		return;
	}

	HWND hwndTab = GetDlgItem(hWnd, IDC_BUYSELL_TAB);

	// set the size of the tab buttons
	TabCtrl_SetItemSize(hwndTab, 50, 75);

	// create the tab buttons
	TCITEM tci;
	tci.mask = TCIF_TEXT;

	tci.pszText = "Ingredients";
	TabCtrl_InsertItem(hwndTab, INDEX_INGREDIENTS, &tci);

	tci.pszText = "Chairs";
	TabCtrl_InsertItem(hwndTab, INDEX_CHAIRS, &tci);

	tci.pszText = "Ovens";
	TabCtrl_InsertItem(hwndTab, INDEX_OVENS, &tci);

	tci.pszText = "Tables";
	TabCtrl_InsertItem(hwndTab, INDEX_TABLES, &tci);

	tci.pszText = "Advertisements";
	TabCtrl_InsertItem(hwndTab, INDEX_ADS, &tci);

	this->RefreshListBox();
	this->RefreshCaption();
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::OnNotify(LPNMHDR lpHdr)
{
	if(lpHdr->code == TCN_SELCHANGE)
	{
		this->RefreshListBox();

		// disable/enable the sell button
		if(this->GetTabIndex() == INDEX_ADS)
			EnableWindow(GetDlgItem(hWnd, IDC_BUYSELL_SELL), FALSE);
		else
			EnableWindow(GetDlgItem(hWnd, IDC_BUYSELL_SELL), TRUE);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::RefreshCaption()
{
	short id = this->m_pRestaurant->GetId();
	double budget = this->m_pRestaurant->GetBudget();

	char text[280];
	sprintf(text, "Restaurant #%d ($%.2f)", id, budget);
	SetWindowText(hWnd, text);
}
/*----------------------------------------------------------------------------------------------*/
void CBuySellDlg::RefreshListBox()
{
	SendDlgItemMessage(hWnd, IDC_BUYSELL_LIST, LB_RESETCONTENT, NULL, NULL);

	short index = this->GetTabIndex();
	switch(index)
	{
		case INDEX_ADS:
			this->LB_DumpAdvertisements();
			break;

		case INDEX_CHAIRS:
			this->LB_DumpChairs();
			break;

		case INDEX_INGREDIENTS:
			this->LB_DumpIngredients();
			break;

		case INDEX_OVENS:
			this->LB_DumpOvens();
			break;

		case INDEX_TABLES:
			this->LB_DumpTables();
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
long CBuySellDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

		case WM_NOTIFY:
			this->OnNotify((LPNMHDR)lParam);
			break;

	}
	return 0;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CCatalogDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
LPARAM CCatalogDlg::GetSelItem()
{
	// get the selected index in the listbox
	int iSel = SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_GETCURSEL, NULL, NULL);
	
	if(iSel == LB_ERR)
		return 0;

	return SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_GETITEMDATA, iSel, NULL);	
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CCatalogDlg::GetQuantity()
{
	// get the quantity
	char text[10];
	GetWindowText(GetDlgItem(hWnd, IDC_CATALOG_COUNT), text, 10);
	unsigned int quantity = atoi(text);

	return quantity;
}
/*----------------------------------------------------------------------------------------------*/
void CCatalogDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case IDC_CATALOG_CANCEL:
			EndDialog(hWnd, FALSE);
			break;

		case IDC_CATALOG_BUY:
			this->OnBuySelected();
			EndDialog(hWnd, TRUE);
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CCatalogDlg::OnInitDialog()
{
	SetWindowText(hWnd, "Still needs a good caption");

	SetWindowText(GetDlgItem(hWnd, IDC_CATALOG_COUNT), "0");
	this->OnInitListBox();
}
/*----------------------------------------------------------------------------------------------*/
long CCatalogDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CAdsCatDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CAdsCatDlg::OnInitListBox()
{
	if(!this->m_pAdsList) return;

	// add all the advertisements to the listbox
	for(unsigned int c = 1 ; c <= this->m_pAdsList->GetCount() ; c++)
	{
		CAdCategory *pCategory = this->m_pAdsList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCategory->AdList.GetCount() ; i++)
		{
			CAdvertisement *pAd = pCategory->AdList.GetItem(i);

			char desc[256];
			pAd->GetDescription(desc);

			int index = SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)desc);
			SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_SETITEMDATA, index, (LPARAM)pAd);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CAdsCatDlg::OnBuySelected()
{
	// get the quantity
	unsigned int quantity = this->GetQuantity();
	// get the pinter to the CAd object
	CAdvertisement *pAd = (CAdvertisement*)this->GetSelItem();

	if(pAd && quantity > 0)
		this->m_pRestaurant->Advertisement_Buy(pAd, quantity);
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CChairCatDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CChairCatDlg::OnInitListBox()
{
	// NOTE: as of now, we only have one generic chair

	// add chairs to the listbox
	char text[256];
	sprintf(text, "Generic Chair");
	int index = SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_ADDSTRING, 0, 
		(LPARAM)(LPCSTR) text);
}
/*----------------------------------------------------------------------------------------------*/
void CChairCatDlg::OnBuySelected()
{
	// NOTE: as of now, we only have one generic chair,
	// so we will just use the price of the one chair

	unsigned int quantity = this->GetQuantity();

	if(quantity > 0)
		this->m_pRestaurant->BuyChair(quantity, this->m_chair_price);
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CIngCatDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CIngCatDlg::OnInitListBox()
{
	if(!this->m_pIngList) return;

	// add all the advertisements to the listbox
	for(unsigned int c = 1 ; c <= this->m_pIngList->GetCount() ; c++)
	{
		CIngCategory *pCategory = this->m_pIngList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCategory->IngList.GetCount() ; i++)
		{
			CIngredient *pIng = pCategory->IngList.GetItem(i);

			char name[256];
			pIng->GetName(name);

			int index = SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)name);
			SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_SETITEMDATA, index, (LPARAM)pIng);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CIngCatDlg::OnBuySelected()
{
	// get the pointer to the CIngredient object
	CIngredient *pIng = (CIngredient*)this->itemList->GetClientData(this->itemList->GetSelection());
	
	// get the quantity
	unsigned int quantity = this->GetQuantity();

	if(pIng && quantity > 0)
		this->m_pRestaurant->Ingredient_Buy(pIng, quantity);
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	COvenCatDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void COvenCatDlg::OnInitListBox()
{
	if(!this->m_pOvenList) return;

	// add all the ovens to the listbox
	for(unsigned int c = 1 ; c <= this->m_pOvenList->GetCount() ; c++)
	{
		COven *pOven = this->m_pOvenList->GetItem(c);

		char name[256];
		pOven->GetName(name);

		int index = SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)name);
		SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_SETITEMDATA, index, (LPARAM)pOven);
	}
}
/*----------------------------------------------------------------------------------------------*/
void COvenCatDlg::OnBuySelected()
{
	// get the quantity
	unsigned int quantity = this->GetQuantity();
	// get the pinter to the COven object
	COven *pOven = (COven*)this->GetSelItem();

	if(pOven && quantity > 0)
	{
		for(unsigned int c = 0 ; c < quantity ; c++)
			this->m_pRestaurant->BuyOven(pOven);
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CTabCatDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CTableCatDlg::OnInitListBox()
{
	if(!this->m_pTableList) return;

	// add all the ovens to the listbox
	for(unsigned int c = 1 ; c <= this->m_pTableList->GetCount() ; c++)
	{
		CTable *pTable = this->m_pTableList->GetItem(c);

		char name[256];
		pTable->GetName(name);

		int index = SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)name);
		SendDlgItemMessage(hWnd, IDC_CATALOG_LIST, LB_SETITEMDATA, index, (LPARAM)pTable);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CTableCatDlg::OnBuySelected()
{
	// get the quantity
	unsigned int quantity = this->GetQuantity();
	// get the pinter to the CTable object
	CTable *pTable = (CTable*)this->GetSelItem();

	if(pTable && quantity > 0)
	{
		for(unsigned int c = 0 ; c < quantity ; c++)
			this->m_pRestaurant->BuyTable(pTable);
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CSellEditDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CSellEditDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case 1:
			{
				// set the number variable
				char text[10];
				GetWindowText(GetDlgItem(hWnd, 102), text, 9);
				this->m_Number = atoi(text);

				EndDialog(hWnd, TRUE);
			}
			break;

		case 2:
			EndDialog(hWnd, FALSE);
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSellEditDlg::OnInitDialog()
{
	// set the window's caption
	SetWindowText(hWnd, "Sell how many?");

	// set the number in the editbox
	char text[5];
	itoa(this->m_Number, text, 10);
	SetWindowText(GetDlgItem(hWnd, 102), text);

	// set the description
	SetWindowText(GetDlgItem(hWnd, 101), this->m_Desc);
}
/*----------------------------------------------------------------------------------------------*/
void CSellEditDlg::SetDescription(char* desc)
{
	strcpy(this->m_Desc, desc);
	SetWindowText(GetDlgItem(hWnd, 101), this->m_Desc);
}
/*----------------------------------------------------------------------------------------------*/
long CSellEditDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CSellListDlg Implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
void CSellListDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case 1:
		case 2:
			EndDialog(hWnd, FALSE);
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CSellListDlg::OnInitDialog()
{

}
/*----------------------------------------------------------------------------------------------*/
long CSellListDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
