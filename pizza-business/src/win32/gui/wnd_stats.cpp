#include "wnd_stats.h"
/*----------------------------------------------------------------------------------------------*/
void CDayStatsDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(wParam))
	{
		case 1:
		case 2:
			EndDialog(hWnd, FALSE);
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CDayStatsDlg::OnInitDialog()
{
	if(!this->m_pStats) return;

	char caption [32];
	sprintf(caption, "Day statistics for turn #%d", m_pStats->turn_id + 1);
	SetWindowText(hWnd, caption);

	// fill in the static text control
	// this will be temporary for the release
	char text[1024];
	sprintf(text, "Money Income: $%.2f\n\n"
				  "Pizzas Sold: %d\n\n"
				  "Customer visits: %d\n\n"
				  "Customers left: %d\n\n"
				  "Popularity: %.2f",
		m_pStats->money_made, m_pStats->pizzas_sold, m_pStats->customers_enter,
		m_pStats->customers_longwait+m_pStats->customers_nospace, m_pStats->popularity);
	SetWindowText(GetDlgItem(hWnd, 101), text);
}
/*----------------------------------------------------------------------------------------------*/
long CDayStatsDlg::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch(iMessage)
	{
		case WM_INITDIALOG:
			this->OnInitDialog();
			break;

		case WM_COMMAND:
			this->OnCommand(wParam, lParam);
			break;

	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/