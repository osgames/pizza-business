#ifndef _JS_MENUWND_H
#define _JS_MENUWND_H
/*----------------------------------------------------------------------------------------------*/
#include "window.h"
#include "hoverbtn.h"
/*----------------------------------------------------------------------------------------------*/
#define IDC_NEWGAME		501
#define IDC_LOADGAME	502
#define IDC_SAVEGAME	503
#define IDC_CREDITS		504
#define IDC_EXIT		505
#define IDC_RESUME		506
/*----------------------------------------------------------------------------------------------*/
class CMenuWindow : public CDialog
{
public: // construction/destruction
	CMenuWindow(HINSTANCE hInstance, bool bEnableResume = false);
	~CMenuWindow();

public:
	INT DoModalDialog(HWND hParent);
	long WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);

private: // window message handlers
	void OnCommand(WPARAM wParam, LPARAM lParam);
	void OnDrawItem(LPDRAWITEMSTRUCT lpdis);
	void OnPaint();

private:
	void Register();

private: // window stuff
	static char szClassName[41];
	static bool registered;
	HINSTANCE m_hInstance;
	// buttons
	CHoverButton *m_pBtnNewGame;
	CHoverButton *m_pBtnLoadGame;
	CHoverButton *m_pBtnSaveGame;
	CHoverButton *m_pBtnResume;
	CHoverButton *m_pBtnQuit;

	bool m_bEnableResume;

private: // GDI stuff
	HDC memDC;
	HBITMAP hBmp, oldBmp;
};
/*----------------------------------------------------------------------------------------------*/
#endif