#ifndef _JS_DATAREST_H
#define _JS_DATAREST_H
/*----------------------------------------------------------------------------------------------*/
#include "action.h"
#include "buysell.h"
#include "staff.h"
#include "furniture.h"
#include "pizzas.h"
#include "bank.h"

#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class CGameEngine;
class CRestaurant;
/*----------------------------------------------------------------------------------------------*/
class COwner
{
friend CGameEngine;

public:
	COwner();

public:
	// name
	void GetName(char *buffer) { strcpy(buffer, m_Name); }
	void SetName(char *name) { strcpy(m_Name, name); }

public:
	unsigned int GetRestaurantCount() { return m_StoreList.GetCount(); }
	unsigned int AddRestaurant(CRestaurant *pRestaurant);
	CRestaurant* GetRestaurant(unsigned int index = 1) { return m_StoreList.GetItem(index); }

	// budget
	double GetBudget() { return m_Budget; }
	double SetBudget(double budget) { return (m_Budget = budget); }

private:
	List<CRestaurant> m_StoreList;

protected:
	char m_Name[256];
	double m_Budget;
};
/*----------------------------------------------------------------------------------------------*/
struct DayStats {
	unsigned int pizzas_sold;
	unsigned int customers_enter;								// # cust visits
	unsigned int customers_nospace;								// # cust leave (no empty chairs)
	unsigned int customers_longwait;							// # cust leave (order too slow)
	double popularity;											// popularity after the simulation
	unsigned int turn_id;
	double money_made;
};
/*----------------------------------------------------------------------------------------------*/
class CRestaurant
{
friend CGameEngine;

public:	// construction/destruction
	CRestaurant(COwner *pOwner);
	~CRestaurant() {}

public: // data access
	COwner* GetOwner() { return m_pOwner; }
	unsigned short GetId() { return m_Id; }
	void SetId(unsigned short id) { m_Id = id; }

	void GetName(char *buffer) { strcpy(buffer, m_Name); }
	void SetName(char *name) { strcpy(m_Name, name); }

	// budge stuff
	double AddToBudget(double addition) { return (m_Budget += addition); }
	double GetBudget() { return m_Budget; }
	double SetBudget(double budget) { return (m_Budget = budget); }
	double SubractFromBudget(double subtraction) { return (m_Budget -= subtraction); }

	// staff stuff
	void GetEmployees(List<CEmployee> *pList, Emp_Title title = TITLE_IRRELEVANT);
	void FireEmployee(CEmployee *pEmployee);
	void HireEmployee(CEmployee *pEmployee, Emp_Title title);

	// pricing
	void EnableAutoPricing(bool enable) { m_bEnableAutoPricing = enable; }
	bool IsAutoPricingEnabled() { return m_bEnableAutoPricing; }
	short GetAutoPricePercentage() { return m_AutoPrice; }
	void SetAutoPricePercentage(short percentage) { m_AutoPrice = percentage; }

	// popularity stuff
	double GetCurrentPopularity();

	// chair stuff
	unsigned int GetChairCount() { return m_NumChairs; }
	unsigned int GetChairSupportCount();

	// furniture stuff
	void BuyChair(int quantity, float price);
	void BuyOven(COven *pOven);
	void BuyTable(CTable *pTable);
	void SellChair(int quantity, float fChairPrice);
	void SellOven(COven *pOven);
	void SellTable(CTable *pTable);

	// shop status stuff
	bool IsShopOpen() { return shop_open; }
	void CloseShop() { shop_open = false; }
	void OpenShop() { shop_open = true; }

	// advertisment stuff
	void Advertisement_Buy(CAdvertisement *pAd, unsigned int quantity);

	// ingredient stuff
	unsigned int GetIngTypeCount(short id);
	void Ingredient_Buy(CIngredient *pIng, unsigned int quantity);
	void Ingredient_Sell(CIngredient *pIng, unsigned int quantity);
	void Ingredient_Use(CIngredient *pIng);
	void InitIngredientList(List<CIngCategory> *pCatList);
	
	// pizza stuff
	void AddPizzaToList(CPizza *pPizza);
	bool BuyPizza(CPizzaObject *pPizza);
	bool CookPizza(CPizzaObject *pPizza);
	void InitPizzaList(List<CPizza> *pPizzaList);
	bool IsPizzaSupported(CPizzaObject *pPizza);
	void Toppings_SetSelectable(bool enable) { m_ToppingsSelectable = enable; }
	bool Toppings_IsSelectable() { return m_ToppingsSelectable; }

	// misc
	void DoEndTurnStuff();										// called from simulation

private: // functions
	void SetOwner(COwner* pOwner) { m_pOwner = pOwner; }
	void SetChairCount(unsigned int count) { m_NumChairs = count; }

	void AddAdvertismentToList(CAdvertisement *pAd, unsigned int quantity);
	Ingredient* AddIngredientToList(CIngredient *pIngredient, unsigned int quantity);

	// called from DoEndTurnStaff
	void DecrementAds();										// removes one ad from the list
	void DecrementIngLife();									// ingredient experation function
	void SubtractPayroll();										// pay the employees

public:	// data
	List<AdItem> m_AdsList;										// list of ads purchased
	List<CEmployee> m_EmpList;									// list of employeed staff
	List<IngCat> m_IngList;										// list of ingredients purchased
	List<CTable> m_TableList;
	List<COven> m_OvenList;
	List<CPizzaObject> m_PizzaList;

	CBankAccount *m_pBankAccount;

	// NOTE: this list could get pretty long
	// i think it would be a good idea to keep only
	// the stats for the previous week (7 max). The
	// rest could be kept in a temporary file or deleted.
	List<DayStats> m_StatsList;

private: // data
	unsigned short m_Id;
	COwner* m_pOwner;
	char m_Name[256];
	double m_Budget;
	unsigned int m_NumChairs;
	bool shop_open;
	bool m_ToppingsSelectable;
	bool m_bEnableAutoPricing;
	short m_AutoPrice;											// price percentage increase
};
/*----------------------------------------------------------------------------------------------*/
#endif
