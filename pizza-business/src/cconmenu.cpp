#include <cstdio>
#include <cstdlib>
#include "interface.h"
#include "cconmenu.h"
#include "conutils.h"
using namespace std;
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CMenu implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CMenu::CMenu()
{

}
/*----------------------------------------------------------------------------------------------*/
CMenu::CMenu(unsigned int menu_id)
{
	this->SetMenuID(menu_id);
}
/*----------------------------------------------------------------------------------------------*/
CMenu::~CMenu()
{

}
/*----------------------------------------------------------------------------------------------*/
void CMenu::AddMenuItem(unsigned int id, char* text)
{
	// create a new menu item; set data
	MenuItem *pNewItem = new MenuItem;
	pNewItem->commandID = id;
	strcpy(pNewItem->text, text);

	// add item to list
	this->m_MenuList.AddItem(pNewItem);
}
/*----------------------------------------------------------------------------------------------*/
void CMenu::DeleteItemByID(unsigned int id)
{
	// search for matching id
	for(int c = 1 ; c <= this->m_MenuList.GetCount() ; c++)
	{
		MenuItem* pItem = this->m_MenuList.GetItem(c);
		
		// if id is found, delete it from list and exit loop
		if(pItem->commandID == id)
		{
			this->m_MenuList.DeleteItem(c);
			break;
		}
	}	
}
/*----------------------------------------------------------------------------------------------*/
MenuItem* CMenu::DisplayMenu(char* prompt)
{
	// display the menu list
	for(int c = 1 ; c <= this->m_MenuList.GetCount() ; c++)
	{
		MenuItem* pItem = this->m_MenuList.GetItem(c);
		cout << pItem->commandID << ". " << pItem->text << endl;
	}
	
	// display prompt and get user input
	char reply[3] = { '\0' };
	PromptUser(prompt, reply, 3);

	MenuItem *pItem = this->GetMenuItemByID(atoi(reply));

	return pItem;
}
/*----------------------------------------------------------------------------------------------*/
MenuItem* CMenu::GetMenuItemByID(unsigned int id)
{
	for(int c = 1 ; c <= this->m_MenuList.GetCount() ; c++)
	{
		MenuItem* pItem = this->m_MenuList.GetItem(c);
		if(pItem->commandID == id)
			return pItem;
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/