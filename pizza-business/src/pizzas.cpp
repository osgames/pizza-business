#include "pizzas.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CPizza implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CPizza::CPizza()
{
	// do not delete the ingredients in the list
	// cause they are pointers to the ingredient
	// listed in the data pool
	m_IngList.SetDeleteFlag(false);
}
/*----------------------------------------------------------------------------------------------*/
bool CPizza::AddIngredient(CIngredient *pIngredient)
{
	if(!pIngredient) return false;

	// be sure the ingredient is not already in the list
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		CIngredient *pTestIng = this->m_IngList.GetItem(c);
		if(pIngredient->GetId() == pTestIng->GetId())
			return false;
	}

	// now just append the ingredient to the list
	this->m_IngList.AddItem(pIngredient);
	return true;
}
/*----------------------------------------------------------------------------------------------*/
CIngredient* CPizza::GetIngredient(unsigned int index)
{
	return this->m_IngList.GetItem(index);
}
/*----------------------------------------------------------------------------------------------*/
float CPizza::GetProductionCost()
{
	// add up the prices for all the ingredients
	// in the ingredient list

	float total_cost = 0.0f;

	unsigned int count = this->GetIngredientCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngredient *pIngredient = this->GetIngredient(c);
		total_cost += pIngredient->GetPrice();
	}

	return total_cost;
}
/*----------------------------------------------------------------------------------------------*/
void CPizza::RemoveAllIngredients()
{
	this->m_IngList.DeleteAllItems();
}
/*----------------------------------------------------------------------------------------------*/