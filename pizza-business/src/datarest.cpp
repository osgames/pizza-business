#include "datarest.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	COwner implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
COwner::COwner()
{
	this->SetName("");
	this->SetBudget(0.0f);
}
/*----------------------------------------------------------------------------------------------*/
unsigned int COwner::AddRestaurant(CRestaurant *pRestaurant)
{
	this->m_StoreList.AddItem(pRestaurant);
	return this->m_StoreList.GetCount();
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CRestaurant implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CRestaurant::CRestaurant(COwner *pOwner)
{
	this->SetOwner(pOwner);
	this->SetName("");
	this->SetBudget(10000.0);
	this->m_NumChairs = 0;
	this->Toppings_SetSelectable(false);
	this->SetAutoPricePercentage(5);
	m_bEnableAutoPricing = true;

	// employees removed from list should not be deleted
	this->m_EmpList.SetDeleteFlag(false);

	// tables removed from the list should not be deleted
	this->m_TableList.SetDeleteFlag(false);

	// ovens removed from the list should not be deleted
	this->m_OvenList.SetDeleteFlag(false);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::AddAdvertismentToList(CAdvertisement *pAd, unsigned int quantity)
{
	// find the ad in the list and add to the count
	for(unsigned int c = 1 ; c <= this->m_AdsList.GetCount() ; c++)
	{
		AdItem *pTestAd = this->m_AdsList.GetItem(c);
		if(pTestAd->pAd == pAd)
		{
			pTestAd->quantity += quantity;
			return;
		}
	}

	// if still in the function, then add a new AdItem to the list
	AdItem *pNewItem = new AdItem;
	pNewItem->pAd = pAd;
	pNewItem->quantity = quantity;
	this->m_AdsList.AddItem(pNewItem);
}
/*----------------------------------------------------------------------------------------------*/
Ingredient* CRestaurant::AddIngredientToList(CIngredient *pIng, unsigned int quantity)
{
	// find the category
	CIngCategory *pCat = pIng->GetCategory();
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);
		if(pIngCat->pCategory == pCat)
		{
			// now find the ingredient
			for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
			{
				Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
				
				if(pIngredient->pIngredient == pIng)
				{
					// create new IngItem and add them to the list
					for(unsigned int t = 0 ; t < quantity ; t++)
					{
						IngItem *pItem = new IngItem;
						pIngredient->IngList.AddItem(pItem);
						pItem->life_left = pIng->GetQuality();
					}
					return pIngredient;
				}
			}
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::AddPizzaToList(CPizza *pPizza)
{
	if(!pPizza) return;

	CPizzaObject *pObject = new CPizzaObject(pPizza);
	this->m_PizzaList.AddItem(pObject);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Advertisement_Buy(CAdvertisement *pAd, unsigned int quantity)
{
	// be sure the budget is enough
	double total_price = (double)pAd->GetPrice() * (double)quantity;
	if(this->GetBudget() < total_price)
		return;

	this->AddAdvertismentToList(pAd, quantity);
	this->SubractFromBudget(total_price);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::BuyChair(int quantity, float price)
{
	// be sure the budget is enough
	if(this->GetBudget() < (double)price * (double)quantity)
		return;

	// be sure quanity is within the table support
	if(this->GetChairCount() + quantity > this->GetChairSupportCount())
		return;

	// add to the chair count
	this->m_NumChairs += quantity;

	// subtract from budget
	this->SubractFromBudget((double)price * (double)quantity);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::BuyOven(COven *pOven)
{
	// be sure budget is enough
	if(this->GetBudget() < (double)pOven->GetPrice())
		return;

	// add item to list
	this->m_OvenList.AddItem(pOven);

	// subtract from budget
	this->SubractFromBudget(pOven->GetPrice());
}
/*----------------------------------------------------------------------------------------------*/
bool CRestaurant::BuyPizza(CPizzaObject *pPizza)
{
	if(!pPizza) return false;

	// add the cost of the pizza to the budget
	this->AddToBudget(pPizza->GetPrice());
	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::BuyTable(CTable *pTable)
{
	// be sure budget is enough
	if(this->GetBudget() < (double)pTable->GetPrice())
		return;

	// add item to list
	this->m_TableList.AddItem(pTable);

	// subtract from budget
	this->SubractFromBudget(pTable->GetPrice());
}
/*----------------------------------------------------------------------------------------------*/
bool CRestaurant::CookPizza(CPizzaObject *pPizza)
{
	if(this->IsPizzaSupported(pPizza) == false)
		return false;

	// use the ingredients
	for(unsigned int c = 1 ; c <= pPizza->GetPizza()->GetIngredientCount() ; c++)
	{
		CIngredient *pIngredient = pPizza->GetPizza()->GetIngredient(c);
		this->Ingredient_Use(pIngredient);
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::DecrementAds()
{
	// subtract 1 from each advertisement quantity
	// if quantity equals 0, then remove ad from the list
	for(unsigned int c = 1 ; c <= this->m_AdsList.GetCount() ; c++)
	{
		AdItem *pAd = this->m_AdsList.GetItem(c);
		pAd->quantity--;
	
		if(pAd->quantity < 1)
		{
			this->m_AdsList.DeleteItem(c);
			c = 1;	// reset the counter
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::DecrementIngLife()
{
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pCat = this->m_IngList.GetItem(c);

		for(unsigned int i = 1 ; i <= pCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIng = pCat->ingredients.GetItem(i);

			// decrement the life of ingredient item
			for(unsigned int u = 1 ; u <= pIng->IngList.GetCount() ; u++)
			{
				IngItem *pItem = pIng->IngList.GetItem(u);
				pItem->life_left--;

				// delete item from list if life is less than 1
				if(pItem->life_left < 1)
				{
					pIng->IngList.DeleteItem(u);
					u--;
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::DoEndTurnStuff()
{
	this->SubtractPayroll();									// payroll
	this->DecrementAds();										// advertisements
	this->DecrementIngLife();									// ingredient's lifespan
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::FireEmployee(CEmployee *pEmployee)
{
	if(pEmployee == 0) return;
	// be sure employee is employeed
	if(pEmployee->m_EmployedAt == 0)
		return;

	pEmployee->m_EmployedAt = 0;
	pEmployee->ClearStatistics();
	pEmployee->SetDaysEmployeed(1);
	pEmployee->SetTitle(TITLE_UNEMPLOYED);

	// find matching pointer int the restaurant's employee list
	for(int c = 1 ; c <= this->m_EmpList.GetCount() ; c++)
	{
		if(pEmployee == this->m_EmpList.GetItem(c))
		{
			// found the index; delete item
			this->m_EmpList.DeleteItem(c);
			break;
		}
	}

}
/*----------------------------------------------------------------------------------------------*/
double CRestaurant::GetCurrentPopularity()
{
	// if first turn, popularity equals 0
	int count = this->m_StatsList.GetCount();
	if(count > 0)
	{
		DayStats *pStats = this->m_StatsList.GetItem(1);
		if(pStats)
			return pStats->popularity;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::GetEmployees(List<CEmployee> *pList, Emp_Title title)
{
	if(!pList) return;

	// be sure the delete flag is false for the list object
	pList->SetDeleteFlag(false);

	for(int c = 1 ; c <= this->m_EmpList.GetCount() ; c++)
	{
		CEmployee *pEmp = this->m_EmpList.GetItem(c);
		Emp_Title position = pEmp->GetTitle();
		
		if(title == TITLE_IRRELEVANT || title == position)
			pList->AddItem(pEmp);
	}
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CRestaurant::GetChairSupportCount()
{
	unsigned int count = 0;

	for(unsigned int c = 1 ; c <= this->m_TableList.GetCount() ; c++)
	{
		CTable *pTable = this->m_TableList.GetItem(c);

		count += pTable->GetChairsSupported();
	}

	return count;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CRestaurant::GetIngTypeCount(short id)
{
	// find the ingredient, return the count in inventory
	
	// 1st, find the category in the inventory
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);
		// 2nd, find the ingredient in thecategory
		for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
			
			// if the ingredient was found, return the count
			if(pIngredient->pIngredient->GetId() == id)
				return pIngredient->IngList.GetCount();
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::HireEmployee(CEmployee *pEmployee, Emp_Title title)
{
	if(pEmployee == 0) return;
	// be sure employee is unemployeed
	if(pEmployee->m_EmployedAt != 0)
		return;

	// if hiring a manager, be sure
	// there is not one already
	if(title == TITLE_MANAGER)
	{
		List<CEmployee> List;
		this->GetEmployees(&List, TITLE_MANAGER);
		if(List.GetCount() > 0)
			return;
	}

	// add employee to list; set employee data
	this->m_EmpList.AddItem(pEmployee);
	pEmployee->m_EmployedAt = this;
	pEmployee->SetTitle(title);
	
	// reset stats
	pEmployee->ClearStatistics();
	pEmployee->SetDaysEmployeed(0);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Ingredient_Buy(CIngredient *pIng, unsigned int quantity)
{
	if(pIng == 0) return;

	float expense = pIng->GetPrice() * (float)quantity;

	// be sure within budget
	if(this->GetBudget() >= expense)
	{
		this->AddIngredientToList(pIng, quantity);
		// sub from budget
		this->SubractFromBudget((double)expense);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Ingredient_Sell(CIngredient *pIng, unsigned int quantity)
{
	if(!pIng) return;

	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pCat = this->m_IngList.GetItem(c);
		if(pCat->pCategory == pIng->GetCategory())
		{
			// find the ingredient, delete the quantity of the product,
			// and credit the restaurant's account
			for(unsigned int i = 1 ; i <= pCat->ingredients.GetCount() ; i++)
			{
				Ingredient *pIngredient = pCat->ingredients.GetItem(i);
				if(pIngredient->pIngredient == pIng)
				{
					// delete the quantity number of ingredients
					if(quantity > pIngredient->IngList.GetCount())
						quantity = pIngredient->IngList.GetCount();

					double money_recieved = 0.0f;
					// the ingredients are arranged from oldest to the newest
					// so just the delete the ones at the beginning
					for(unsigned int d = 1 ; d <= quantity ; d++)
					{
						IngItem *pItem = pIngredient->IngList.GetItem(1);
						
						// calculate the money that will be recieved from the transaction
						float percentage = ((float)pItem->life_left / (float)pIngredient->pIngredient->GetQuality());
						money_recieved += (double)percentage * (double)pIngredient->pIngredient->GetPrice();
							
						pIngredient->IngList.DeleteItem(1);
					}

					// credit the restaurant's account
					this->AddToBudget(money_recieved);

					return; // no need to continue with function
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Ingredient_Use(CIngredient *pIng)
{
	if(pIng == 0) return;

	// find the ingredient
	// 1st, find the category in the inventory
	CIngCategory *pCat = pIng->GetCategory();
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);
		if(pIngCat->pCategory == pCat)
		{
			// 2nd, find the ingredient in thecategory
			for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
			{
				Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
				
				// if the ingredient was found, pop the last item off the list
				// since they should be arrange by newest first
				if(pIngredient->pIngredient->GetId() == pIng->GetId())
				{
					unsigned int count = pIngredient->IngList.GetCount();
					pIngredient->IngList.DeleteItem(count);
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::InitIngredientList(List<CIngCategory> *pCatList)
{
	if(pCatList == 0) return;

	// create the category; add it to the list
	for(unsigned int c = 1 ; c <= pCatList->GetCount() ; c++)
	{
		CIngCategory *pCategory = pCatList->GetItem(c);

		IngCat *pNewCat = new IngCat;
		this->m_IngList.AddItem(pNewCat);
		pNewCat->pCategory = pCategory;
	
		for(unsigned int i = 1 ; i <= pCategory->IngList.GetCount() ; i++)
		{
			CIngredient *pIngredient = pCategory->IngList.GetItem(i);

			Ingredient *pNewIng = new Ingredient;
			pNewCat->ingredients.AddItem(pNewIng);
			pNewIng->pIngredient = pIngredient;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::InitPizzaList(List<CPizza> *pPizzaList)
{
	for(unsigned int c = 1 ; c <= pPizzaList->GetCount() ; c++)
		this->AddPizzaToList(pPizzaList->GetItem(c));
}
/*----------------------------------------------------------------------------------------------*/
bool CRestaurant::IsPizzaSupported(CPizzaObject *pPizza)
{
	// go through the ingredients necessary to create the pizza
	// and if the restaurant does not have a certain ingredient
	// in stock, return false
	// else return true

	for(unsigned int c = 1 ; c <= pPizza->GetPizza()->GetIngredientCount() ; c++)
	{
		CIngredient *pIngredient = pPizza->GetPizza()->GetIngredient(c);
		if(this->GetIngTypeCount(pIngredient->GetId()) == false)
			return false;
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SellChair(int quantity, float fChairPrice)
{
	// delete quantity number of chairs

	unsigned int chair_count = this->GetChairCount();
	if(quantity > chair_count)
		quantity = chair_count;

	// delete the chairs
	unsigned int chairs_left = chair_count - quantity;
	this->SetChairCount(chairs_left);

	// add 25% of the price of each chair to the budget
	double money_recieved = ((chair_count - quantity) * fChairPrice) * 0.25;
	this->AddToBudget(money_recieved);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SellOven(COven *pOven)
{
	if(!pOven) return;

	// find the oven
	for(unsigned int c = 1 ; c <= this->m_OvenList.GetCount() ; c++)
	{
		COven *pTestOven = this->m_OvenList.GetItem(c);
		if(pTestOven == pOven)
		{
			// delete item from the list
			this->m_OvenList.DeleteItem(c);
			// add 25% of the price to the budget
			this->AddToBudget(pOven->GetPrice() * 0.25);

			return;	// no need to continue with the function
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SellTable(CTable *pTable)
{
	if(!pTable) return;

	for(unsigned int c = 1 ; c <= this->m_TableList.GetCount() ; c++)
	{
		CTable *pTestTable = this->m_TableList.GetItem(c);

		if(pTestTable == pTable)
		{
			// delete table from the list
			this->m_TableList.DeleteItem(c);
			// add 25% of the price to the budget
			this->AddToBudget(pTable->GetPrice() * 0.25);

			return;	// no need to continue with function
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
// NOTE: this function is only called by the simulation after the player
// decides to end his turn.
void CRestaurant::SubtractPayroll()
{
	for(unsigned int c = 1 ; c <= this->m_EmpList.GetCount() ; c++)
	{
		CEmployee *pEmp = this->m_EmpList.GetItem(c);
		// increment the employees #days of employement
		pEmp->SetDaysEmployeed(1+pEmp->GetDaysEmployed());

		// subtract emplyee salary from budget
		float salary = pEmp->GetSalary();
		this->SubractFromBudget((double)salary);
	}
}
/*----------------------------------------------------------------------------------------------*/