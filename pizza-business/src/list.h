#ifndef _JS_LIST_H
#define _JS_LIST_H
/*----------------------------------------------------------------------------------------------*/
namespace ListH {

template <typename T> class List
{
public:
	List() { count = 0; pHead = 0; pTail = 0; del = true; }
	~List() { DeleteAllNodes(); }

public:
	unsigned int AddItem(T* pItem);
	unsigned int  GetCount() { return count; }
	void DeleteAllItems() { DeleteAllNodes(); }
	void DeleteItem(int index);
	T*   GetItem(int index);
	unsigned int InsertItem(T* pItem, int index);

	void SetDeleteFlag(bool flag) { del = flag; }

private:

	class Node
	{
	public:
		Node(T* pItem) { this->pItem = pItem; pItem = 0; pNext = 0; pPrev = 0; }
		~Node() { delete pItem; }

		T*   GetItem() { return pItem; }
		void SetItem(T* pItem) { this->pItem = pItem; }

		Node* GetNext() { return pNext; }
		void  SetNext(Node* next) { pNext = next; }

		Node* GetPrev() { return pPrev; }
		void  SetPrev(Node* prev) { pPrev = prev; }
	
	private:
		T*    pItem;
		Node* pNext;
		Node* pPrev;
	};

	void  DeleteAllNodes();

	int   count;
	Node* pHead;
	Node* pTail;

	bool del;
};
/*----------------------------------------------------------------------------------------------*/
template <typename T> unsigned int List<T>::AddItem(T* pItem)
{
	// create new node object
	Node* pNewNode = new Node(pItem);

	// add new node to end of list
	if(pHead == 0)
		pHead = pTail = pNewNode;
	else
	{
		pNewNode->SetPrev(pTail);
		pTail->SetNext(pNewNode);
		pTail = pNewNode;
	}
	// increment refrence count
	return ++count;
}
/*----------------------------------------------------------------------------------------------*/
template <typename T> void List<T>::DeleteAllNodes()
{
	while(GetCount())
		DeleteItem(1);

	pHead = pTail = 0;
}
/*----------------------------------------------------------------------------------------------*/
template <typename T> void List<T>::DeleteItem(int index)
{
	// find the indexed song; connect the next and prev song pointers
	// then delete the specified node
	if(index < 1 || index > count || pHead == 0)
		return;

	// get pointer to indexed node
	Node* pDelNode = pHead;
	for(int i = 1 ; i < index ; i++)
		pDelNode = pDelNode->GetNext();
	// get prev and next pointers
	Node* pDelNext = pDelNode->GetNext();
	Node* pDelPrev = pDelNode->GetPrev();
	// swap pointers
	if(pDelNext)
		pDelNext->SetPrev(pDelPrev);
	else
		pTail = pDelPrev;
	if(pDelPrev)
		pDelPrev->SetNext(pDelNext);
	else
		pHead = pDelNext;
	// delete object and decrement refrence count
	if(del == true)
		delete pDelNode;
	count--;
}
/*----------------------------------------------------------------------------------------------*/
template <typename T> T* List<T>::GetItem(int index)
{
	if(index < 1 || index > count || pHead == 0)
		return 0;

	// move to specified node
	Node *pNode = pHead;
	T* pItem = pNode->GetItem();
	for(int i = 1 ; i < index ; i++)
	{
		pNode = pNode->GetNext();
	}
	// return pointer to the item
	return pNode->GetItem();
}
/*----------------------------------------------------------------------------------------------*/
template <typename T> unsigned int List<T>::InsertItem(T* pItem, int index)
{
	// just add the item if list is empty
	if(index < 1 || index > count || pHead == 0)
	{
		return AddItem(pItem);
	}
	else
	{
		// create new node object
		Node* pNewNode = new Node(pItem);

		// move to specified node
		Node *pNode = pHead;
		int i = 0;
		for(i = 1 ; i < index ; i++)
			pNode = pNode->GetNext();

		Node* pIns = pNode;
		Node* pPrev = pIns->GetPrev();
		Node* pNext = pIns->GetNext();

		// set the previous pointer
		if(pPrev)
			pPrev->SetNext(pNewNode);
		else
			pHead = pNewNode;

		pNewNode->SetPrev(pPrev);


		// set the next pointer
		pIns->SetPrev(pNewNode);
		pNewNode->SetNext(pIns);

		if(!pNext)
			pTail = pNewNode; // code may never reach here
		
		// increment refrence count
		count++;

		return i;
	}
}

}	// end of namespace ListH
/*----------------------------------------------------------------------------------------------*/
#endif
