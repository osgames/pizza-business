#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include "coniface.h"
#include "conutils.h"
#include "tableview.h"
#include "datapool.h"
#include "sim_main.h"
#include "main.h"
using namespace std;
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CConInterface implementation																 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CConInterface::CConInterface()
{
	this->BuildMenus();
}
/*----------------------------------------------------------------------------------------------*/
CConInterface::~CConInterface()
{

}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Advertisement_Buy(CAdCategory *pCat, CRestaurant *pRestaurant)
{
	CAdvertisement *pBuyAd = 0;
	short index = 0;

	do
	{
		// create the table; add fields
		Table table;
		table.CreateField("", 4, ios::left);
		table.CreateField("Description", 65, ios::left);
		table.CreateField("Price", 10, ios::right);

		// add a row for each advertisement and set data
		for(unsigned int c = 1 ; c <= pCat->AdList.GetCount() ; c++)
		{
			char id[4], desc[1024], price[10];
			CAdvertisement *pAd = pCat->AdList.GetItem(c);

			sprintf(id, "%d.", c);
			pAd->GetDescription(desc);
			sprintf(price, "$%.2f", pAd->GetPrice());
			
			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, desc);
			table.m_Recordset.SetText(iRow, 3, price);
		}


		this->DisplayHeader();
		this->DisplayAds();
		Console_NewLine();

		// display the table
		CTableView Viewer;
		cout << "Available advertisements:" << endl;
		Viewer.DisplayTable(table);

		cout << "Buy which advertisement?" << endl;
		// get user selection
		char reply[3];
		strcpy(reply, "");
		PromptUser("Select (0 to cancel): ", reply, 3);

		// break from function if user cancels
		if(strcmp(reply, "0") == 0)
			return;

		index = atoi(reply);

		pBuyAd = pCat->AdList.GetItem(index);
		if(!pBuyAd)
			continue;

		if(index > pCat->AdList.GetCount() || strlen(reply) < 1)
		{
			continue;
		}
		else
		{

			do
			{
				char quant[10];
				PromptUser("Buy how much: ", quant, 10);

				if(strlen(quant) < 1)
					continue;

				unsigned int quantity = atoi(quant);
				if(quantity < 1)
					break;

				if(pBuyAd)
					pRestaurant->Advertisement_Buy(pBuyAd, quantity);
				break;

			}while(1);
		}

	}while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::BuildMenus()
{
	// build the main menu
	this->m_Menu_Main.SetMenuID(ID_MENU_MAIN);
	this->m_Menu_Main.AddMenuItem(IDM_MAIN_REST, "Restaurant");
    this->m_Menu_Main.AddMenuItem(IDM_MAIN_HOME, "Home");
	this->m_Menu_Main.AddMenuItem(IDM_MAIN_STAFF, "Staff");
	this->m_Menu_Main.AddMenuItem(IDM_MAIN_ENDTURN, "End Turn");
	this->m_Menu_Main.AddMenuItem(IDM_MAIN_LOAD, "Load game");
	this->m_Menu_Main.AddMenuItem(IDM_MAIN_SAVE, "Save game");
	this->m_Menu_Main.AddMenuItem(IDM_MAIN_QUIT, "Quit");

	// build the home menu
	this->m_Menu_Home.SetMenuID(ID_MENU_HOME);
	this->m_Menu_Home.AddMenuItem(IDM_HOME_INGINFO, "Ingredient Info");
	this->m_Menu_Home.AddMenuItem(IDM_HOME_PIZZAS, "Pizza Info");
	this->m_Menu_Home.AddMenuItem(IDM_HOME_POPULARITY, "Popularity");
	this->m_Menu_Home.AddMenuItem(IDM_HOME_BACK, "<- go back to 'main-menu'");

	// build the restaurant menu
	this->m_Menu_Rest.SetMenuID(ID_MENU_REST);
	this->m_Menu_Rest.AddMenuItem(IDM_REST_AD, "Advertisement");
	this->m_Menu_Rest.AddMenuItem(IDM_REST_PHONE, "Use Telephone");
	this->m_Menu_Rest.AddMenuItem(IDM_REST_RENAME, "Name your restaurant");
	this->m_Menu_Rest.AddMenuItem(IDM_REST_BUYSELL, "Buy/Sell things");
	this->m_Menu_Rest.AddMenuItem(IDM_REST_BACK, "<- go back to 'main-menu'");

	// build the staff menu
	this->m_Menu_Staff.SetMenuID(ID_MENU_STAFF);
	this->m_Menu_Staff.AddMenuItem(IDM_STAFF_HIRE, "Hire more people");
	this->m_Menu_Staff.AddMenuItem(IDM_STAFF_FIRE, "Fire an employee");
	this->m_Menu_Staff.AddMenuItem(IDM_STAFF_BACK, "<- go back to 'main-menu'");

	// build the staff_title menu
	this->m_Menu_Staff_Title.SetMenuID(ID_MENU_STAFF_TITLE);
	this->m_Menu_Staff_Title.AddMenuItem(IDM_STAFF_TITLE_CHEF, "Chef");
	this->m_Menu_Staff_Title.AddMenuItem(IDM_STAFF_TITLE_WAITER, "Waiter");
	this->m_Menu_Staff_Title.AddMenuItem(IDM_STAFF_TITLE_MANAGER, "Manager");
	this->m_Menu_Staff_Title.AddMenuItem(IDM_STAFF_TITLE_BACK, "<- go back to 'staff-menu'");

	// build the buy/sell menu
	m_Menu_Rest_BuySell.SetMenuID(ID_MENU_REST_BUYSELL);
	m_Menu_Rest_BuySell.AddMenuItem(IDM_REST_BUYSELL_BUY, "Buy");
	m_Menu_Rest_BuySell.AddMenuItem(IDM_REST_BUYSELL_SELL, "Sell");
	m_Menu_Rest_BuySell.AddMenuItem(IDM_REST_BUYSELL_BACK, "<- go back to 'previous-menu'");

	// build the rest_buysell menu (selection menu)
	this->m_Menu_Rest_BS_Item.SetMenuID(ID_BUYSELL_ITEM);
	this->m_Menu_Rest_BS_Item.AddMenuItem(IDM_BUYSELL_ITEM_ING, "Ingredients");
	this->m_Menu_Rest_BS_Item.AddMenuItem(IDM_BUYSELL_ITEM_FURN, "Furniture");
	this->m_Menu_Rest_BS_Item.AddMenuItem(IDM_BUYSELL_ITEM_FLOOR, "Floor/Carpet");
	this->m_Menu_Rest_BS_Item.AddMenuItem(IDM_BUYSELL_ITEM_RECIPE, "Recipes");
	this->m_Menu_Rest_BS_Item.AddMenuItem(IDM_BUYSELL_ITEM_BACK, "<- go back to 'previous-menu'");

	// build the furniture menu
	m_Menu_Rest_BS_Furniture.SetMenuID(ID_MENU_FURNITURE);
	m_Menu_Rest_BS_Furniture.AddMenuItem(IDM_FURNITURE_CHAIR, "Chair");
	m_Menu_Rest_BS_Furniture.AddMenuItem(IDM_FURNITURE_TABLE, "Table");
	m_Menu_Rest_BS_Furniture.AddMenuItem(IDM_FURNITURE_OVEN, "Oven");
	m_Menu_Rest_BS_Furniture.AddMenuItem(IDM_FURNITURE_BACK, "<- go back to 'previous-menu'");
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Clear()
{
	Console_Clear();
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayAds()
{
	// create the table
	Table table;
	table.CreateField("Days", 5, ios::left);
	table.CreateField("Desc", 60, ios::left);

	List<AdItem> *pList = &this->GetCurrentRestaurant()->m_AdsList;
	for(unsigned int c = 1 ; c <= pList->GetCount() ; c++)
	{
		AdItem *pAd = pList->GetItem(c);

		char desc[1024], days[5];
		pAd->pAd->GetDescription(desc);
		sprintf(days, "%d", pAd->quantity);
		
		short iRow = table.m_Recordset.AddRecord();
		table.m_Recordset.SetText(iRow, 1, days);
		table.m_Recordset.SetText(iRow, 2, desc);
	}

	CTableView viewer;

	cout << "Advertisements owned:" << endl;
	viewer.DisplayTable(table);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayBudget()
{
	cout << "Money available: " << setprecision(2) << setiosflags(ios::fixed|ios::showpoint)
		 << "$" << (this->GetCurrentRestaurant())->GetBudget() << endl;
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayEmployeeList(List<CEmployee> *pList)
{
	bool show_comp = false;
	// build the table
	Table table;
	table.CreateField("", 4, ios::left);
	table.CreateField("Name", 30, ios::left);
	table.CreateField("Salary", 10, ios::left);
	table.CreateField("Age", 6, ios::left);
	if(show_comp == true)
		table.CreateField("Competency", 12, ios::left);
	// add a row for each employee and set data
	for(int i = 1 ; i <= pList->GetCount() ; i++)
	{
		char id[4], name[256], salary[10], age[6];
		CEmployee *pEmp = pList->GetItem(i);

		sprintf(id, "%d.", i);
		sprintf(salary, "$%.2f", pEmp->GetSalary());
		pEmp->GetName(name);

		sprintf(age, "%d", pEmp->GetAge());
		
		short iRow = table.m_Recordset.AddRecord();
		table.m_Recordset.SetText(iRow, 1, id);
		table.m_Recordset.SetText(iRow, 2, name);
		table.m_Recordset.SetText(iRow, 3, salary);
		table.m_Recordset.SetText(iRow, 4, age);

		if(show_comp == true)
		{
			char comp[12];
			sprintf(comp, "%d", pEmp->GetCompetency());
			table.m_Recordset.SetText(iRow, 5, comp);
		}
	}

	// display the table
	CTableView Viewer;
	Viewer.DisplayTable(table);	
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayHeader()
{
	this->Clear();
	this->DisplayRestName();
	this->DisplayBudget();
	this->DisplayTurnCount();
	
	Console_NewLine();
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayInventory(CRestaurant *pRestaurant)
{
	if(pRestaurant == 0) return;

	cout << "Inventory:" << endl;
	Console_HorzLine(60);

	List<IngCat> *pCatList = &pRestaurant->m_IngList;
	for(unsigned int c = 1 ; c <= pCatList->GetCount() ; c++)
	{
		char cat_name[256];
		IngCat *pCat = pCatList->GetItem(c);
		pCat->pCategory->GetName(cat_name);
		cout << cat_name << endl;

		for(unsigned int i = 1 ; i <= pCat->ingredients.GetCount() ; i++)
		{
			char ing_name[256], text[264];
			Ingredient *pIngredient = pCat->ingredients.GetItem(i);
			pIngredient->pIngredient->GetName(ing_name);

			sprintf(text, "%d %s", pIngredient->IngList.GetCount(), ing_name);
			cout << "\t" << text << endl;
		}
	}
	Console_HorzLine(60);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayMenu(int menu_id)
{
	switch(menu_id)
	{
		case ID_MENU_MAIN:
			this->OnMenu_Main();
			break;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayPizzaInfo(CRestaurant *pRestaurant)
{
	// create a table of pizzas to display on screen
	Table table;
	table.CreateField("", 4, ios::left);
	table.CreateField("Pizza Name", 30, ios::left);
	table.CreateField("Cost", 8, ios::left);
	table.CreateField("Price", 8, ios::left);
	table.CreateField("Supported", 10, ios::left);

	for(unsigned int c = 1 ; c <= pRestaurant->m_PizzaList.GetCount() ; c++)
	{
		CPizzaObject *pPizza = pRestaurant->m_PizzaList.GetItem(c);
		
		// get the data in character format
		char name[256], cost[8], price[8], id[4], supported[4];
		pPizza->GetPizza()->GetName(name);
		sprintf(cost, "$%.2f", pPizza->GetPizza()->GetProductionCost());
		sprintf(price, "$%.2f", pPizza->GetPrice());
		sprintf(supported, "%s", pRestaurant->IsPizzaSupported(pPizza)? "YES":"NO");
		sprintf(id, "%d", c);

		// add row
		short iRow = table.m_Recordset.AddRecord();
		table.m_Recordset.SetText(iRow, 1, id);
		table.m_Recordset.SetText(iRow, 2, name);
		table.m_Recordset.SetText(iRow, 3, cost);
		table.m_Recordset.SetText(iRow, 4, price);
		table.m_Recordset.SetText(iRow, 5, supported);
	}

	// display the table
	CTableView Viewer;
	cout << "Possible pizzas to sell:" << endl;
	Viewer.DisplayTable(table);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayStatistics(CRestaurant *pRestaurant)
{
	/*

		x customers entered your restaurant today...
		x customers left because there was no room to sit...
		x customers left because of waiting too long.
		x pizzas were sold today...
		x dollars were made today.
	*/

	this->DisplayHeader();

	DayStats *pStats = pRestaurant->m_StatsList.GetItem(1);
	cout << pStats->customers_enter << " customers entered your restaurant today." << endl;
	cout << pStats->customers_nospace << " customers left because there was no room to sit." << endl;
	cout << pStats->customers_longwait << " customers left because of waiting too long." << endl;
	cout << pStats->pizzas_sold << " pizzas were sold today." << endl;
	cout << "$" << pStats->money_made << " was made today." << endl;

	Console_NewLine();
	Console_NewLine();

	char input[256];
	PromptUser("Press enter to continue ...", input, 256);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayStaff(Emp_Title title_filter)
{
	// create a table
	Table table;
	table.CreateField("Name", 30, ios::left);
	table.CreateField("Title", 20, ios::left);

	// add each employee to the table
	List<CEmployee> EmpList;
	this->GetCurrentRestaurant()->GetEmployees(&EmpList, title_filter);
	for(int c = 1 ; c <= EmpList.GetCount() ; c++)
	{
		CEmployee *pEmp = EmpList.GetItem(c);

		char name[256], title[20];
		pEmp->GetName(name);

		Emp_Title position = pEmp->GetTitle();
		switch(position)
		{
			case TITLE_CHEF:
				strcpy(title, "Chef");
				break;
			case TITLE_WAITER:
				strcpy(title, "Waiter");
				break;
			case TITLE_MANAGER:
				strcpy(title, "Manager");
				break;
		}
		
		// add record; set cells' text
		short iRow = table.m_Recordset.AddRecord();
		table.m_Recordset.SetText(iRow, 1, name);
		table.m_Recordset.SetText(iRow, 2, title);
	}

	// display table
	CTableView viewer;
	viewer.DisplayTable(table);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayTurnCount()
{
	// display the turn if less than 1 year
	unsigned int turn = m_pGameEngine->GetTurn();
	if(turn < 364)
		cout << "Day " << turn << endl;
	else
	{
		// compensate for years
		unsigned int years = turn / 364;
		unsigned int days = turn % 364;

		cout << years << " years and " << days << " Days" << endl;
	}
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::DisplayRestName()
{
	char name[256];
	this->GetCurrentRestaurant()->GetName(name);
	cout << name << endl;
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Furniture_Chair_Buy(CRestaurant *pRestaurant)
{
	int quantity = 0;

	do
	{
		this->DisplayHeader();

		float price = m_pGameEngine->m_DataPool.m_Chair_price;

		cout << "There is not a selection for chairs." << endl;
		cout << "The restaurant has " << pRestaurant->GetChairCount() << "/"
			 << pRestaurant->GetChairSupportCount() << " chairs." << endl << endl;

		char input[10], prompt[256];
		sprintf(prompt, "Buy how many chairs (at $%.2f): ", price);
		PromptUser(prompt, input, 10);
		
		quantity = atoi(input);

		if(quantity < 1 || strlen(input) < 1)
			continue;

		pRestaurant->BuyChair(quantity, price);
	}while(quantity > 0);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Furniture_Chair_Sell(CRestaurant *pRestaurant)
{
	int quantity = 0;

	do
	{
		this->DisplayHeader();

		float price = m_pGameEngine->m_DataPool.m_Chair_price;

		cout << "There is not a selection for chairs." << endl;
		cout << "The restaurant has " << pRestaurant->GetChairCount() << "/"
			 << pRestaurant->GetChairSupportCount() << " chairs." << endl << endl;

		char input[10], prompt[256];
		sprintf(prompt, "Sell how many chairs (at $%.2f): ", price * 0.25);
		PromptUser(prompt, input, 10);
		
		quantity = atoi(input);

		if(quantity < 1 || strlen(input) < 1)
			continue;

		pRestaurant->SellChair(quantity, price);
	}while(quantity > 0);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Furniture_Oven_Buy(CRestaurant *pRestaurant)
{
	CTableView viewer;
	Table table;
	unsigned int c = 0;

	do
	{
		this->DisplayHeader();

		// build a table with ovens already owned
		table.ClearTable();
		table.CreateField("", 3, ios::left);
		table.CreateField("Name", 40, ios::left);
		table.CreateField("Pizzas", 8, ios::left);
		table.CreateField("Time", 8, ios::left);

		List<COven> *pList = &pRestaurant->m_OvenList;
		for(c = 1 ; c <= pList->GetCount() ; c++)
		{
			char id[4], name[256], pizzas[4], time[4];
			COven *pOven = pList->GetItem(c);
			
			pOven->GetName(name);
			sprintf(id, "%d", c);
			sprintf(pizzas, "%d", pOven->GetPizzaSupport());
			sprintf(time, "%d", pOven->GetCookTime());

			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, name);
			table.m_Recordset.SetText(iRow, 3, pizzas);
			table.m_Recordset.SetText(iRow, 4, time);
		}

		// show the tables of ovens owned
		cout << "Ovens owned:" << endl;
		viewer.DisplayTable(table);
		Console_NewLine();

		// build the available ovens to buy
		table.ClearTable();
		table.CreateField("", 3, ios::left);
		table.CreateField("Name", 40, ios::left);
		table.CreateField("Pizzas", 8, ios::left);
		table.CreateField("Time", 8, ios::left);
		table.CreateField("Price", 8, ios::left);

		pList = &m_pGameEngine->m_DataPool.m_OvenList;
		for(c = 1 ; c <= pList->GetCount() ; c++)
		{
			char id[4], name[256], pizzas[4], time[4], price[8];
			COven *pOven = pList->GetItem(c);
			
			pOven->GetName(name);
			sprintf(id, "%d", c);
			sprintf(pizzas, "%d", pOven->GetPizzaSupport());
			sprintf(time, "%d", pOven->GetCookTime());
			sprintf(price, "$%.2f", pOven->GetPrice());

			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, name);
			table.m_Recordset.SetText(iRow, 3, pizzas);
			table.m_Recordset.SetText(iRow, 4, time);
			table.m_Recordset.SetText(iRow, 5, price);
		}

		// show the table to buy ovens
		cout << "Ovens available:" << endl;
		viewer.DisplayTable(table);

		// get user selection
		char reply[3];
		strcpy(reply, "");
		PromptUser("Select an oven to buy (0 to cancel): ", reply, 4);

		// break from function if user cancels
		if(strcmp(reply, "0") == 0)
			return;

		int index = atoi(reply);

		COven *pOven = m_pGameEngine->m_DataPool.m_OvenList.GetItem(index);
		if(pOven)
			pRestaurant->BuyOven(pOven);

	}while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Furniture_Oven_Sell(CRestaurant *pRestaurant)
{
	CTableView viewer;
	Table table;
	unsigned int c = 0;

	do
	{
		this->DisplayHeader();

		// build a table with ovens already owned
		table.ClearTable();
		table.CreateField("", 3, ios::left);
		table.CreateField("Name", 40, ios::left);
		table.CreateField("Pizzas", 8, ios::left);
		table.CreateField("Time", 8, ios::left);

		List<COven> *pList = &pRestaurant->m_OvenList;
		for(c = 1 ; c <= pList->GetCount() ; c++)
		{
			char id[4], name[256], pizzas[4], time[4];
			COven *pOven = pList->GetItem(c);
			
			pOven->GetName(name);
			sprintf(id, "%d", c);
			sprintf(pizzas, "%d", pOven->GetPizzaSupport());
			sprintf(time, "%d", pOven->GetCookTime());

			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, name);
			table.m_Recordset.SetText(iRow, 3, pizzas);
			table.m_Recordset.SetText(iRow, 4, time);
		}

		// show the tables of ovens owned
		cout << "Ovens owned:" << endl;
		viewer.DisplayTable(table);

		// get user selection
		char reply[3];
		PromptUser("Select an oven to sell (0 to cancel): ", reply, 4);

		// break from function if user cancels
		if(strcmp(reply, "0") == 0)
			return;

		int index = atoi(reply);

		COven *pOven = pRestaurant->m_OvenList.GetItem(index);
		if(pOven)
			pRestaurant->SellOven(pOven);

	}while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Furniture_Table_Buy(CRestaurant *pRestaurant)
{
	CTableView viewer;
	Table table;
	unsigned int c = 0;

	do
	{
		this->DisplayHeader();

		// build a table already owned
		table.ClearTable();
		table.CreateField("", 3, ios::left);
		table.CreateField("Name", 40, ios::left);
		table.CreateField("Chairs", 8, ios::left);

		List<CTable> *pList = &pRestaurant->m_TableList;;
		for(c = 1 ; c <= pList->GetCount() ; c++)
		{
			char id[4], name[256], chairs[4];
			CTable *pTable = pList->GetItem(c);
			
			pTable->GetName(name);
			sprintf(id, "%d", c);
			sprintf(chairs, "%d", pTable->GetChairsSupported());

			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, name);
			table.m_Recordset.SetText(iRow, 3, chairs);
		}

		// show the tables owned table
		cout << "Tables owned:" << endl;
		viewer.DisplayTable(table);
		Console_NewLine();

		// build the available tables to buy
		table.ClearTable();
		table.CreateField("", 3, ios::left);
		table.CreateField("Name", 40, ios::left);
		table.CreateField("Chairs", 8, ios::left);
		table.CreateField("Price", 8, ios::left);

		pList = &m_pGameEngine->m_DataPool.m_TableList;
		for(c = 1 ; c <= pList->GetCount() ; c++)
		{
			char id[4], name[256], chairs[4], price[8];
			CTable *pTable = pList->GetItem(c);
			
			pTable->GetName(name);
			sprintf(id, "%d", c);
			sprintf(chairs, "%d", pTable->GetChairsSupported());
			sprintf(price, "$%.2f", pTable->GetPrice());

			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, name);
			table.m_Recordset.SetText(iRow, 3, chairs);
			table.m_Recordset.SetText(iRow, 4, price);
		}

		// show the tables to buy table
		cout << "Tables available:" << endl;
		viewer.DisplayTable(table);

		// get user selection
		char reply[3];
		strcpy(reply, "");
		PromptUser("Select a table to buy (0 to cancel): ", reply, 4);

		// break from function if user cancels
		if(strcmp(reply, "0") == 0)
			return;

		int index = atoi(reply);

		CTable *pTable = m_pGameEngine->m_DataPool.m_TableList.GetItem(index);
		if(pTable)
			pRestaurant->BuyTable(pTable);
	}
	while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Furniture_Table_Sell(CRestaurant *pRestaurant)
{
	CTableView viewer;
	Table table;
	unsigned int c = 0;

	do
	{
		this->DisplayHeader();

		// build a table already owned
		table.ClearTable();
		table.CreateField("", 3, ios::left);
		table.CreateField("Name", 40, ios::left);
		table.CreateField("Chairs", 8, ios::left);

		List<CTable> *pList = &pRestaurant->m_TableList;;
		for(c = 1 ; c <= pList->GetCount() ; c++)
		{
			char id[4], name[256], chairs[4];
			CTable *pTable = pList->GetItem(c);
			
			pTable->GetName(name);
			sprintf(id, "%d", c);
			sprintf(chairs, "%d", pTable->GetChairsSupported());

			short iRow = table.m_Recordset.AddRecord();
			table.m_Recordset.SetText(iRow, 1, id);
			table.m_Recordset.SetText(iRow, 2, name);
			table.m_Recordset.SetText(iRow, 3, chairs);
		}

		// show the tables owned table
		cout << "Tables owned:" << endl;
		viewer.DisplayTable(table);

		// get user selection
		char reply[3];
		PromptUser("Select a table to sell (0 to cancel): ", reply, 4);

		// break from function if user cancels
		if(strcmp(reply, "0") == 0)
			return;

		int index = atoi(reply);

		CTable *pTable = pRestaurant->m_TableList.GetItem(index);
		if(pTable)
			pRestaurant->SellTable(pTable);
	}
	while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Ingredient_Buy(CIngCategory *pIngCat, CRestaurant *pRestaurant)
{
	// be sure we were passed valid pointers
	if(pIngCat == 0 || pRestaurant == 0) return;

	char cat_name[256];	
	pIngCat->GetName(cat_name);

	// build a table of the ingredients in the category
	Table table;
	table.CreateField("", 3, ios::left);
	table.CreateField("Ingredient", 40, ios::left);
	table.CreateField("Price", 8, ios::left);
	for(unsigned int c = 1 ; c <= pIngCat->IngList.GetCount() ; c++)
	{
		CIngredient *pItem = pIngCat->IngList.GetItem(c);

		char id[4], name[256], price[8];
		pItem->GetName(name);
		sprintf(price, "%.2f", pItem->GetPrice());
		sprintf(id, "%d", c);

		short iRow = table.m_Recordset.AddRecord();
		table.m_Recordset.SetText(iRow, 1, id);
		table.m_Recordset.SetText(iRow, 2, name);
		table.m_Recordset.SetText(iRow, 3, price);
	}

	this->DisplayHeader();
	
	cout << "Buy an ingredient from " << cat_name << endl;
	// display the table
	CTableView viewer;
	viewer.DisplayTable(table);

	// prompt until we get valid input (index)
	unsigned int index = 0;
	do
	{
		char input[256];
		PromptUser("Buy which ingredient (0 to cancel): ", input, 256);
		index = atoi(input);
	}while(index < 1 && index > pIngCat->IngList.GetCount());

	// get the ingredient
	CIngredient *pIng = pIngCat->IngList.GetItem(index);
	if(!pIng)
		return;

	char input[256];
	PromptUser("Buy how much: ", input, 256);
	unsigned int quantity = atoi(input);

	pRestaurant->Ingredient_Buy(pIng, quantity);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::Ingredient_Sell(CIngCategory *pIngCat, CRestaurant *pRestaurant)
{
	// be sure we were passed valid pointers
	if(pIngCat == 0 || pRestaurant == 0) return;

	char cat_name[256];	
	pIngCat->GetName(cat_name);

	// build a table of the ingredients in the category
	Table table;
	table.CreateField("", 3, ios::left);
	table.CreateField("Ingredient", 40, ios::left);
	table.CreateField("Price", 8, ios::left);
	table.CreateField("In Stock", 10, ios::left);
	for(unsigned int c = 1 ; c <= pIngCat->IngList.GetCount() ; c++)
	{
		CIngredient *pItem = pIngCat->IngList.GetItem(c);

		char id[4], name[256], price[8];
		pItem->GetName(name);
		sprintf(price, "%.2f", pItem->GetPrice() * 0.25);
		sprintf(id, "%d", c);

		short iRow = table.m_Recordset.AddRecord();
		table.m_Recordset.SetText(iRow, 1, id);
		table.m_Recordset.SetText(iRow, 2, name);
		table.m_Recordset.SetText(iRow, 3, price);
	}

	this->DisplayHeader();
	
	cout << "Sell an ingredient from " << cat_name << endl;
	// display the table
	CTableView viewer;
	viewer.DisplayTable(table);

	// prompt until we get valid input (index)
	unsigned int index = 0;
	do
	{
		char input[256];
		PromptUser("Sell which ingredient (0 to cancel): ", input, 256);
		index = atoi(input);
	}while(index < 1 && index > pIngCat->IngList.GetCount());

	// get the ingredient
	CIngredient *pIng = pIngCat->IngList.GetItem(index);
	if(!pIng)
		return;

	char input[256];
	PromptUser("Sell how much: ", input, 256);
	unsigned int quantity = atoi(input);

	pRestaurant->Ingredient_Sell(pIng, quantity);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Furniture(bool buy, CRestaurant *pRestaurant)
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		pItem = this->m_Menu_Rest_BS_Furniture.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		CRestaurant *pRestaurant = this->GetCurrentRestaurant();
		switch(pItem->commandID)
		{
			case IDM_FURNITURE_TABLE:
				if(buy == true)
					this->Furniture_Table_Buy(pRestaurant);
				else
					this->Furniture_Table_Sell(pRestaurant);
				break;

			case IDM_FURNITURE_CHAIR:
				if(buy == true)
					this->Furniture_Chair_Buy(pRestaurant);
				else
					this->Furniture_Chair_Sell(pRestaurant);
				break;

			case IDM_FURNITURE_OVEN:
				if(buy == true)
					this->Furniture_Oven_Buy(pRestaurant);
				else
					this->Furniture_Oven_Sell(pRestaurant);
				break;
		}
	}while((pItem == 0) || pItem->commandID != IDM_FURNITURE_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Home()
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		pItem = this->m_Menu_Home.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_HOME_PIZZAS:
				this->OnMenu_Home_PizzaInfo(this->GetCurrentRestaurant());
				break;
			case IDM_HOME_POPULARITY:
				this->OnMenu_Home_Popularity(this->GetCurrentRestaurant());
				break;
		}
	}while((pItem == 0) || pItem->commandID != IDM_HOME_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Home_AutoPricing(CRestaurant *pRestaurant)
{
	if(!pRestaurant) return;

	do
	{
		this->DisplayHeader();
		this->DisplayPizzaInfo(pRestaurant);
		
		Console_NewLine();
		cout << "The percentage increase is currently set at "
			 << pRestaurant->GetAutoPricePercentage() << "%." << endl;

		char reply[256];
		PromptUser("Set the percentage increase: (0 to cancel): ", reply, 255);
		
		if(strcmp(reply, "0") == 0)
			return;

		pRestaurant->SetAutoPricePercentage(atof(reply));

	}while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Home_PizzaInfo(CRestaurant *pRestaurant)
{
	if(!pRestaurant) return;

	CMenu menu;
	MenuItem *pItem = 0;
	do {
		// create the menu
		char text[256];
		sprintf(text, "Change Autopricing (%d%%)", pRestaurant->GetAutoPricePercentage());
		menu.DeleteAllMenuItems();
		menu.AddMenuItem(1, "Change Prices");
		if(pRestaurant->Toppings_IsSelectable() == false)
			menu.AddMenuItem(2, "Enable customer customization");
		else
			menu.AddMenuItem(2, "Disable customer customization");
		menu.AddMenuItem(3, text);
		menu.AddMenuItem(4, "<- go back to 'previous-menu'");

		// display info
		this->DisplayHeader();
		this->DisplayPizzaInfo(pRestaurant);
		Console_NewLine();

		// display menu
		cout << "Do what: " << endl;
		pItem = menu.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case 1:
				this->OnMenu_Home_SetPizzaPrice(pRestaurant);
				break;
			case 2:
				pRestaurant->Toppings_SetSelectable(!pRestaurant->Toppings_IsSelectable());
				break;
			case 3:
				this->OnMenu_Home_AutoPricing(pRestaurant);
				break;
		}
	}while((pItem == 0) || pItem->commandID != 4);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Home_Popularity(CRestaurant *pRestaurant)
{
	if(!pRestaurant) return;

	this->DisplayHeader();

	cout << "Current popularity: " << pRestaurant->GetCurrentPopularity() << endl;

	char reply[256];
	PromptUser("Press enter to continue", reply, 255);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Home_SetPizzaPrice(CRestaurant *pRestaurant)
{
	// show a list of the pizzas
	if(!pRestaurant) return;

	do
	{
		this->DisplayHeader();
		this->DisplayPizzaInfo(pRestaurant);	

		char reply[256];
		PromptUser("Change price for which pizza (0 to cancel): ", reply, 255);
		
		if(strcmp(reply, "0") == 0)
			return;

		CPizzaObject *pPizzaChange = pRestaurant->m_PizzaList.GetItem(atoi(reply));

		if(!pPizzaChange)
			continue;

		// prompt user for a new price
		char price[256];
		PromptUser("Change price to (0 to cancel): ", price, 255);

		if(strcmp(price, "0") == 0)
			continue;

		pPizzaChange->SetPrice(atof(price));
	}while(1);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Main()
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		pItem = this->m_Menu_Main.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_MAIN_REST:
				this->OnMenu_Rest();
				break;

			case IDM_MAIN_HOME:
				this->OnMenu_Home();
				break;

			case IDM_MAIN_STAFF:
				this->OnMenu_Staff();
				break;

			case IDM_MAIN_ENDTURN:
				{
					// simluate the business day
					m_pGameEngine->SimulateTurn();
					// display statistics
					this->DisplayStatistics(this->GetCurrentRestaurant());
				}
				break;

			case IDM_MAIN_LOAD:
				this->OnMenu_LoadGame();
				break;
			
			case IDM_MAIN_SAVE:
				this->OnMenu_SaveGame();
				break;
		}
	}while((pItem == 0) || pItem->commandID != IDM_MAIN_QUIT);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Rest()
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();
		
		pItem = this->m_Menu_Rest.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_REST_AD:
				this->OnMenu_Advertisement();
				break;

			case IDM_REST_RENAME:
				this->RenameRestaurant(this->GetCurrentRestaurant());
				break;

			case IDM_REST_BUYSELL:
				this->OnMenu_Rest_BuySell();
				break;
		}
	}while((pItem == 0) || pItem->commandID != IDM_REST_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Advertisement()
{
	CMenu AdMenu;
	int id_back = 1;
	// add all categories to menu
	unsigned int c = 0;  
	for(c = 1 ; c <= m_pGameEngine->m_DataPool.m_AdList.GetCount() ; c++)
	{
		CAdCategory *pCat = m_pGameEngine->m_DataPool.m_AdList.GetItem(c);

		char name[256];
		pCat->GetName(name);
		AdMenu.AddMenuItem(c, name);
	}
	id_back = c;
	AdMenu.AddMenuItem(id_back, "<- go back to previous menu");

	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		pItem = AdMenu.DisplayMenu("Choice: ");

		if(pItem && pItem->commandID != id_back)
			this->Advertisement_Buy(m_pGameEngine->m_DataPool.m_AdList.GetItem(pItem->commandID), this->GetCurrentRestaurant());

	}while((pItem == 0) || pItem->commandID != id_back);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Rest_BuySell()
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		pItem = this->m_Menu_Rest_BuySell.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_REST_BUYSELL_BUY:
				this->OnMenu_Rest_Items(true);
				break;
			case IDM_REST_BUYSELL_SELL:
				this->OnMenu_Rest_Items(false);
				break;
		}
	}while((pItem == 0) || pItem->commandID != IDM_REST_BUYSELL_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Ingredient(bool buy, CRestaurant *pRestaurant)
{
	unsigned int back_id = 1;
	// create a menu for the ingredient categories
	CMenu CatMenu;
	for(unsigned int c = 1 ; c <= m_pGameEngine->m_DataPool.m_IngList.GetCount() ; c++, back_id++)
	{
		CIngCategory *pCat = m_pGameEngine->m_DataPool.m_IngList.GetItem(c);

		char name[256];
		pCat->GetName(name);
		CatMenu.AddMenuItem(c, name);
	}
	CatMenu.AddMenuItem(back_id, "<- go back to 'previous-menu'");

	// show the menu; prompt user selection
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();
		this->DisplayInventory(this->GetCurrentRestaurant());
		Console_NewLine();

		// print some text above menu
		cout << (buy ? "Buy " : "Sell ") << "what:" << endl;

		pItem = CatMenu.DisplayMenu("Choice: ");
		if(pItem == 0) continue;
		
		if(pItem->commandID != back_id)
		{
			if(buy == true)
			{
				this->Ingredient_Buy(m_pGameEngine->m_DataPool.m_IngList.GetItem(pItem->commandID), 
					this->GetCurrentRestaurant());
			}
			else
			{
				this->Ingredient_Sell(m_pGameEngine->m_DataPool.m_IngList.GetItem(pItem->commandID), 
					this->GetCurrentRestaurant());
			}
		}
		
	}while((pItem == 0) || pItem->commandID != back_id);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_LoadGame()
{
	Console_Clear();

	char filename[1024];
	PromptUser("Load game from file (just press enter to cancel)\nFilename: ", filename, 1023);

	if(strcmp(filename, "") == 0) return;

	bool bLoaded = m_pGameEngine->LoadGame(filename);

	char prompt[256];
	sprintf(prompt, "%s game. Press enter to continue...", (bLoaded? "Successful loading of ":"Error loading "));
	PromptUser(prompt, filename, 1023);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Rest_Items(bool buy)
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		cout << (buy? "Buy" : "Sell") << " what item: " << endl << endl;

		pItem = this->m_Menu_Rest_BS_Item.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_BUYSELL_ITEM_ING:
				// NOTE: second parameter will have to change for multi-restaurants
				this->OnMenu_Ingredient(buy, this->GetCurrentRestaurant());
				break;
			case IDM_BUYSELL_ITEM_FURN:
				this->OnMenu_Furniture(buy, this->GetCurrentRestaurant());
				break;
		}
	}while((pItem == 0) || pItem->commandID != IDM_BUYSELL_ITEM_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_SaveGame()
{
	Console_Clear();

	char filename[1024];
	PromptUser("Save game to file (just press enter to cancel)\nFilename: ", filename, 1023);

	if(strcmp(filename, "") == 0) return;

	//CGameState gamestate;
	//gamestate.SaveToIni(filename);

	PromptUser("Game was saved\nPress enter to continue...", filename, 1023);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Staff()
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		cout << "Employees: " << endl;
		this->DisplayStaff();
		Console_NewLine();
		
		pItem = this->m_Menu_Staff.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_STAFF_HIRE:
				this->OnMenu_Staff_Title(true);
				break;

			case IDM_STAFF_FIRE:
				this->OnMenu_Staff_Title(false);
				break;
		}

	}while((pItem == 0) || pItem->commandID != IDM_STAFF_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Staff_Action(Emp_Title title, bool hire)
{
	// get the list of employees from recruiter
	List<CEmployee> *pList = &m_pGameEngine->m_DataPool.m_CanList;

	List<CEmployee> list;
	list.SetDeleteFlag(false);
	// build a list of the unemployeed/employeed
	for(int c = 1 ; c <= pList->GetCount() ; c++)
	{
		CEmployee *pEmp = pList->GetItem(c);
		// be sure not employeed
		if(hire == false)
		{
			if(pEmp->m_EmployedAt == 0)
				continue;

			if(pEmp->GetTitle() == title)
				list.AddItem(pEmp);
		}
		else
		{
			if(pEmp->m_EmployedAt != 0)
				continue;
			
			// calculate the salary for the employee
			int competency = pEmp->GetCompetency();
			short age = pEmp->GetAge();
			
			float salary = ((((float)competency)/2.0) * 0.8) + (((float)age) * 0.2);
			salary += salary * (((float)title) * 0.4);

			pEmp->SetSalary(salary);

			list.AddItem(pEmp);
		}
	}

	int index = 0;
	do
	{
		char str_title[20];
		switch(title)
		{
			case TITLE_CHEF:
				strcpy(str_title, "chef");
				break;
			case TITLE_WAITER:
				strcpy(str_title, "waiter");
				break;
			case TITLE_MANAGER:
				strcpy(str_title, "manager");
				break;
		}

		this->DisplayHeader();

		cout << (hire ? "Hire":"Fire") << " a ";
		cout << str_title;
		cout << ":" << endl << endl;
		this->DisplayEmployeeList(&list);

		// get user selection
		char reply[4];
		strcpy(reply, "");
		PromptUser("\nSelect (0 to cancel): ", reply, 4);

		// be sure the valid input
		if(strlen(reply) < 1)
		{
			// set index to fail the condition and repeat loop
			index = list.GetCount()+1;
			continue;
		}

		// break from function if user cancels
		if(strcmp(reply, "0") == 0)
			return;

		index = atoi(reply);
	}while(index > list.GetCount());

	CEmployee *pSelection = list.GetItem(index);
	if(hire == true)
		this->GetCurrentRestaurant()->HireEmployee(pSelection, title);
	else
		this->GetCurrentRestaurant()->FireEmployee(pSelection);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::OnMenu_Staff_Title(bool hire)
{
	MenuItem *pItem = 0;
	do {
		this->DisplayHeader();

		cout << "Employees:" << endl;
		this->DisplayStaff();
		Console_NewLine();

		cout << (hire ? "Hire":"Fire");
		cout << " what position?" << endl;
		pItem = this->m_Menu_Staff_Title.DisplayMenu("Choice: ");
		if(pItem == 0) continue;

		switch(pItem->commandID)
		{
			case IDM_STAFF_TITLE_CHEF:
				this->OnMenu_Staff_Action(TITLE_CHEF, hire);
				break;
			case IDM_STAFF_TITLE_WAITER:
				this->OnMenu_Staff_Action(TITLE_WAITER, hire);
				break;
			case IDM_STAFF_TITLE_MANAGER:
				this->OnMenu_Staff_Action(TITLE_MANAGER, hire);
				break;
		}

	}while((pItem == 0) || pItem->commandID != IDM_STAFF_TITLE_BACK);
}
/*----------------------------------------------------------------------------------------------*/
void CConInterface::RenameRestaurant(CRestaurant *pRestaurant)
{
	this->Clear();

	char oldName[256], newName[256];
	pRestaurant->GetName(oldName);

	// prompt user for new name
	cout << "Change name from: " << oldName << endl;
	PromptUser("To: ", newName, 256);

	if(strlen(newName) > 0)
		pRestaurant->SetName(newName);
}
/*----------------------------------------------------------------------------------------------*/
