#include "staff.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CEmployee implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CEmployee::CEmployee()
{
	m_EmployedAt = 0;
	this->SetTitle(TITLE_UNEMPLOYED);
	this->SetSalary(0.0f);
	this->SetCompetency(0);
	this->SetDaysEmployeed(0);
}
/*----------------------------------------------------------------------------------------------*/
bool CEmployee::ClearStatistics()
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			return this->CChef::ClearStatistics();
		case TITLE_WAITER:
			return this->CWaiter::ClearStatistics();
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CEmployee::GetStatistic(int id)
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			return this->CChef::GetStats(id);
		case TITLE_WAITER:
			return this->CWaiter::GetStats(id);
	}
	return 0;	
}
/*----------------------------------------------------------------------------------------------*/
bool CEmployee::SetStatistic(int id, unsigned int statistic)
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			return this->CChef::SetStats(id, statistic);
		case TITLE_WAITER:
			return this->CWaiter::SetStats(id, statistic);
	}
	return false;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CChef implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
bool CChef::ClearStatistics() 
{
	stats.pizzas_cooked = 0;
	return true;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CChef::GetStats(int id)
{
	switch(id)
	{
		case PIZZAS_COOKED:
			return stats.pizzas_cooked;
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CChef::SetStats(int id, unsigned int statistic)
{
	switch(id)
	{
		case PIZZAS_COOKED:
			stats.pizzas_cooked = statistic;
			return true;
	}
	return false;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CWaiter implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
bool CWaiter::ClearStatistics()
{
	stats.pizzas_ordered = 0;
	stats.pizzas_served = 0;
	return true;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CWaiter::GetStats(int id)
{
	switch(id)
	{
		case PIZZAS_ORDERED:
			return stats.pizzas_ordered;
		case PIZZAS_SERVED:
			return stats.pizzas_served;
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CWaiter::SetStats(int id, unsigned int statistic)
{
	switch(id)
	{
		case PIZZAS_ORDERED:
			stats.pizzas_ordered = statistic;
			return true;
		case PIZZAS_SERVED:
			stats.pizzas_served = statistic;
			return true;
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/