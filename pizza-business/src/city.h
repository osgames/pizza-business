#ifndef _JS_CITY_H
#define _JS_CITY_H
#include "region.h"
class CCity
{
public:
	bool LoadRegions(char* filename);
	CRegion* GetRegionOfRestaurant(int restaurantID);
public:
	struct {
		char name[256];
		char description[1024];
		unsigned short num_regions;
	} data;
	List <CRegion> m_regions;
private:
	void AddRegion(CRegion *newRegion);
};
#endif