#ifndef _JS_CONUTILS_H
#define _JS_CONUTILS_H
/*----------------------------------------------------------------------------------------------*/
void Console_Clear();
void Console_HorzLine(int length);
void Console_NewLine();
void PromptUser(char* prompt, char* buffer, int size);
/*----------------------------------------------------------------------------------------------*/
#endif
