#ifndef _JS_DATA_H
#define _JS_DATA_H
/*----------------------------------------------------------------------------------------------*/
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
struct CharDesc {
	char name[1024];
	short factor;
};

struct CharName {
	char name[256];
};

struct MinMax {
	short min;
	short max;
};

struct FileHeader {
	float app_version;
	float format_version;
};

struct TableData {
	char name[256];
	float price;
	short chair_support;
	short table_id;
};

struct OvenData {
	char name[256];
	float price;
	short pizza_support;
	short cooking_time;
	short oven_id;
};

struct ChairPrice {
	float price;
};

struct AdData {
	char name[256];
	float price;
	short factor;
	short ad_id;
};

struct AdCategory {
	List<AdData> Ads;
	char name[256];
};

struct IngData {
	char name[256];
	float price;
	short quality;
	short weight;
	short vender_id;											// reseverd; for furture possibilites
	short ingredient_id;
};

struct IngCategory {
	List<IngData> Ingredients;
	char name[256];
};

struct RecipeData {
	List<short> Ingredients;
	char name[256];
};
/*----------------------------------------------------------------------------------------------*/
typedef struct tagPBD_DATA {
	// these structures can be written straight to the file
	FileHeader Header;
	MinMax Age;
	MinMax Competency;
	ChairPrice Chair;

	// special structures (written to file in a specific way)
	List<AdCategory> AdCatList;
	List<IngCategory> IngCatList;
	List<RecipeData> RecipeList;

	// each list must be written to a file following a
	// subheader indicating the number of items in the list
	List<CharName> FirstnameList;
	List<CharName> LastnameList;
	List<CharDesc> DescList;
	List<TableData> TableList;
	List<OvenData> OvenList;
}PBD_DATA;
/*----------------------------------------------------------------------------------------------*/
bool Entities_LoadFile(char* filename, PBD_DATA &data);
#ifdef _WINDOWS
void Entities_SaveFile(char* filename, PBD_DATA &data);
#endif
/*----------------------------------------------------------------------------------------------*/
// utility functions - ingredients
IngData* GetIngredientByID(List<IngCategory> *pCatList, short id);
short GetNextAvailableIngID(List<IngCategory> *pCatList);
bool IngIdAvailable(List<IngCategory> *pCatList, short id);

// utility functions - advertisements
short GetNextAvailableAdID(List<AdCategory> *pCatList);
bool AdIdAvailable(List<AdCategory> *pCatList, short id);

// utility functions - tables
short GetNextAvailableTableID(List<TableData> *pTableList);
bool TableIdAvailable(List<TableData> *pTableList, short id);

// utility functions - ovens
short GetNextAvailableOvenID(List<OvenData> *pOvenList);
bool OvenIdAvailable(List<OvenData> *pOvenList, short id);
/*----------------------------------------------------------------------------------------------*/
#endif