#ifndef _JS_DATAPOOL_H
#define _JS_DATAPOOL_H
/*----------------------------------------------------------------------------------------------*/
#include "data.h"
#include "datarest.h"
#include "pizzas.h"

#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
// function prototypes
short GenerateRandomNumber(short min, short max);
/*----------------------------------------------------------------------------------------------*/
class CMarketer
{
public: // construction/destruction
	CMarketer();
	~CMarketer() {}

public: // functions
	void CreateAdvertisements(List<AdCategory> *pAdCat);

public:
	List<CAdCategory> m_AdList;

};
/*----------------------------------------------------------------------------------------------*/
class CRecruiter
{
public: // construction/destruction
	CRecruiter();
	~CRecruiter();

public: // functions
	void CreateEmployees(PBD_DATA & pbd, short max_people);

public: // data
	List<CEmployee> m_CanList;

protected: // functions
	short CalculateCompetencyBonus(short initial_competency, short age);
};
/*----------------------------------------------------------------------------------------------*/
class CDataPool : public CMarketer, public CRecruiter
{
public:
	CDataPool() {}
	~CDataPool() {}
	bool Initialize();

public: // functions
	short GetMaxCompetency() { return m_Max_competency; }
	short GetMinCompetency() { return m_Min_competency; }

public: // helper functions
	CIngredient* GetIngredientByID(short id);
	COven* GetOvenByID(short id);
	CTable* GetTableByID(short id);

public: // data
	List<CIngCategory> m_IngList;
	List<CTable> m_TableList;
	List<COven> m_OvenList;

	List<CPizza> m_PizzaList;
	
	float m_Chair_price;

private: // functions
	void CreateIngredients(List<IngCategory> *pIngList);
	void CreateOvens(List<OvenData> *pOvenList);
	void CreatePizzas(List<RecipeData> *pPizzaList);
	void CreateTables(List<TableData> *pTableList);

private: // data
	short m_Max_competency;
	short m_Min_competency;
};
/*----------------------------------------------------------------------------------------------*/
#endif