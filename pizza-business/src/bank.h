#ifndef _JS_BANK_H
#define _JS_BANK_H
/*----------------------------------------------------------------------------------------------*/
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class CRestaurant;
class CBank;
/*----------------------------------------------------------------------------------------------*/
class CBankLoan
{
public:
	CBankLoan(double amount, float rate);

public:
	double GetBalance() { return m_Balance; }
	void SetBalance(double balance) { m_Balance = balance; }

	unsigned int GetLifeTime() { return m_LifeTime; }
	void SetLifeTime(unsigned int days) { m_LifeTime = days; }

	double GetInitialAmount() { return m_InitialLoan; }

	float GetRate() { return m_Rate; }
	void SetRate(float rate) { m_Rate = rate; }

public:
	bool IsInEffect();
	short DecEffectDays();

private:
	double m_Loan;
	double m_Balance;
	float m_Rate;
	double m_InitialLoan;

	short m_TillEffect;
	unsigned int m_LifeTime;
};
/*----------------------------------------------------------------------------------------------*/
class CBankAccount
{
public:
	CBankAccount(CRestaurant *pRestOwn, CBank *pBank);

public:
	CBankLoan* CreateLoan(double amount, float rate);
	CRestaurant* GetRestaurant() { return m_pRestaurant; }
	double GetAccountBalance();
	unsigned int GetActiveLoanCount();
	bool IsExpired() { return m_bExpired; }
	double PayOffLoans(double amount);

	void ThumbLoans();

public:
	List<CBankLoan> m_LoanList;

private:
	CRestaurant *m_pRestaurant;
	CBank *m_pBank;
	bool m_bExpired;											// indicates the account be removes
};
/*----------------------------------------------------------------------------------------------*/
class CBank
{
friend class CBankAccount;

public:
	CBank();

public:
	CBankAccount* CreateAccount(CRestaurant *pRestaurant);
	void CollectLoanMoney();									// called from the simulation engine
	void DeleteAccount(CRestaurant *pRestaurant);
	void DeleteAllAccounts();

	CBankAccount* GetAccount(unsigned int index);
	unsigned int GetAccountCount() { return m_AccountList.GetCount(); }

	double GetDebtLimit() { return m_max_debt; }

private:
	List<CBankAccount> m_AccountList;
	float m_LoanRate;

	double m_max_debt;
};
/*----------------------------------------------------------------------------------------------*/
#endif