/* 
	Authors & contributers:

	Johnny Salazar

*/
#ifndef _JS_CCONMENU_H
#define _JS_CCONMENU_H
/*----------------------------------------------------------------------------------------------*/
#include <iostream>
#include <cstring>
/*----------------------------------------------------------------------------------------------*/
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
struct MenuItem 
{
	unsigned int commandID;
	char text[256];
};
/*----------------------------------------------------------------------------------------------*/
// class to build a menu list with a prompt
// first build a list of MenuItems; then call DisplayMenu
class CMenu
{
public:
	CMenu();
	CMenu(unsigned int menu_id);
	~CMenu();

public:
	void AddMenuItem(unsigned int id, char* text);
	void DeleteAllMenuItems() { m_MenuList.DeleteAllItems(); }
	void DeleteItemByID(unsigned int id);
	MenuItem* DisplayMenu(char* prompt);						// returns user's reply to prompt
	MenuItem* GetMenuItemByID(unsigned int id);

	unsigned int GetMenuID() { return m_MenuID; }
	void SetMenuID(unsigned int menu_id) { m_MenuID = menu_id; }

private:
	List <MenuItem> m_MenuList;
	unsigned int m_MenuID;
};
/*----------------------------------------------------------------------------------------------*/
#endif