#ifndef _JS_CONIFACE_H
#define _JS_CONIFACE_H
/*----------------------------------------------------------------------------------------------*/
#include "interface.h"
#include "cconmenu.h"
#include "datarest.h"
/*----------------------------------------------------------------------------------------------*/

// menu identifiers
#define ID_MENU_MAIN			101
#define IDM_MAIN_REST			1
#define IDM_MAIN_HOME			2
#define IDM_MAIN_STAFF			3
#define IDM_MAIN_ENDTURN		4
#define IDM_MAIN_LOAD			5
#define IDM_MAIN_SAVE			6
#define IDM_MAIN_QUIT			9

#define ID_MENU_REST			102
#define IDM_REST_AD				1
#define IDM_REST_PHONE			2
#define IDM_REST_RENAME			3
#define IDM_REST_BUYSELL		4
#define IDM_REST_BACK			5

#define ID_MENU_STAFF			103
#define IDM_STAFF_HIRE			1
#define IDM_STAFF_FIRE			2
#define IDM_STAFF_PROMOTE		3
#define IDM_STAFF_DEMOTE		4
#define IDM_STAFF_BACK			5

#define ID_MENU_STAFF_TITLE		104
#define IDM_STAFF_TITLE_CHEF	1
#define IDM_STAFF_TITLE_WAITER	2
#define IDM_STAFF_TITLE_MANAGER	3
#define IDM_STAFF_TITLE_BACK	4

#define ID_MENU_REST_BUYSELL	105
#define IDM_REST_BUYSELL_BUY	1
#define IDM_REST_BUYSELL_SELL	2
#define IDM_REST_BUYSELL_BACK	3

#define ID_BUYSELL_ITEM			106
#define IDM_BUYSELL_ITEM_ING	1
#define IDM_BUYSELL_ITEM_FURN	2
#define IDM_BUYSELL_ITEM_FLOOR	3
#define IDM_BUYSELL_ITEM_RECIPE	4
#define IDM_BUYSELL_ITEM_BACK	5

#define ID_MENU_HOME			107
#define IDM_HOME_INGINFO		1
#define IDM_HOME_PIZZAS			2
#define IDM_HOME_POPULARITY		3
#define IDM_HOME_BACK			4

#define ID_MENU_FURNITURE		108
#define IDM_FURNITURE_CHAIR		1
#define IDM_FURNITURE_TABLE		2
#define IDM_FURNITURE_OVEN		3
#define IDM_FURNITURE_BACK		4
/*----------------------------------------------------------------------------------------------*/
// NOTE: I know the menu variable names are long. No complaints please.
/*----------------------------------------------------------------------------------------------*/
class CConInterface : public CInterface
{
public: // construction/destruction
	CConInterface();
	~CConInterface();

	int Run() { this->DisplayMenu(ID_MENU_MAIN); return 0; }

public: // functions
	void Clear();
	void DisplayAds();
	void DisplayBudget();
	void DisplayHeader();
	void DisplayInventory(CRestaurant *pRestaurant);
	void DisplayPizzaInfo(CRestaurant *pRestaurant);
	void DisplayRestName();
	void DisplayMenu(int menu_id);
	void DisplayStaff(Emp_Title title_filter = TITLE_IRRELEVANT);
	void DisplayStatistics(CRestaurant *pRestaurant);
	void DisplayTurnCount();

private: // functions
	void BuildMenus();
	void DisplayEmployeeList(List<CEmployee> *list);

	void OnMenu_Home();
	void OnMenu_Home_AutoPricing(CRestaurant *pRestaurant);
	void OnMenu_Home_PizzaInfo(CRestaurant *pRestaurant);
	void OnMenu_Home_SetPizzaPrice(CRestaurant *pRestaurant);
	void OnMenu_Home_Popularity(CRestaurant *pRestaurant);

	void OnMenu_Main();
	void OnMenu_LoadGame();
	void OnMenu_SaveGame();
	
	// restaurant menu handlers
	void OnMenu_Rest();
	void OnMenu_Advertisement();
	void OnMenu_Rest_BuySell();
	void OnMenu_Rest_Items(bool buy);
	void OnMenu_Ingredient(bool buy, CRestaurant *pRestaurant);	// called from OnMenu_Rest_Items()
	void OnMenu_Furniture(bool buy, CRestaurant *pRestaurant);
	
	void OnMenu_Staff();
	void OnMenu_Staff_Action(Emp_Title title, bool hire = true);
	void OnMenu_Staff_Title(bool hire = true);

private:
	void Advertisement_Buy(CAdCategory *pCat, CRestaurant *pRestaurant);

	void Furniture_Chair_Buy(CRestaurant *pRestaurant);
	void Furniture_Chair_Sell(CRestaurant *pRestaurant);
	void Furniture_Oven_Buy(CRestaurant *pRestaurant);
	void Furniture_Oven_Sell(CRestaurant *pRestaurant);
	void Furniture_Table_Buy(CRestaurant *pRestaurant);
	void Furniture_Table_Sell(CRestaurant *pRestaurant);

	void Ingredient_Buy(CIngCategory *pIngCat, CRestaurant *pRestaurant);
	void Ingredient_Sell(CIngCategory *pIngCat, CRestaurant *pRestaurant);

	void RenameRestaurant(CRestaurant *pRestaurant);

private: // data
	// menus
	CMenu m_Menu_Main;
	
	CMenu m_Menu_Rest;
	CMenu m_Menu_Rest_BuySell;
	CMenu m_Menu_Rest_BS_Item;
	CMenu m_Menu_Rest_BS_Furniture;

	CMenu m_Menu_Home;

	CMenu m_Menu_Staff;
	CMenu m_Menu_Staff_Title;
};
/*----------------------------------------------------------------------------------------------*/
#endif