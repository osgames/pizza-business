#ifndef _JS_INI_WORK_H
#define _JS_INI_WORK_H
/*----------------------------------------------------------------------------------------------*/
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
/*----------------------------------------------------------------------------------------------*/
class CIniFile
{
public:
	CIniFile(char* filename) { strcpy(m_Filename, filename); }

public: // functions (services)
	double GetDouble(char* section_name, char* key_name, double default_double);
	unsigned int GetInteger(char* section_name, char* key_name, unsigned int default_int);
	unsigned int GetString(char* section_name, char* key_name, char* default_string,
		char* returned_string, unsigned int rs_size);
	bool IsFileValid();

private:
	bool MoveToSection(ifstream &infile, char* section_name);
	bool GetKeyString(ifstream &infile, char* key_name, char* returned_string, unsigned int rs_size);

private:
	char m_Filename[1024];
};
/*----------------------------------------------------------------------------------------------*/
#endif