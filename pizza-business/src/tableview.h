#ifndef _JS_TABLEVIEW_H
#define _JS_TABLEVIEW_H
/*----------------------------------------------------------------------------------------------*/
#include "table.h"
/*----------------------------------------------------------------------------------------------*/
class CTableView
{
public: // construction/destruction
	CTableView() {}
	~CTableView() {}

public: // functions
	void DisplayTable(Table &table);

private: // functions
	void DrawHorzLine(Table &table);
	void DrawRecord(Record *pRecord);
	void DrawTableHeader(Table &table);
};
/*----------------------------------------------------------------------------------------------*/
#endif
