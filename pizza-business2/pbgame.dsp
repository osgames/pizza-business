# Microsoft Developer Studio Project File - Name="pbgame" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=pbgame - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "pbgame.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "pbgame.mak" CFG="pbgame - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "pbgame - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "pbgame - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "pbgame - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "c:\wx2\include" /I "c:\wx2\lib\mswd" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "__WINDOWS__" /D "__WXMSW__" /D DEBUG=1 /D "__WXDEBUG__" /D "__WIN95__" /D "__WIN32__" /D WINVER=0x0400 /D "STRICT" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /i "c:\wx2\include" /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib rpcrt4.lib wsock32.lib winmm.lib wxmswd.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"libcd.lib" /nodefaultlib:"libcid.lib" /nodefaultlib:"msvcrt.lib" /out:"bin/pbgame.exe" /pdbtype:sept /libpath:"c:\wx2\lib"
# SUBTRACT LINK32 /nodefaultlib

!ENDIF 

# Begin Target

# Name "pbgame - Win32 Release"
# Name "pbgame - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "Gui_Source"

# PROP Default_Filter "cpp;c"
# Begin Source File

SOURCE=.\src\graph.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\hoverbutton.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\mainframe.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\mainmenu.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\pie_chart.cpp
# End Source File
# Begin Source File

SOURCE=.\src\statisticsdialog.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\wnd_bank.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\wnd_buy.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\wnd_contracts.cpp
# End Source File
# Begin Source File

SOURCE=.\src\wnd_pizza.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\wnd_staff.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\wnd_stats.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx.rc
# End Source File
# End Group
# Begin Group "Core_Source"

# PROP Default_Filter "c;cpp"
# Begin Source File

SOURCE=.\src\bank.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\city.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\contracts.cpp
# End Source File
# Begin Source File

SOURCE=.\src\data.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\datapool.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\datarest.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\furniture.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\pbengine.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\pizzas.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\region.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\sim_main.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\staff.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# Begin Group "Util_Source"

# PROP Default_Filter "c;cpp"
# Begin Source File

SOURCE=.\src\inifiles.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\src\logger.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=.\src\capp.cpp

!IF  "$(CFG)" == "pbgame - Win32 Release"

!ELSEIF  "$(CFG)" == "pbgame - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Group "Gui_Header"

# PROP Default_Filter "h;hpp"
# Begin Source File

SOURCE=.\src\graph.h
# End Source File
# Begin Source File

SOURCE=.\src\hoverbutton.h
# End Source File
# Begin Source File

SOURCE=.\src\mainframe.h
# End Source File
# Begin Source File

SOURCE=.\src\mainmenu.h
# End Source File
# Begin Source File

SOURCE=.\src\pie_chart.h
# End Source File
# Begin Source File

SOURCE=.\src\statisticsdialog.h
# End Source File
# Begin Source File

SOURCE=.\src\wnd_bank.h
# End Source File
# Begin Source File

SOURCE=.\src\wnd_buy.h
# End Source File
# Begin Source File

SOURCE=.\src\wnd_contracts.h
# End Source File
# Begin Source File

SOURCE=.\src\wnd_pizza.h
# End Source File
# Begin Source File

SOURCE=.\src\wnd_staff.h
# End Source File
# Begin Source File

SOURCE=.\src\wnd_stats.h
# End Source File
# End Group
# Begin Group "Core_Header"

# PROP Default_Filter "h;hpp"
# Begin Source File

SOURCE=.\src\action.h
# End Source File
# Begin Source File

SOURCE=.\src\bank.h
# End Source File
# Begin Source File

SOURCE=.\src\buysell.h
# End Source File
# Begin Source File

SOURCE=.\src\city.h
# End Source File
# Begin Source File

SOURCE=.\src\contracts.h
# End Source File
# Begin Source File

SOURCE=.\src\data.h
# End Source File
# Begin Source File

SOURCE=.\src\datapool.h
# End Source File
# Begin Source File

SOURCE=.\src\datarest.h
# End Source File
# Begin Source File

SOURCE=.\src\furniture.h
# End Source File
# Begin Source File

SOURCE=.\src\logger.h
# End Source File
# Begin Source File

SOURCE=.\src\pbengine.h
# End Source File
# Begin Source File

SOURCE=.\src\pizzas.h
# End Source File
# Begin Source File

SOURCE=.\src\region.h
# End Source File
# Begin Source File

SOURCE=.\src\sim_main.h
# End Source File
# Begin Source File

SOURCE=.\src\staff.h
# End Source File
# End Group
# Begin Group "Util_Header"

# PROP Default_Filter "h;hpp"
# Begin Source File

SOURCE=.\src\inifiles.h
# End Source File
# Begin Source File

SOURCE=.\src\list.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\capp.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\blank.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\bullseye.cur
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\cdrom.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\computer.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\drive.ico
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\error.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\file1.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\floppy.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\folder1.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\folder2.ico
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\hand.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\info.ico
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\magnif1.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\noentry.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\pbrush.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\pencil.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\pntleft.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\pntright.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\query.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\question.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\removble.ico
# End Source File
# Begin Source File

SOURCE=..\..\..\wx2\include\wx\msw\wx\msw\rightarr.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\roller.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\size.cur
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\tip.ico
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\warning.ico
# End Source File
# Begin Source File

SOURCE=C:\wx2\include\wx\msw\wx\msw\watch1.cur
# End Source File
# End Group
# Begin Source File

SOURCE=.\notes.txt
# End Source File
# End Target
# End Project
