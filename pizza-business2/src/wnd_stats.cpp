#include "wnd_stats.h"
#include <wx/notebook.h>
#include <wx/dcclient.h>
/*----------------------------------------------------------------------------+
|																			  |						 
|	CDayStatsDlg implementation												  |						 
|																		      |						 
+----------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CDayStatsDlg, wxDialog)
	EVT_PAINT(CDayStatsDlg::OnPaint)
	EVT_BUTTON(-1, CDayStatsDlg::OnOKClicked)
END_EVENT_TABLE()
/*---------------------------------------------------------------------------*/
CDayStatsDlg::CDayStatsDlg(wxWindow *parent, CRestaurant* pRestaurant)
	:wxDialog(parent, -1, "Statistics Dialog", wxPoint(-1, -1), wxSize(500, 500), wxDEFAULT_DIALOG_STYLE, "dialogBox"),
	m_pRestaurant(pRestaurant)
{
	this->Centre();

	// set the window's caption
	wxString caption;
	caption.sprintf("Statistics for Restaurant [%d]", this->m_pRestaurant->GetId());
	this->SetTitle(caption);

	// create the notebook window
	const short nb_space_sides = 10;
	const short nb_space_bottom = 50;

	wxSize wndSize = this->wxWindow::GetClientSize();
	m_pNoteBook = new wxNotebook(this, -1, wxPoint(nb_space_sides, nb_space_sides),
		wxSize(wndSize.GetWidth() - (nb_space_sides * 2),
		wndSize.GetHeight() - nb_space_sides - nb_space_bottom));

	// create the first page and add it to the notebook
	CStatsPage1 *page1 = new CStatsPage1(m_pNoteBook);
	m_pNoteBook->AddPage(page1, "Daily Statistics");
	page1->Initialize(this->m_pRestaurant);

	// create the second page and add it to the notebook
	CStatsPage2 *page2 = new CStatsPage2(m_pNoteBook);
	m_pNoteBook->AddPage(page2, "Expenditures");
	page2->Initialize(this->m_pRestaurant);

    CStatsPage3 *page3 = new CStatsPage3(m_pNoteBook);
    m_pNoteBook->AddPage(page3, "Money Made");
    page3->Initialize(this->m_pRestaurant);

	// create the ok button to close this window
	const short button_width = 75;
	const short button_height = 25;
	this->m_pOkButton = new wxButton(this, -1, "&Ok", 
		wxPoint(wndSize.GetWidth() - button_width - nb_space_sides,
		wndSize.GetHeight() - button_height - nb_space_sides),
		wxSize(button_width, button_height));
}
/*---------------------------------------------------------------------------*/
CDayStatsDlg::~CDayStatsDlg()
{
	this->m_pNoteBook->Destroy();
	this->m_pOkButton->Destroy();
}
/*---------------------------------------------------------------------------*/
void CDayStatsDlg::OnPaint(wxPaintEvent &e)
{
	wxPaintDC paint(this);
}
/*---------------------------------------------------------------------------*/
void CDayStatsDlg::OnOKClicked(wxMouseEvent &e)
{
	this->EndModal(wxID_OK);
}
/*----------------------------------------------------------------------------+
|																		      |						 
|	CStatsPage1 implementation											      |						 
|																		      |						 
+----------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CStatsPage1, wxWindow)
	EVT_BUTTON(101, CStatsPage1::OnClicked_PrevStats)
	EVT_BUTTON(102, CStatsPage1::OnClicked_NextStats)
	EVT_CHECKBOX(202, CStatsPage1::OnChecked_FilterChange)
END_EVENT_TABLE()
/*---------------------------------------------------------------------------*/
CStatsPage1::CStatsPage1(wxWindow *parent) : wxWindow(parent, -1, wxDefaultPosition, wxSize(470, 390)),
m_CurStatIndex(1), m_ColCount(0)
{

}
/*---------------------------------------------------------------------------*/
CStatsPage1::~CStatsPage1()
{
	// destroy the static controls
	this->line->Destroy();
	this->income->Destroy();
	this->pizzasSold->Destroy();
	this->custsVisits->Destroy();
	this->custsLeft->Destroy();
	this->popularity->Destroy();
	this->m_pDayTitle->Destroy();
	this->m_pDayText->Destroy();

	m_BmpStatsBk->Destroy();
	this->m_pPrevButton->Destroy();
	this->m_pNextButton->Destroy();

	this->m_pListView->Destroy();
	this->m_pStaffTitle->Destroy();
	this->m_pCheckCook->Destroy();
	this->m_pCheckWaiter->Destroy();
	this->m_pCheckManager->Destroy();
}
/*---------------------------------------------------------------------------*/
void CStatsPage1::Initialize(CRestaurant* pRestaurant)
{
	this->m_pRestaurant = pRestaurant;

	wxSize wndSize = this->GetClientSize();

	// load the bitmap for use behing the statisics text
	wxBitmap bmpStatsBk;
	bmpStatsBk.LoadFile("stats_bk.bmp", wxBITMAP_TYPE_BMP);
	wxMask *bk_mask = new wxMask(bmpStatsBk, wxColour(0,0,255));
	bmpStatsBk.SetMask(bk_mask);
	// create the static bitmap behind the statistics text
	m_BmpStatsBk = new wxStaticBitmap(this, -1, bmpStatsBk, wxPoint(100, 50), wxSize(354, 97));

	// instantiate the static controls
	line = new wxStaticLine(this, -1, wxPoint(10, 200), wxSize(wndSize.GetWidth()-20, 2));
	this->income = new wxStaticText(this, -1, "", wxPoint(140, 60));
	this->pizzasSold = new wxStaticText(this, -1, "", wxPoint(310, 60));
	this->custsVisits =  new wxStaticText(this, -1, "", wxPoint(140, 90));
	this->custsLeft = new wxStaticText(this, -1, "", wxPoint(140, 120));
	this->popularity = new wxStaticText(this, -1, "", wxPoint(310, 90));

	// change the background color of the static text controls to white
	this->income->SetBackgroundColour(wxColour(255,255,255));
	this->pizzasSold->SetBackgroundColour(wxColour(255,255,255));
	this->custsVisits->SetBackgroundColour(wxColour(255,255,255));
	this->custsLeft->SetBackgroundColour(wxColour(255,255,255));
	this->popularity->SetBackgroundColour(wxColour(255,255,255));

	// create the font for the bold title text
	wxFont StatsFont(18, wxDEFAULT, wxNORMAL, wxBOLD, FALSE,
		"Comic Sans MS", wxFONTENCODING_SYSTEM);

	// set the font for the daily stats title
	this->m_pDayTitle = new wxStaticText(this, -1, "", wxPoint(100, 10));
	this->m_pDayTitle->SetFont(StatsFont);
	this->m_pDayTitle->SetLabel("Daily Statistics");

	// create the previous and next buttons with the bmp and a mask
		// prev
	wxBitmap prev_bitmap;
	prev_bitmap.LoadFile("l_arrow.bmp", wxBITMAP_TYPE_BMP);
	wxMask *prev_mask = new wxMask(prev_bitmap, wxColour(255,255,255));
	prev_bitmap.SetMask(prev_mask);
	this->m_pPrevButton = new wxBitmapButton(this, 101, prev_bitmap,
		wxPoint(100, 155), wxSize(50, 36));
		// next
	wxBitmap next_bitmap;
	next_bitmap.LoadFile("r_arrow.bmp", wxBITMAP_TYPE_BMP);
	wxMask *next_mask = new wxMask(next_bitmap, wxColour(255,255,255));
	next_bitmap.SetMask(next_mask);
	this->m_pNextButton = new wxBitmapButton(this, 102, next_bitmap,
		wxPoint(wndSize.GetWidth()-60, 155), wxSize(50, 36));

	// create the static text control to display the date/turn
	this->m_pDayText = new wxStaticText(this, -1, "Sunday", wxPoint(150, 165), 
		wxSize((wndSize.GetWidth()-60)-150, 25), wxALIGN_CENTRE);

	// create the staff text title
	m_pStaffTitle = new wxStaticText(this, -1, "", wxPoint(100, 210));
	m_pStaffTitle->SetFont(StatsFont);
	m_pStaffTitle->SetLabel("The Staff");

	// create the listview control
	this->m_pListView = new wxListCtrl(this, -1, wxPoint(100, 245), 
		wxSize(wndSize.GetWidth()-110, wndSize.GetHeight()-255), wxLC_REPORT);

	// create the radio buttons
	m_pCheckCook = new wxCheckBox(this, 202, "Cooks", wxPoint(20, 265));
	m_pCheckWaiter = new wxCheckBox(this, 202, "Waiters", wxPoint(20, 295));
	m_pCheckManager = new wxCheckBox(this, 202, "Managers", wxPoint(20, 325));

	m_pCheckCook->SetValue(true);
	m_pCheckWaiter->SetValue(true);
	m_pCheckManager->SetValue(true);

	// update controls
	this->RefreshStatistics();
	this->RefreshStaffList();
}
/*---------------------------------------------------------------------------*/
void CStatsPage1::OnChecked_FilterChange(wxMouseEvent &e)
{
	this->RefreshStaffList();
}
/*---------------------------------------------------------------------------*/
void CStatsPage1::OnClicked_NextStats(wxMouseEvent &e)
{
	if(this->m_CurStatIndex > 1)
	{
		this->m_CurStatIndex--;
		this->RefreshStatistics();
	}
}
/*---------------------------------------------------------------------------*/
void CStatsPage1::OnClicked_PrevStats(wxMouseEvent &e)
{
	unsigned int count = this->m_pRestaurant->m_StatsList.GetCount();
	if(this->m_CurStatIndex < count)
	{
		this->m_CurStatIndex++;
		this->RefreshStatistics();
	}
}
/*---------------------------------------------------------------------------*/
void CStatsPage1::RefreshStaffList()
{
	// delete the items
	this->m_pListView->DeleteAllItems();
	// delete the columns
	while(this->m_ColCount > 0)
	{
		this->m_pListView->DeleteColumn(0);
		this->m_ColCount--;
	}

	// setup the column index table
	long index_name = 0, index_title = 1, index_ordered = 4, index_served = 5,
		index_cooked = 6, index_days_worked = 2, index_salary = 3;

	// create the columns in the listview control
	this->m_pListView->InsertColumn(index_name, "Name", wxLIST_FORMAT_LEFT, 200);
	this->m_pListView->InsertColumn(index_title, "Position", wxLIST_FORMAT_LEFT, 100);
	this->m_pListView->InsertColumn(index_days_worked, "Days Worked", wxLIST_FORMAT_LEFT, 100);
	this->m_pListView->InsertColumn(index_salary, "Salary", wxLIST_FORMAT_LEFT, 100);
	this->m_ColCount = 4;

	// add the additional columns if necessary
	if(this->m_pCheckWaiter->GetValue() == true)
	{
		this->m_pListView->InsertColumn(index_ordered, "Pizzas Ordered", wxLIST_FORMAT_LEFT, 100);
		this->m_pListView->InsertColumn(index_served, "Pizzas Served", wxLIST_FORMAT_LEFT, 100);
		this->m_ColCount += 2;
	}
	if(this->m_pCheckCook->GetValue() == true)
	{
		index_cooked = this->m_ColCount;
		this->m_pListView->InsertColumn(index_cooked, "Pizzas Cooked", wxLIST_FORMAT_LEFT, 100);
		this->m_ColCount++;
	}

	// add the appropriate staff to the listview control with respect to the filter
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_EmpList.GetCount() ; c++)
	{
		CEmployee *pWorker = this->m_pRestaurant->m_EmpList.GetItem(c);

		Emp_Title title = pWorker->GetTitle();

		if(this->m_pCheckWaiter->GetValue() == true && title == TITLE_WAITER ||
			this->m_pCheckCook->GetValue() == true && title == TITLE_CHEF ||
			this->m_pCheckManager->GetValue() == true && title == TITLE_MANAGER)
		{


			char name[256];
			pWorker->GetName(name);

			wxListItem item;
			item.m_mask = wxLIST_MASK_TEXT;
			// set the name of employee
			item.m_text = name;
			item.m_itemId = this->m_pListView->InsertItem(item);

			// set the # of days employeed
			item.m_text.sprintf("%d", pWorker->GetDaysEmployed());
			item.m_col = index_days_worked;
			this->m_pListView->SetItem(item);

			// set the salaray
			item.m_text.sprintf("$%.2f", pWorker->GetSalary());
			item.m_col = index_salary;
			this->m_pListView->SetItem(item);

			// set his job position and his statistics
			if(title == TITLE_CHEF)
			{
				item.m_text = "Cook";
				item.m_col = index_title;
				this->m_pListView->SetItem(item);

				item.m_text.sprintf("%d", pWorker->GetStatistic(PIZZAS_COOKED, INDEX_STATS_TOTALS));
				item.m_col = index_cooked;
				this->m_pListView->SetItem(item);
			}
			else if(title == TITLE_WAITER)
			{
				item.m_text = "Waiter";
				item.m_col = index_title;
				this->m_pListView->SetItem(item);

				item.m_text.sprintf("%d", pWorker->GetStatistic(PIZZAS_ORDERED, INDEX_STATS_TOTALS));
				item.m_col = index_ordered;
				this->m_pListView->SetItem(item);

				item.m_text.sprintf("%d", pWorker->GetStatistic(PIZZAS_SERVED, INDEX_STATS_TOTALS));
				item.m_col = index_served;
				this->m_pListView->SetItem(item);
			}
			else if(title == TITLE_MANAGER)
			{
				item.m_text = "Manager";
				item.m_col = index_title;
				this->m_pListView->SetItem(item);
			}
		}
	}
}
/*---------------------------------------------------------------------------*/
void CStatsPage1::RefreshStatistics()
{
	// if there are valid statistics, then fill
	// out the controls appropriately
	DayStats *pStats = this->m_pRestaurant->m_StatsList.GetItem(m_CurStatIndex);
	if(pStats)
	{
		char text[40];

		sprintf(text, "Money Income: $%.2f", pStats->money_made);
		this->income->SetLabel(text);
		
		sprintf(text, "Pizzas Sold: %d", pStats->pizzas_sold);
		this->pizzasSold->SetLabel(text);

		sprintf(text, "Customer Visits: %d", pStats->customers_enter);
		this->custsVisits->SetLabel(text);

		sprintf(text, "Customers that left: %d",
			pStats->customers_longwait + pStats->customers_nospace);
		this->custsLeft->SetLabel(text);

		sprintf(text, "Popularity: %.2f", pStats->popularity);
		this->popularity->SetLabel(text);

		//sprintf(text, "Turn %d", pStats->turn_id);
		//this->m_pDayText->SetTitle(text);
		int day = pStats->turn_id % 7;
		switch(day)
		{
		case 0:
			this->m_pDayText->SetTitle("Sunday");
			break;
		case 1:
			this->m_pDayText->SetTitle("Monday");
			break;
		case 2:
			this->m_pDayText->SetTitle("Tuesday");
			break;
		case 3:
			this->m_pDayText->SetTitle("Wednesday");
			break;
		case 4:
			this->m_pDayText->SetTitle("Thursday");
			break;
		case 5:
			this->m_pDayText->SetTitle("Friday");
			break;
		case 6:
			this->m_pDayText->SetTitle("Saturday");
		}
	}
	else
		this->m_pDayTitle->SetLabel("Daily Statistics (None yet)");
}
/*----------------------------------------------------------------------------+
|																		      |						 
|	CStatsPage2 implementation											      |						 
|																		      |						 
+----------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CStatsPage2, wxWindow)

END_EVENT_TABLE()
/*---------------------------------------------------------------------------*/
CStatsPage2::CStatsPage2(wxWindow *parent) : wxWindow(parent, -1, wxDefaultPosition, wxSize(470, 390))
{
	// create the pie chart
	m_pChartIncome = new CPieChartWnd(this, -1, wxPoint(20,20), wxSize(400, 200));
}
/*---------------------------------------------------------------------------*/
CStatsPage2::~CStatsPage2()
{
	m_pChartIncome->Destroy();
}
/*---------------------------------------------------------------------------*/
void CStatsPage2::Initialize(CRestaurant *pRestaurant)
{
	this->m_pRestaurant = pRestaurant;

    Expenditures temp;

    temp = this->m_pRestaurant->expenses;

    m_pChartIncome->AddSlice("Advertisements", static_cast<long>(temp.ads));
    m_pChartIncome->AddSlice("Chairs", static_cast<long>(temp.chairs));
    m_pChartIncome->AddSlice("Ingredients", static_cast<long>(temp.ingredients));
    m_pChartIncome->AddSlice("Ovens", static_cast<long>(temp.ovens));
    m_pChartIncome->AddSlice("Tables", static_cast<long>(temp.tables));
}
/*---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------+
|																		      |						 
|	CStatsPage3 implementation											      |						 
|																		      |						 
+----------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CStatsPage3, wxWindow)
    EVT_PAINT(CStatsPage3::OnPaint)
END_EVENT_TABLE()
CStatsPage3::CStatsPage3(wxWindow *parent) : wxWindow(parent, -1, wxDefaultPosition, wxSize(470, 390))
{
}
/*---------------------------------------------------------------------------*/
CStatsPage3::~CStatsPage3()
{}
/*---------------------------------------------------------------------------*/
void CStatsPage3::Initialize(CRestaurant *pRestaurant)
{
    this->m_pRestaurant = pRestaurant;
    
    // Setup line graph
    income.SetGraphHeight(380);
    income.SetGraphWidth(460);
    income.SetGraphPosition(wxPoint(0,0));
    income.SetPadding(20, 30, 30, 10);
    income.SetXAxisLabel("Day");
    income.SetYAxisLabel("Money Made in dollars");
    unsigned int count = this->m_pRestaurant->m_StatsList.GetCount();

    std::vector<lgCoord> points;

    int j = 0;
    for (unsigned int i = 1; i <= count; i++)
    {
        j = this->m_pRestaurant->m_StatsList.GetItem(i)->turn_id;
        points.push_back(lgCoord(j, 
                                    this->m_pRestaurant->m_StatsList.GetItem(i)->money_made));
    }
    
    /*points.push_back(lgCoord(1, 3000));
    points.push_back(lgCoord(2, 0));
    points.push_back(lgCoord(3, -1300.56));
    points.push_back(lgCoord(4, -300.57));
    points.push_back(lgCoord(5, 500.4));
    points.push_back(lgCoord(6, 0));
    points.push_back(lgCoord(7, 10));*/
    income.SetGraphPoints(points);
}
/*---------------------------------------------------------------------------*/
void CStatsPage3::OnPaint(wxPaintEvent &e)
{
    wxPaintDC paint(this);

    income.DrawGraph(paint);

    if (this->income.GetNumPoints() == 0)
        paint.DrawText("This graph will not show until you have simulated a turn", 10, 10);
}
/*---------------------------------------------------------------------------*/



