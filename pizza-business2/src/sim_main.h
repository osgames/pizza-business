#ifndef _JS_SIM_MAIN_H
#define _JS_SIM_MAIN_H
#define DEBUG_SIM

// next 3 lines is to make wxWindows
// compatible with the STL headers
#ifdef new
#undef new
#endif

#include <list>
#include <vector>
#include <queue>
#include <map>
#include <fstream>
#include "pbengine.h"
#include "action.h"

#include "list.h"
using namespace ListH;

typedef std::map<CIngredient*, int> CustLikes;

/*----------------------------------------------------------------------------------------------*/
class SimCustomer;
class SimChair;
class SimOvenSlot;
class SimOrder;
class SimWaiter;
class SimCook;
class SimManager;
class SimEmployee;
/*----------------------------------------------------------------------------------------------*/
class SimOrder
{
public:
    SimOrder();
    ~SimOrder();
	int GetOrdererID();
	void SetOrdererID(int id);
	std::vector<SimChair>::iterator GetChairOfOrderer();
	void SetChairOfOrderer(std::vector<SimChair>::iterator tableTemp);
	CPizzaObject* GetPizza();
	void SetPizza(CPizzaObject* newPizza);
private:
    int ordererID;
	std::vector<SimChair>::iterator chair;
	CPizzaObject pizza;
};
/*----------------------------------------------------------------------------------------------*/
class CSimulator : public CGameEngine
{
public:
	CSimulator();
	~CSimulator();

public:
	void SimulateTurn();

private:
    void GenerateCustomerLikes(std::vector<SimCustomer> &custs, CDataPool *pData);
	int DetNumOfCusts(CRestaurant *restaurant);
	void SetArrivalTimes();
	void FillEmployeeVectors(CRestaurant* restaurant);
    bool AssignCustomerToChair(std::vector<SimCustomer>::iterator i);
	void FillOvenVector(CRestaurant* restaurant);
	void GenerateCustomerArrivals(DayStats *pStats);
	SimOrder GenerateOrder(std::vector<SimChair>::iterator chair, CRestaurant *pRestaurant);
	void UpdateWaiterStatuses(CRestaurant *pRestaurant, DayStats *pStats);
	void UpdateCookStatuses(CRestaurant *pRestaurant);
	void UpdateCustomerStatuses(DayStats *pStats);
	void UpdateOvens();
    int DetCustLikeRatingOfPizza(SimCustomer &cust, CPizzaObject *p);
    bool CustomerWillBuyPizza(int pizzaLikeRating, CPizzaObject *p);
    CPizzaObject* DetermineCustsFavoritePizza(SimCustomer &cust, List<CPizzaObject> *availablePizzas, int &favRating);

private:
    std::vector <SimChair> chairPool;		// Contains all the chair entities, in which customers can sit
	std::vector <SimCustomer> customerPool;	// Contains all the customer entities, which are finite state machines
	std::vector <SimWaiter> waiterVec;	// Contains all the waiter entities, which are finite state machines
	std::vector <SimCook> cookVec;		// Contains all the cook entities, which are FSMs
	std::vector <SimManager> managerVec;	// Contains all the manager entities, which are FSMs; there should only be one
									// ... manager, but a vector is used for consistency
	std::vector <SimOvenSlot> ovens;		// Contains all the "oven slots" in which pizzas will be cooked
	std::queue <SimOrder> pizzasToBePrepared;	// Will hold those "orders" which have been taken by servants, but have not...
									// yet been prepared by the cooks
	std::queue <SimOrder> pizzasToBeCooked;	// Will hold those "orders" which have been prepared, but have not yet...
									// ... been cooked.  Pizzas will automatically be placed in an oven when one...
									// ... becomes available
	std::queue <SimOrder> finishedPizzas;	// Will hold those "orders" which have been prepared and cooked...
								// ... and need to be taken back to the customer
	int currentMinute;	// Keeps track of the minute of the simulation
	int customerID;		// Used to assign each customer a unique id (assigned during seating)
						// ... incremented after each assignment
	double moneyFromPizzas;
    std::ofstream debugSim;


};
/*----------------------------------------------------------------------------------------------*/
class SimCustomer
{
public:
	SimCustomer();
	~SimCustomer() {};
	STATUS GetCurrentStatus();
	void SetCurrentStatus(STATUS newAction);
	int GetArrivalTime();
	void SetArrivalTime(int time);
	void SetCustomerID(int id);
	int GetCustomerID();
    void SetStatusEnd(int time);
    int GetStatusEnd();
    CustLikes ingLikes;

private:
	STATUS currentAction;
	int arrivalTime;
	int customerID;
    int currActionEndsAt;


};
/*----------------------------------------------------------------------------------------------*/
class SimChair
{
public:
    SimChair();
    ~SimChair();
    SimCustomer* GetOccupant();
    void SetOccupant(SimCustomer* newOccupant);
    bool IsChairOccupied();
    void SetChairOccupied(bool newOccupied);
private:
    SimCustomer* occupant;    
    bool occupied;
};
/*----------------------------------------------------------------------------------------------*/
class SimOvenSlot
{
public:
	SimOvenSlot();
	SimOvenSlot(int cookTime);
	~SimOvenSlot();
	void SetCookTime(int cookTime);
	int GetCookTime();
	bool IsOccupied();
	void SetOccupancy(bool occupied);
	void SetOrderCooking(SimOrder order);
	SimOrder GetOrderCooking();
	void SetCookingEndTime(int endTime);
	int GetCookingEndTime();

private:
	int myCookTime;
	SimOrder orderCooking;
	bool isOccupied;
	int cookingEndTime;
};
/*----------------------------------------------------------------------------------------------*/
class SimEmployee
{
public:
	SimEmployee();
	~SimEmployee();
	STATUS GetCurrentStatus();
	void SetCurrentStatus(STATUS newStatus);
	int GetStatusEnd();
	void SetStatusEnd(int end);
public:
	CEmployee* actualEmployee;	// points to the employee used  by the management portion of the game
	int id;
private:
	STATUS currentStatus;
	int statusEnd;
};
/*----------------------------------------------------------------------------------------------*/
class SimWaiter: public SimEmployee
{
public:
	SimWaiter();
	~SimWaiter();
	std::vector<SimChair>::iterator GetCurrentCustomerChair();
	void SetCurrentCustomerChair(std::vector<SimChair>::iterator chair);
	SimOrder GetCurrentOrder();
	void SetCurrentOrder(SimOrder newOrder);
private:
	std::vector<SimChair>::iterator currentCustomerChair;
	SimOrder currentOrder;
};
/*----------------------------------------------------------------------------------------------*/
class SimCook: public SimEmployee
{
public:
	SimCook();
	~SimCook();
	SimOrder GetCurrentOrder();
	void SetCurrentOrder(SimOrder newOrder);
private:
	SimOrder currentOrder;
};
/*----------------------------------------------------------------------------------------------*/
class SimManager: public SimEmployee
{
public:
	SimManager();
	~SimManager();
	std::vector<SimChair>::iterator GetCurrentCustomerChair();
	void SetCurrentCustomerChair(std::vector<SimChair>::iterator customer);
	SimOrder GetCurrentOrder();
	void SetCurrentOrder(SimOrder newOrder);
private:
	SimOrder currentOrder;
	std::vector<SimChair>::iterator currentCustomerChair;
};
/*----------------------------------------------------------------------------------------------*/
#endif



