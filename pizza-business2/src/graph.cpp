#include "graph.h"
#include <wx/window.h>
#undef new
#include <sstream>
#include "datarest.h"
/*----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------+
|																		                         |						 
|	Graph implementation											                         |						 
|																		                         |						 
+-----------------------------------------------------------------------------------------------*/
Graph::Graph() : graphPixelWidth(0), graphPixelHeight(0), graphPixelPosition(0,0), paddingTop(50), paddingBottom(50), paddingRight(50), paddingLeft(50)
{
}
/*----------------------------------------------------------------------------------------------*/
void Graph::SetGraphHeight(int height)
{
	this->graphPixelHeight = height;
}
/*----------------------------------------------------------------------------------------------*/
void Graph::SetGraphWidth(int width)
{
	this->graphPixelWidth = width;
}
/*----------------------------------------------------------------------------------------------*/
void Graph::SetGraphPosition(wxPoint pos)
{
	this->graphPixelPosition = pos;
}
/*----------------------------------------------------------------------------------------------*/
void Graph::SetPadding(int top, int bottom, int left, int right)
{
	this->paddingTop = top;
	this->paddingBottom = bottom;
	this->paddingRight = right;
	this->paddingLeft = left;
}
/*----------------------------------------------------------------------------------------------*/
void Graph::SetXAxisLabel(std::string label)
{
	this->xLabel = label;
}
/*----------------------------------------------------------------------------------------------*/
void Graph::SetYAxisLabel(std::string label)
{
	this->yLabel = label;
}
/*-----------------------------------------------------------------------------------------------+
|																		                         |						 
|	LineGraph implementation											                         |						 
|																		                         |						 
+-----------------------------------------------------------------------------------------------*/
LineGraph::LineGraph() : Graph()
{
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::SetGraphPoints(std::vector<lgCoord> points)
{
	this->graphPoints = points;
	
	this->CalcMaxXValue();
	this->CalcMinXValue();
	this->CalcMaxYValue();
	this->CalcMinYValue();
		
	this->RelativizeXValues();
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::RelativizeXValues()
{
	for (unsigned int i = 0; i < this->graphPoints.size(); i++)
	{
		this->graphPoints[i].x = (this->graphPoints[i].actX - this->minXValue + 1);
	}
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::DrawGraph(wxDC &graphDC)
{
//	int buffer = 10;
	lgCoord prevPoint(0,0);
	bool hasPosValues = false;
	bool hasNegValues = false;
	std::stringstream s;
	wxCoord w, h;

	if (this->graphPoints.size() <= 0)
		return;

	wxBrush brush(*wxWHITE, wxSOLID);
	graphDC.SetBrush(brush);
	graphDC.DrawRectangle(this->graphPixelPosition.x, this->graphPixelPosition.y, this->graphPixelWidth, this->graphPixelHeight);

	graphDC.SetPen(*wxBLACK_PEN);

	double scalarX = double(this->graphPixelWidth - (paddingRight + paddingLeft)) / (this->maxXValue-this->minXValue+1);
	double scalarY;

	if (this->ContainsPositiveYValues())
	{
		if (this->ContainsNegativeYValues())
		{
			// Set the x origin in the middle of the drawing area (from top to bottom)
			graphDC.SetDeviceOrigin(this->graphPixelPosition.x + paddingLeft, this->graphPixelPosition.y + paddingTop + (this->graphPixelHeight - paddingTop - paddingBottom) / 2);
			// Draw the axis in a different color than black, so that people realize this is y = 0, because the bottom line where the 'axis' would normall be will also be drawn and the labels will be put down there
			graphDC.SetPen(*wxGREEN_PEN);
			graphDC.DrawLine(0, 0, (this->graphPixelWidth - (paddingLeft + paddingRight)), 0);
			graphDC.SetPen(*wxBLACK_PEN);
			graphDC.DrawLine(0, 0, 0, (-1) * (this->graphPixelHeight - paddingTop - paddingBottom) / 2);
			graphDC.DrawLine(0, 0, 0, (this->graphPixelHeight - paddingTop - paddingBottom) / 2);
			// Draw bottom line where intervals are marked, as the x axis is not there in this case
			graphDC.DrawLine(0, (this->graphPixelHeight - paddingBottom - paddingTop)/2, this->graphPixelWidth - paddingRight - paddingLeft, (this->graphPixelHeight - paddingBottom - paddingTop)/2);
			if (this->maxYValue > labs(this->minYValue))
			{
				scalarY = double((this->graphPixelHeight - paddingTop - paddingBottom) / 2) / this->maxYValue*(-1);				
			}
			else
			{
				scalarY = double((this->graphPixelHeight - paddingTop - paddingBottom) / 2) / this->minYValue;
			}
			hasPosValues = true;
			hasNegValues = true;
		}
		else // Contains only positive y values
		{
			// set the x origin in the bottom of the drawing area
			graphDC.SetDeviceOrigin(this->graphPixelPosition.x + paddingLeft, this->graphPixelPosition.y + graphPixelHeight - paddingBottom);
			graphDC.SetPen(*wxGREEN_PEN);
			graphDC.DrawLine(0, 0, (this->graphPixelWidth - (paddingLeft + paddingRight)), 0);
			graphDC.SetPen(*wxBLACK_PEN);
			graphDC.DrawLine(0, 0, 0, (-1) * (this->graphPixelHeight - paddingTop - paddingBottom));
			scalarY = double(this->graphPixelHeight - paddingTop - paddingBottom) / this->maxYValue*(-1);
			hasPosValues = true;
		}
	}
	else // Contains only negative y values
	{
		// set the x origin in the top of the drawing area
		graphDC.SetDeviceOrigin(this->graphPixelPosition.x + paddingLeft, this->graphPixelPosition.y + paddingTop);
		graphDC.SetPen(*wxGREEN_PEN);
		graphDC.DrawLine(0, 0, (this->graphPixelWidth - (paddingLeft + paddingRight)), 0);
		graphDC.SetPen(*wxBLACK_PEN);
		graphDC.DrawLine(0, 0, 0, (this->graphPixelHeight - paddingTop - paddingBottom));
		// Draw bottom line where intervals are marked, as the x axis is not there in this case
		graphDC.DrawLine(0, this->graphPixelHeight - paddingBottom - paddingTop, this->graphPixelWidth - paddingRight - paddingLeft, this->graphPixelHeight - paddingBottom - paddingTop);
		scalarY = double(this->graphPixelHeight - paddingBottom - paddingTop) / this->minYValue;
		hasNegValues = true;
	}

	graphDC.DrawPoint(graphPoints[0].x*scalarX, graphPoints[0].y*scalarY);
	s << graphPoints[0].y;
	graphDC.GetTextExtent(s.str().c_str(), &w, &h);
	graphDC.DrawText(s.str().c_str(), graphPoints[0].x*scalarX - w, graphPoints[0].y*scalarY-h);
	s.str("");
	prevPoint = graphPoints[0];

	for (unsigned int i = 1; i < graphPoints.size(); i++)
	{
		graphDC.SetPen(*wxRED_PEN);
        graphDC.DrawLine(prevPoint.x*scalarX, prevPoint.y*scalarY, graphPoints[i].x*scalarX, graphPoints[i].y*scalarY);
		graphDC.SetPen(*wxBLACK_PEN);
		// Draw mark for intervals along the x axis
		if (hasPosValues)
		{
			if (hasNegValues)
			{
				graphDC.DrawLine(prevPoint.x*scalarX, (this->graphPixelHeight - paddingBottom - paddingTop)/2 + 4, 
									prevPoint.x*scalarX, (this->graphPixelHeight - paddingBottom - paddingTop)/2);
				s << prevPoint.actX;
				graphDC.GetTextExtent(s.str().c_str(), &w, &h);
				graphDC.DrawText(s.str().c_str(), prevPoint.x*scalarX - (w/2) - 2, (this->graphPixelHeight - paddingBottom - paddingTop)/2 + 5);
				s.str("");
			}
			else
			{
				graphDC.DrawLine(prevPoint.x*scalarX, 0, prevPoint.x*scalarX, 4);
				// Number the increments...
				s << prevPoint.actX;
				graphDC.GetTextExtent(s.str().c_str(), &w, &h);
				graphDC.DrawText(s.str().c_str(), prevPoint.x*scalarX - (w/2) - 2, 5);
				s.str("");
			}
		}
		else
		{
			graphDC.DrawLine(prevPoint.x*scalarX, this->graphPixelHeight - paddingTop - paddingBottom + 4,
								prevPoint.x * scalarX, this->graphPixelHeight - paddingBottom - paddingTop);
			// Number the increments...
			s << prevPoint.actX;
			graphDC.GetTextExtent(s.str().c_str(), &w, &h);
			graphDC.DrawText(s.str().c_str(), prevPoint.x*scalarX - (w/2) - 2, (graphPixelHeight - paddingTop - paddingBottom + 5));
			s.str("");
		}
		s << graphPoints[i].y;
		graphDC.GetTextExtent(s.str().c_str(), &w, &h);
		graphDC.DrawText(s.str().c_str(), graphPoints[i].x*scalarX - w, graphPoints[i].y*scalarY-h);
		s.str("");
		prevPoint = graphPoints[i];
	}
	if (hasPosValues)
	{
		if (hasNegValues)
		{
			graphDC.DrawLine(prevPoint.x*scalarX, (this->graphPixelHeight - paddingBottom - paddingTop)/2 + 4, 
								prevPoint.x*scalarX, (this->graphPixelHeight - paddingBottom - paddingTop)/2);
			// Number the increments...
			s << prevPoint.actX;
			graphDC.GetTextExtent(s.str().c_str(), &w, &h);
			graphDC.DrawText(s.str().c_str(), prevPoint.x*scalarX - (w/2) - 2, (this->graphPixelHeight - paddingBottom - paddingTop)/2 + 5);
			s.str("");
			// Draw x axis label
			graphDC.GetTextExtent(this->xLabel.c_str(), &w, &h);
			graphDC.DrawText(this->xLabel.c_str(), ((this->graphPixelWidth - paddingRight - paddingLeft) / 2) - (w / 2), 
								((this->graphPixelHeight - paddingBottom - paddingTop) / 2) + paddingBottom - h);
			// Draw y axis label
			graphDC.GetTextExtent(this->yLabel.c_str(), &w, &h);
			graphDC.DrawRotatedText(this->yLabel.c_str(), 0 - paddingLeft, 0 + (w/2), 90); 
		}
		else
		{
			graphDC.DrawLine(prevPoint.x*scalarX, 0, prevPoint.x*scalarX, 4);
			// Number the increments...
			s << prevPoint.actX;
			graphDC.GetTextExtent(s.str().c_str(), &w, &h);
			graphDC.DrawText(s.str().c_str(), prevPoint.x*scalarX - (w/2) - 2, 5);
			s.str("");
			// Draw x axis label
			graphDC.GetTextExtent(this->xLabel.c_str(), &w, &h);
			graphDC.DrawText(this->xLabel.c_str(), ((this->graphPixelWidth - paddingRight - paddingLeft) / 2) - (w / 2), 
								((this->graphPixelHeight - paddingBottom - paddingTop)) + paddingBottom - h);
			// Draw y axis label
			graphDC.GetTextExtent(this->yLabel.c_str(), &w, &h);
			graphDC.DrawRotatedText(this->yLabel.c_str(), 0 - paddingLeft, (-1)*(((graphPixelHeight - paddingBottom - paddingTop)/2) - (w/2)), 90); 
		}
	}
	else
	{
		graphDC.DrawLine(prevPoint.x*scalarX, this->graphPixelHeight - paddingTop - paddingBottom + 4,
							prevPoint.x * scalarX, this->graphPixelHeight - paddingBottom - paddingTop);
		// Number the increments...
		s << prevPoint.actX;
		graphDC.GetTextExtent(s.str().c_str(), &w, &h);
		graphDC.DrawText(s.str().c_str(), prevPoint.x*scalarX - (w/2) - 2, (graphPixelHeight - paddingTop - paddingBottom + 5));
		s.str("");
		// Draw x axis label
		graphDC.GetTextExtent(this->xLabel.c_str(), &w, &h);
		graphDC.DrawText(this->xLabel.c_str(), ((this->graphPixelWidth - paddingRight - paddingLeft) / 2) - (w / 2), 
							this->graphPixelHeight - paddingTop - h);
		// Draw y axis label
		graphDC.GetTextExtent(this->yLabel.c_str(), &w, &h);
		graphDC.DrawRotatedText(this->yLabel.c_str(), 0 - paddingLeft, ((graphPixelHeight - paddingBottom - paddingTop)/2) + (w/2), 90); 
	}
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::CalcMaxXValue()
{
	if (!(graphPoints.size() >= 1))
		return;
	double maxX = graphPoints[0].actX;
    for (unsigned int i = 1; i < this->graphPoints.size(); i++)
	{
		if (this->graphPoints[i].actX > maxX)
		{
			maxX = graphPoints[i].actX;
		}
	}
	this->maxXValue = static_cast<int>(maxX);
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::CalcMinXValue()
{
	if (!(graphPoints.size() >= 1))
		return;
	double minX = graphPoints[0].actX;
    for (unsigned int i = 1; i < this->graphPoints.size(); i++)
	{
		if (this->graphPoints[i].actX < minX)
		{
			minX = graphPoints[i].actX;
		}
	}
	this->minXValue = static_cast<int>(minX);
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::CalcMaxYValue()
{
	if (!(graphPoints.size() >= 1))
		return;
	int maxY = graphPoints[0].y;
    for (unsigned int i = 1; i < this->graphPoints.size(); i++)
	{
		if (this->graphPoints[i].y > maxY)
		{
			maxY = graphPoints[i].y;
		}
	}
	this->maxYValue = maxY;
}
/*----------------------------------------------------------------------------------------------*/
void LineGraph::CalcMinYValue()
{
	if (!(graphPoints.size() >= 1))
		return;

	int minY = graphPoints[0].y;
	for (unsigned int i = 1; i < this->graphPoints.size(); i++)
	{
		if (this->graphPoints[i].y < minY)
		{
			minY = graphPoints[i].y;
		}
	}
	this->minYValue = minY;
}
/*----------------------------------------------------------------------------------------------*/
bool LineGraph::ContainsNegativeYValues()
{
	for (unsigned int i = 0; i < this->graphPoints.size(); i++)
	{
		if (this->graphPoints[i].y < 0)
			return true;
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/
int LineGraph::GetNumPoints()
{
    return this->graphPoints.size();
}
/*----------------------------------------------------------------------------------------------*/
bool LineGraph::ContainsPositiveYValues()
{
	for (unsigned int i = 0; i < this->graphPoints.size(); i++)
	{
		if (this->graphPoints[i].y > 0)
			return true;
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/



