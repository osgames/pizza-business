/*----------------------------------------------------------------------------------------------*/
#include "wnd_contracts.h"
#include "datarest.h"
#include "datapool.h"
#include "pbutils.h"
#include <wx/intl.h>
#include <wx/msgdlg.h>
/*----------------------------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CContractsDlg, wxDialog)
	EVT_BUTTON(CD_BUTTON_OK, CContractsDlg::OnOkClicked)
	EVT_BUTTON(CD_BUTTON_ADDCONTRACT, CContractsDlg::OnAddContractClicked)
	EVT_BUTTON(CD_BUTTON_EDITCONTRACT, CContractsDlg::OnEditContractClicked)
    EVT_BUTTON(CD_BUTTON_DELETECONTRACT, CContractsDlg::OnRemoveContractClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(CContractEditor, wxDialog)
	EVT_BUTTON(CE_BUTTON_OK, CContractEditor::OnOkClicked)
	EVT_BUTTON(CE_BUTTON_CANCEL, CContractEditor::OnCancelClicked)
	EVT_RADIOBUTTON(-1, CContractEditor::OnRadioButton)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CContractsDlg implementation										   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CContractsDlg::CContractsDlg(wxWindow *parent, ContractManager *pContractManager, CDataPool *myData) : 
				wxDialog(parent, -1, _("Contract Manager"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER)
{
	//this->m_pRestaurant = pRestaurant;
    this->contractManager = pContractManager;
	this->m_pData = myData;

	this->currentContracts =  new wxStaticText(this, -1, _("Current Contracts:"), wxPoint(10, 10));
	this->contractList = new wxListCtrl(this, -1, wxDefaultPosition, wxSize(400, 250), wxLC_REPORT);
	wxSize listSize = this->contractList->GetSize();
	this->contractList->InsertColumn(0, "Contract Info", wxLIST_FORMAT_LEFT, (int)(.7*listSize.GetWidth()));
	this->contractList->InsertColumn(1, "Contract Type", wxLIST_FORMAT_LEFT, (int)(.25*listSize.GetWidth()));

	this->addContract = new wxButton(this, CD_BUTTON_ADDCONTRACT, _("New Contract"), wxDefaultPosition);
	this->editContract = new wxButton(this, CD_BUTTON_EDITCONTRACT, _("Edit Contract"), wxDefaultPosition);
	this->deleteContract = new wxButton(this, CD_BUTTON_DELETECONTRACT, _("Remove Contract"), wxDefaultPosition);
	this->line = new wxStaticLine(this, -1, wxDefaultPosition, wxSize(300, 1), wxLI_HORIZONTAL);
	this->ok =  new wxButton(this, CD_BUTTON_OK, _("Ok"), wxDefaultPosition);

	this->mainSizer =  new wxBoxSizer(wxVERTICAL);
	this->mainSizer->Add(this->currentContracts, 0, wxALL, 10);
	this->mainSizer->Add(this->contractList, 1, wxEXPAND|wxLEFT|wxRIGHT, 10);

	this->middleBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->middleBtnSizer->Add(this->addContract, 0, wxALIGN_RIGHT|wxRIGHT, 10);
	this->middleBtnSizer->Add(this->editContract, 0, wxALIGN_RIGHT|wxRIGHT, 10);
	this->middleBtnSizer->Add(this->deleteContract, 0, wxALIGN_RIGHT);
	this->mainSizer->Add(this->middleBtnSizer, 0, wxALIGN_RIGHT|wxALL, 10);

	this->mainSizer->Add(this->line, 0, wxEXPAND|wxLEFT|wxRIGHT, 10);
	this->mainSizer->Add(this->ok, 0, wxTOP|wxALIGN_CENTER_HORIZONTAL, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);

	this->RefreshContractsList();

    this->Centre();
}
/*----------------------------------------------------------------------------------------------*/
CContractsDlg::~CContractsDlg()
{
	this->currentContracts->Destroy();
	this->contractList->Destroy();
	this->addContract->Destroy();
	this->editContract->Destroy();
	this->deleteContract->Destroy();
	this->line->Destroy();
	this->ok->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
void CContractsDlg::RefreshContractsList()
{
	// Refresh list box
	std::list<Contract *> myList;
	myList = this->contractManager->GetContracts();
	wxString temp;
	char desc[256];
	int myIndex;

	this->contractList->DeleteAllItems();

	std::list<Contract *>::iterator i;
	for (i = myList.begin(); i != myList.end(); i++)
	{
		temp = "";
		myIndex = this->contractList->GetItemCount();
		if ((*i)->itemType == INGREDIENTS)
		{
			//((CIngredient *)(*i)->itemToBuy)->GetName(desc);
            this->m_pData->GetIngredientByID((*i)->itemID)->GetName(desc);
			temp << _("Buy") << " " << (*i)->quantityToBuy << " "
				 << desc << " " << _("every") << " " << (*i)->frequency << " " << _("days");
			this->contractList->InsertItem(myIndex, temp);
			this->contractList->SetItem(myIndex, 1, "Ingredient");
		}
		else if ((*i)->itemType == ADVERTISEMENTS)
		{
			//((CAdvertisement *)(*i)->itemToBuy)->GetDescription(desc);
            this->m_pData->GetAdvertisementByID((*i)->itemID)->GetDescription(desc);
			temp << _("Buy") << " " << (*i)->quantityToBuy << " "
				 << desc << " " << _("every") << " " << (*i)->frequency << " " << _("days");
			this->contractList->InsertItem(myIndex, temp);
			this->contractList->SetItem(myIndex, 1, "Advertisement");
		}
		this->contractList->SetItemData(myIndex, (long)(*i));		
	}
	this->Refresh();
}
/*----------------------------------------------------------------------------------------------*/
void CContractsDlg::OnOkClicked(wxMouseEvent &e)
{
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CContractsDlg::OnAddContractClicked(wxMouseEvent &e)
{
	Contract myContract;
	CContractEditor dlg(this, &myContract, "Add", this->m_pData);

	if (dlg.ShowModal() == wxOK)
	{
		this->contractManager->AddContract(myContract);
	}
	this->RefreshContractsList();
}
/*----------------------------------------------------------------------------------------------*/
void CContractsDlg::OnEditContractClicked(wxMouseEvent &e)
{
	Contract *myContract;
	int count = this->contractList->GetItemCount();

	for (int i = 0; i < count; i++)
	{
		if (this->contractList->GetItemState(i, wxLIST_STATE_SELECTED))
		{
			myContract = (Contract *)this->contractList->GetItemData(i);
			CContractEditor dlg(this, myContract, "Edit", this->m_pData);
			dlg.ShowModal();
			this->RefreshContractsList();
			return;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CContractsDlg::OnRemoveContractClicked(wxMouseEvent &e)
{
    Contract *myContract;
    int count = this->contractList->GetItemCount();

    for (int i = 0; i < count; i++)
    {
        if (this->contractList->GetItemState(i, wxLIST_STATE_SELECTED))
        {
            myContract = (Contract *)this->contractList->GetItemData(i);
            this->contractManager->RemoveContract(myContract);
            this->RefreshContractsList();
            return;
        }
    }
    this->RefreshContractsList();
}
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	CContractEditor implementation										   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
CContractEditor::CContractEditor(wxWindow *parent, Contract *contract, wxString dlgType, CDataPool *data):
				wxDialog(parent, -1, _("Contract Editor"), wxDefaultPosition, wxSize(400, 400), wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER)
{
	this->myDlgType = dlgType;
	this->currContract = contract;
	this->m_pData = data;

	this->radioBox = new wxStaticBox(this, -1, _("Contract Item Type"), wxDefaultPosition, wxSize(380, 50));
	this->advertisements = new wxRadioButton(this, CE_RADIOBUTTON_ADS, _("Advertisements"), wxDefaultPosition, 
												wxDefaultSize, wxRB_GROUP);
	this->ingredients = new wxRadioButton(this, CE_RADIOBUTTON_INGS, _("Ingredients")); 
	this->selectItem = new wxStaticText(this, -1, _("Select the contract item"), wxDefaultPosition);
	this->contractItemSelector = new wxComboBox(this, -1, "", wxDefaultPosition, wxSize(420, 20));
	this->enterQuantity = new wxStaticText(this, -1, _("Enter the amount of items to be bought:"), wxDefaultPosition);
	this->quantity = new wxTextCtrl(this, -1, "", wxDefaultPosition);
	this->buyThisItem = new wxStaticText(this, -1, _("Buy this item every"), wxDefaultPosition);
	this->days = new wxStaticText(this, -1, _("days"), wxDefaultPosition);
	this->frequency = new wxTextCtrl(this, -1, "", wxDefaultPosition);
	this->line = new wxStaticLine(this, -1);
	this->ok = new wxButton(this, CE_BUTTON_OK, _("Ok"), wxDefaultPosition);
	this->cancel = new wxButton(this, CE_BUTTON_CANCEL, _("Cancel"), wxDefaultPosition);

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->staticBoxSizer = new wxStaticBoxSizer(this->radioBox, wxHORIZONTAL);
	this->staticBoxSizer->Add(this->advertisements, 0, wxLEFT|wxALIGN_CENTER_VERTICAL, 10);
	this->staticBoxSizer->Add(this->ingredients, 0, wxLEFT|wxALIGN_CENTER_VERTICAL, 20);
	this->mainSizer->Add(this->staticBoxSizer, 0, wxEXPAND|wxALL, 10);
	this->mainSizer->Add(this->selectItem, 0, wxLEFT|wxTOP, 10);
	this->mainSizer->Add(this->contractItemSelector, 0, wxEXPAND|wxALL, 10);
	this->middle1Sizer = new wxBoxSizer(wxHORIZONTAL);
	this->middle1Sizer->Add(this->enterQuantity, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10);
	this->middle1Sizer->Add(this->quantity, 0, wxALIGN_CENTER_VERTICAL);
	this->mainSizer->Add(this->middle1Sizer, 0, wxLEFT|wxRIGHT, 10);
	this->middle2Sizer = new wxBoxSizer(wxHORIZONTAL);
	this->middle2Sizer->Add(this->buyThisItem, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10);
	this->middle2Sizer->Add(this->frequency, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10);
	this->middle2Sizer->Add(this->days, 0, wxALIGN_CENTER_VERTICAL);
	this->mainSizer->Add(this->middle2Sizer, 0, wxALL, 10);
	this->mainSizer->Add(this->line, 0, wxEXPAND|wxALL, 10);
	this->bottomBtnSizer = new wxBoxSizer(wxHORIZONTAL);
	this->bottomBtnSizer->Add(this->ok, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->bottomBtnSizer->Add(this->cancel, 0, wxALIGN_RIGHT);
	this->mainSizer->Add(this->bottomBtnSizer, 0, wxALIGN_RIGHT|wxRIGHT|wxLEFT, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);

	this->RefreshComboBox();
	this->contractItemSelector->SetSelection(0);

	char buffer[256];
	int index;
	if (myDlgType == "Edit")
	{
		// Initialize values to contract's current values
		if (this->currContract->itemType == ADVERTISEMENTS)
		{
			this->advertisements->SetValue(true);
			this->ingredients->SetValue(false);
			this->RefreshComboBox();
			//((CAdvertisement *)(this->currContract->itemToBuy))->GetDescription(buffer);
            this->m_pData->GetAdvertisementByID(this->currContract->itemID)->GetDescription(buffer);
			index = this->contractItemSelector->FindString(buffer);
            if (index != -1)
				this->contractItemSelector->SetSelection(index);
			this->quantity->SetValue(ToString(this->currContract->quantityToBuy).c_str());
			this->frequency->SetValue(ToString(this->currContract->frequency).c_str());
		}
		else if (this->currContract->itemType == INGREDIENTS)
		{
			this->ingredients->SetValue(true);
			this->advertisements->SetValue(false);
			this->RefreshComboBox();
			//((CIngredient *)(this->currContract->itemToBuy))->GetName(buffer);
            this->m_pData->GetIngredientByID(this->currContract->itemID)->GetName(buffer);
			index = this->contractItemSelector->FindString(buffer);
			if (index != -1)
				this->contractItemSelector->SetSelection(index);
			this->quantity->SetValue(ToString(this->currContract->quantityToBuy).c_str());
			this->frequency->SetValue(ToString(this->currContract->frequency).c_str());
		}
	}
	this->Centre();
}
/*----------------------------------------------------------------------------------------------*/
CContractEditor::~CContractEditor()
{
	this->radioBox->Destroy();
	this->advertisements->Destroy();
	this->ingredients->Destroy();
	this->selectItem->Destroy();
	this->contractItemSelector->Destroy();
	this->enterQuantity->Destroy();
	this->buyThisItem->Destroy();
	this->days->Destroy();
	this->quantity->Destroy();
	this->frequency->Destroy();
	this->ok->Destroy();
	this->cancel->Destroy();
	this->line->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
void CContractEditor::OnOkClicked(wxMouseEvent &e)
{
    //long temp;
    void* temp;
    temp = this->contractItemSelector->GetClientData(this->contractItemSelector->GetSelection());
	if (this->advertisements->GetValue())
    {
		this->currContract->itemType = ADVERTISEMENTS;
        if (temp != NULL)
            this->currContract->itemID = ((CAdvertisement *)(temp))->GetId();
    }
	else
    {
		this->currContract->itemType = INGREDIENTS;
        if (temp != NULL)
            this->currContract->itemID = ((CIngredient *)(temp))->GetId();
    }

	//this->currContract->itemToBuy = (long)this->contractItemSelector->GetClientData(this->contractItemSelector->GetSelection());

	if (this->quantity->GetValue().IsNumber() && this->quantity->GetValue() != "")
	{
		this->currContract->quantityToBuy = StringTo<unsigned int>(this->quantity->GetValue().c_str());
	}
	else
	{
		wxMessageDialog dlg(this, "Please enter a valid integer for the quantity", "Input Invalid", wxOK);
		dlg.ShowModal();
		return;
	}

	if (this->frequency->GetValue().IsNumber() && this->frequency->GetValue() != "")
	{
		this->currContract->frequency = StringTo<unsigned int>(this->frequency->GetValue().c_str());
	}
	else
	{
		wxMessageDialog dlg(this, "Please enter a valid integer for the frequency", "Input Invalid", wxOK);
		dlg.ShowModal();
		return;
	}
	this->currContract->daysUntilNextBuy = this->currContract->frequency - 1;
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CContractEditor::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*----------------------------------------------------------------------------------------------*/
void CContractEditor::RefreshComboBox()
{
	char desc[256];

	this->contractItemSelector->Clear();
	if (this->advertisements->GetValue())
	{
		for (unsigned int i = 1; i <= this->m_pData->m_AdList.GetCount(); i++)
		{
			CAdCategory *pCategory = this->m_pData->m_AdList.GetItem(i);
			for (unsigned int j = 1; j <= pCategory->AdList.GetCount(); j++)
			{
				CAdvertisement *pAd = pCategory->AdList.GetItem(j);
				pAd->GetDescription(desc);
				this->contractItemSelector->Append(desc, pAd);
			}
		}
	}
	else
	{
		for (unsigned int i = 1; i <= this->m_pData->m_IngList.GetCount(); i++)
		{
			CIngCategory *pCategory = this->m_pData->m_IngList.GetItem(i);
			for (unsigned int j = 1; j <= pCategory->IngList.GetCount(); j++)
			{
				CIngredient *pIng = pCategory->IngList.GetItem(j);
				pIng->GetName(desc);
				this->contractItemSelector->Append(desc, pIng);
			}
		}
	}
	this->contractItemSelector->SetSelection(0);
}
/*----------------------------------------------------------------------------------------------*/
void CContractEditor::OnRadioButton(wxMouseEvent &e)
{
	this->RefreshComboBox();
}
/*----------------------------------------------------------------------------------------------*/



