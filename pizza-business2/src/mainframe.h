#ifndef _JS_MAINFRAME_H
#define _JS_MAINFRAME_H
/*---------------------------------------------------------------------------*/
#include "capp.h"	// includes "mainframe.h"
#include "mainmenu.h"
/*---------------------------------------------------------------------------*/
class CMainPanel : public wxPanel
{
public:
	// constructor/destructor
	CMainPanel(wxFrame* frame, int x, int y, int width, int height);
	~CMainPanel();

	// event handlers
	void OnBank(wxCommandEvent& event);
	void OnBuySell(wxCommandEvent& event);
	void OnEndTurn(wxCommandEvent& event);
	void OnMainMenu(wxCommandEvent& event);
	void OnPizzas(wxCommandEvent& event);
	void OnStaff(wxCommandEvent& event);
	void OnStats(wxCommandEvent& event);

	void OnPaint(wxPaintEvent& event);
	void OnPaintButton(wxEvent &e);

public:
	void Initialize();
	void CreateNavButtons();
	void ShowMainMenu();
	void ShowStatsDialog();

	CRestaurant* GetCurrentRestaurant() { return m_pCurRestaurant; }
	void SetCurrentRestaurant(CRestaurant *pRestaurant) { m_pCurRestaurant = pRestaurant; }

private:
	DECLARE_EVENT_TABLE();

	// controls
	CMainMenu *m_MainMenu;
	CHoverButton *m_pBtnStaff;
	CHoverButton *m_pBtnStats;
	CHoverButton *m_pBtnBuySell;
	CHoverButton *m_pBtnPizzas;
	CHoverButton *m_pBtnBank;
	CHoverButton *m_pBtnEndTurn;
	CHoverButton *m_pBtnMainMenu;

	// graphics
	wxBitmap m_BmpUI, m_BmpBk, m_BmpTemp;

private:
	CApplication *m_pApplication;
	CRestaurant *m_pCurRestaurant;
	bool m_bFullScreen;

	unsigned int m_navbar_width;
	unsigned int m_navbar_height;
	unsigned short m_button_width;
	unsigned short m_button_height;
};
/*---------------------------------------------------------------------------*/
class CMainFrame : public wxFrame
{
public:
	// constructor/destructor
	CMainFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
	~CMainFrame();

	// event handlers
	void OnCommand(wxCommandEvent& event);

public:
	unsigned int m_FrameHeight, m_FrameWidth;

private:
	DECLARE_EVENT_TABLE();

	// controls
	CMainPanel *m_Panel;

private:
	CApplication *m_pApplication;
};
/*---------------------------------------------------------------------------*/
// IDs for controls
enum {
	// TODO: put control IDs here
	BUTTON_TEST = 1,
	BTN_STATS = 2,
	BTN_STAFF = 3,
	BTN_BUYSELL = 4,
	BTN_PIZZAS = 5,
	BTN_BANK = 6,
	BTN_ENDTURN = 7,
	BTN_MAINMENU = 8,
};
/*---------------------------------------------------------------------------*/
#endif

