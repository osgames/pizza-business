#include "capp.h"
#include "mainframe.h"
/*---------------------------------------------------------------------------*/
IMPLEMENT_APP(CApplication);
/*---------------------------------------------------------------------------*/
bool CApplication::OnInit()
{
	// initialize the game engine
	if(m_Simulator.Initialize() == false)
		return FALSE;

	// create the main application window
	CMainFrame *frame = new CMainFrame("Pizza Business", wxPoint(50, 50),
		wxSize(800, 600));

	this->SetTopWindow(frame);

	return TRUE;
}
/*---------------------------------------------------------------------------*/



