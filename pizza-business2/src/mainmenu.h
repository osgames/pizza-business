#ifndef _JS_MAINMENU_H
#define _JS_MAINMENU_H
/*---------------------------------------------------------------------------*/
#include "capp.h"
#include "hoverbutton.h"
/*---------------------------------------------------------------------------*/
class CMainMenu : public wxDialog
{
public:
	CMainMenu(wxWindow* parent, wxWindowID id, const wxString& title,
		 const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
		 long style = wxDEFAULT_DIALOG_STYLE, const wxString& name = "dialogBox");
	~CMainMenu();

public:
	// command event handlers
	void OnNewGame(wxCommandEvent& event);
	void OnLoadGame(wxCommandEvent& event);
	void OnSaveGame(wxCommandEvent& event);
	void OnQuit(wxCommandEvent& event);

	// window event handlers
	void OnPaint(wxPaintEvent& event);
	void OnPaintButton(wxEvent &e);
	void OnPressed(wxMouseEvent &e);

private:
	// controls
	CHoverButton *m_BtnNewGame;
	CHoverButton *m_BtnLoadGame;
	CHoverButton *m_BtnSaveGame;
	CHoverButton *m_BtnQuit;

	// graphics
	wxBitmap m_Bmp;

private:
	DECLARE_EVENT_TABLE();
};
/*---------------------------------------------------------------------------*/
// IDs for controls
enum {
	// TODO: put control IDs here
	IDC_NEWGAME		= 1001,
	IDC_QUIT		= 1002,
	IDC_SAVEGAME	= 1003,
	IDC_LOADGAME	= 1004
};
/*---------------------------------------------------------------------------*/
#endif


