#ifndef _JS_DATA_H
#define _JS_DATA_H
/*----------------------------------------------------------------------------------------------*/
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
struct CharDesc {
	char name[1024];
	short factor;
};

struct CharName {
	char name[256];
};

struct MinMax {
	short min;
	short max;
};

struct FileHeader {
	float app_version;
	float format_version;
	// file's data offsets to a header
	// followed by the actual data
	long offset_ages;
	long offset_compentency;
	long offset_firstnames;
	long offset_lastnames;
	long offset_desc;
	long offset_chair_price;
	long offset_ovens;
	long offset_tables;
	long offset_ads;
	long offset_ingredients;
	long offset_recipes;
};

struct TableData {
	char name[256];
	float price;
	short chair_support;
	short table_id;
};

struct OvenData {
	char name[256];
	float price;
	short pizza_support;
	short cooking_time;
	short oven_id;
};

struct ChairPrice {
	float price;
};

struct AdData {
	char name[256];
	float price;
	short factor;
	short ad_id;
};

struct AdCategory {
	List<AdData> Ads;
	char name[256];
};

struct IngData {
	char name[256];
	float price;
	short quality;
	short weight;
	short vender_id;											// reseverd; for furture possibilites
	short ingredient_id;
};

struct IngCategory {
	List<IngData> Ingredients;
	char name[256];
};

struct RecipeData {
	List<short> Ingredients;
	char name[256];
};
/*----------------------------------------------------------------------------------------------*/
// Here are the flags used for PBD_DATA.dwFlags
// to determine which part(s) of the data file to access
//		constant's name		hex		binary representation
#define DATA_AGE			0x0002	// 0000 0000 0000 0010
#define DATA_COMPETENCY		0x0004  // 0000 0000 0000 0100
#define DATA_CHAIRPRICE		0x0008	// 0000 0000 0000 1000
#define DATA_ADS			0x0010	// 0000 0000 0001 0000
#define DATA_INGREDIENTS	0x0020	// 0000 0000 0010 0000
#define DATA_RECIPES		0x0040	// 0000 0000 0100 0000
#define DATA_FIRSTNAMES		0x0080	// 0000 0000 1000 0000
#define DATA_LASTNAMES		0x0100	// 0000 0001 0000 0000
#define DATA_DESC			0x0200	// 0000 0010 0000 0000
#define DATA_TABLES			0x0400	// 0000 0100 0000 0000
#define DATA_OVENS			0x0800	// 0000 1000 0000 0000
#define DATA_ALL			0xFFFF	// 1111 1111 1111 1111
/*----------------------------------------------------------------------------------------------*/
typedef struct tagPBD_DATA {
	unsigned int dwFlags;
	// these structures can be written straight to the file
	FileHeader Header;
	MinMax Age;
	MinMax Competency;
	ChairPrice Chair;

	// special structures (written to file in a specific way)
	List<AdCategory> AdCatList;
	List<IngCategory> IngCatList;
	List<RecipeData> RecipeList;

	// each list must be written to a file following a
	// subheader indicating the number of items in the list
	List<CharName> FirstnameList;
	List<CharName> LastnameList;
	List<CharDesc> DescList;
	List<TableData> TableList;
	List<OvenData> OvenList;
}PBD_DATA;
/*----------------------------------------------------------------------------------------------*/
bool Entities_LoadFile(char* filename, PBD_DATA &data);
#ifdef _ENT_EDITOR
#ifdef _WINDOWS
void Entities_SaveFile(char* filename, PBD_DATA &data);
#endif
#endif
/*----------------------------------------------------------------------------------------------*/
// utility functions - ingredients
IngData* GetIngredientByID(List<IngCategory> *pCatList, short id);
short GetNextAvailableIngID(List<IngCategory> *pCatList);
bool IngIdAvailable(List<IngCategory> *pCatList, short id);

// utility functions - advertisements
short GetNextAvailableAdID(List<AdCategory> *pCatList);
bool AdIdAvailable(List<AdCategory> *pCatList, short id);

// utility functions - tables
short GetNextAvailableTableID(List<TableData> *pTableList);
bool TableIdAvailable(List<TableData> *pTableList, short id);

// utility functions - ovens
short GetNextAvailableOvenID(List<OvenData> *pOvenList);
bool OvenIdAvailable(List<OvenData> *pOvenList, short id);
/*----------------------------------------------------------------------------------------------*/
#endif


