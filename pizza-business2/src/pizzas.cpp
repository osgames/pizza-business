#include "pizzas.h"
#include "datapool.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CPizza implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CPizza::CPizza()
{
	// do not delete the ingredients in the list
	// cause they are pointers to the ingredient
	// listed in the data pool
	m_IngList.SetDeleteFlag(false);

	strcpy(this->m_Data.name, "");
}
/*----------------------------------------------------------------------------------------------*/
bool CPizza::AddIngredient(CIngredient *pIngredient)
{
	if(!pIngredient) return false;

	// be sure the ingredient is not already in the list
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		CIngredient *pTestIng = this->m_IngList.GetItem(c);
		if(pIngredient->GetId() == pTestIng->GetId())
			return false;
	}

	// now just append the ingredient to the list
	this->m_IngList.AddItem(pIngredient);
	return true;
}
/*----------------------------------------------------------------------------------------------*/
CIngredient* CPizza::GetIngredient(unsigned int index)
{
	return this->m_IngList.GetItem(index);
}
/*----------------------------------------------------------------------------------------------*/
float CPizza::GetProductionCost()
{
	// add up the prices for all the ingredients
	// in the ingredient list

	float total_cost = 0.0f;

	unsigned int count = this->GetIngredientCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngredient *pIngredient = this->GetIngredient(c);
		total_cost += pIngredient->GetPrice();
	}

	return total_cost;
}
/*----------------------------------------------------------------------------------------------*/
bool CPizza::IsIngredientIncluded(CIngredient *pIngredient)
{
	unsigned int count = this->GetIngredientCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngredient *pIng = this->GetIngredient(c);
		if(pIng->GetId() == pIngredient->GetId())
			return true;
	}

	return false;
}
/*----------------------------------------------------------------------------------------------*/
void CPizza::LoadFromFile(std::ifstream &infile, CDataPool &dataPool)
{
	// read the m_Data structure
	infile.read((char*)&m_Data, sizeof(m_Data));

	// read the list of the ingredients and build the pizza
	for(unsigned int c = 1 ; c <= m_Data.ingCount ; c++)
	{
		PizzaIng pizzaIng;
		infile.read((char*)&pizzaIng, sizeof(PizzaIng));

		this->AddIngredient(dataPool.GetIngredientByID(static_cast<short>(pizzaIng.iId)));
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizza::RemoveAllIngredients()
{
	this->m_IngList.DeleteAllItems();
}
/*----------------------------------------------------------------------------------------------*/
void CPizza::RemoveIngredient(CIngredient *pIngredient)
{
	// find the ingredient and remove it from the list
	unsigned int count = this->m_IngList.GetCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngredient *pIng = this->m_IngList.GetItem(c);
		if(pIng->GetId() == pIngredient->GetId())
		{
			this->m_IngList.DeleteItem(c);
			return;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizza::SaveToFile(std::ofstream &outfile)
{
	// write the m_Data structure
	m_Data.ingCount = this->m_IngList.GetCount();
	outfile.write((const char*)&m_Data, sizeof(m_Data));

	// write the list of the ingredients used for the pizza
	for(unsigned int c = 1 ; c <= m_Data.ingCount ; c++)
	{
		CIngredient *pIngredient = this->m_IngList.GetItem(c);

		PizzaIng pizzaIng;
		pizzaIng.iId = pIngredient->GetId();
		outfile.write((const char*)&pizzaIng, sizeof(PizzaIng));
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CPizzaObject implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CPizzaObject::CPizzaObject(CPizza *pPizza)
{
	// copy the name of the pizza
	char name[256];
	pPizza->GetName(name);
	m_Pizza.SetName(name);

	// copy the ingredients
	unsigned int count = pPizza->GetIngredientCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngredient *pIng = pPizza->GetIngredient(c);
		m_Pizza.AddIngredient(pIng);
	}

	// set the price to the default
	this->SetPrice(0.0f);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaObject::LoadFromFile(std::ifstream &infile, CDataPool &dataPool)
{
	// read the m_Data structure from the file
	infile.read((char*) &m_Data, sizeof(m_Data));

	// load the pizza's data from file
	this->m_Pizza.LoadFromFile(infile, dataPool);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaObject::SaveToFile(std::ofstream &outfile)
{
	// write the m_Data structure to the file
	outfile.write((const char*) &m_Data, sizeof(m_Data));

	// write the CPizza class object to the file
	this->m_Pizza.SaveToFile(outfile);
}
/*----------------------------------------------------------------------------------------------*/



