#ifndef _JS_MANAGEMENT_H_
#define _JS_MANAGEMENT_H_
/*----------------------------------------------------------------------------------------------*/
#include <list>
#include <fstream>
/*----------------------------------------------------------------------------------------------*/
class Contract;
class CRestaurant;
/*----------------------------------------------------------------------------------------------*/
enum ContractItem
{
	INGREDIENTS = 1,
	ADVERTISEMENTS,
	NO_ITEM
};
/*----------------------------------------------------------------------------------------------*/
class ContractManager
{
public:
	ContractManager();
	~ContractManager();
    void SetRestaurant(CRestaurant *pRestaurant);
	void AddContract(Contract &contractToAdd);
	void RemoveContract(Contract *contractToRemove);
	const std::list<Contract *>& GetContracts();  
	void DecrementContractCounters();
	void PerformContract(std::list<Contract *>::iterator i);

	void LoadFromFile(std::ifstream &infile, unsigned int count);
	void SaveToFile(std::ofstream &outfile);

private:
	std::list<Contract *> contracts;
	CRestaurant *m_pRestaurant;
};
/*----------------------------------------------------------------------------------------------*/
class Contract
{
public:
	Contract();

public:
	unsigned int daysUntilNextBuy;
	unsigned int frequency; // Contract is performed every <frequency> days
	ContractItem itemType;
	//long itemToBuy;
	unsigned int quantityToBuy;
    unsigned short itemID;
};
/*----------------------------------------------------------------------------------------------*/
#endif


