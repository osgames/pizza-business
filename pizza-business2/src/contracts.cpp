#include "contracts.h"
#include "datarest.h"
#include "datapool.h"
#include "capp.h"
#include <list>
#include <wx/wx.h>
/*----------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------+
|																		   |						 
|	ContractManager implementation										   |						 
|																		   |						 
+-------------------------------------------------------------------------*/
ContractManager::ContractManager()
{}
/*----------------------------------------------------------------------------------------------*/
ContractManager::~ContractManager()
{
	std::list<Contract *>::iterator i;
	for (i = this->contracts.begin(); i != this->contracts.end(); i++)
	{
		if ( (*i) != NULL )
			delete (*i);
	}
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::AddContract(Contract &contractToAdd)
{
	Contract* newContract = new Contract;
	(*newContract) = contractToAdd;
	this->contracts.push_back(newContract);
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::RemoveContract(Contract *contractToRemove)
{
	std::list<Contract *>::iterator it;
	std::list<Contract *>::iterator end = this->contracts.end();

	for (it = this->contracts.begin(); it != end; it++)
	{
		if ((*it) == contractToRemove)
		{
			delete (*it);
			this->contracts.erase(it);
			break;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::DecrementContractCounters()
{
	std::list<Contract *>::iterator i;
	for (i = this->contracts.begin(); i != this->contracts.end(); i++)
	{
		if ((*i)->daysUntilNextBuy == 0)
		{
			this->PerformContract(i);
			(*i)->daysUntilNextBuy = (*i)->frequency - 1;
		}
		else
			(*i)->daysUntilNextBuy -= 1;
	}
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::PerformContract(std::list<Contract *>::iterator i)
{
    CApplication *pApp = &wxGetApp();
    CDataPool *pool = &pApp->m_Simulator.m_DataPool;
	ContractItem cItem = (*i)->itemType;
 
	switch (cItem)
	{
	case INGREDIENTS:
		//this->m_pRestaurant->Ingredient_Buy((CIngredient *)(*i)->itemToBuy, (*i)->quantityToBuy);
        this->m_pRestaurant->Ingredient_Buy(pool->GetIngredientByID((*i)->itemID), (*i)->quantityToBuy);
		break;

	case ADVERTISEMENTS:
		//this->m_pRestaurant->Advertisement_Buy((CAdvertisement *)(*i)->itemToBuy, (*i)->quantityToBuy);
        this->m_pRestaurant->Advertisement_Buy(pool->GetAdvertisementByID((*i)->itemID), (*i)->quantityToBuy);
		break;
	}
}
/*----------------------------------------------------------------------------------------------*/
const std::list<Contract *>& ContractManager::GetContracts()
{
	return this->contracts;
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::SetRestaurant(CRestaurant *pRestaurant)
{
    this->m_pRestaurant = pRestaurant;
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::LoadFromFile(std::ifstream &infile, unsigned int count)
{
	// read the specified amount of contracts from the file
	// and add them to the list
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		Contract contract;
		infile.read((char*)&contract, sizeof(Contract));
		this->AddContract(contract);
	}
}
/*----------------------------------------------------------------------------------------------*/
void ContractManager::SaveToFile(std::ofstream &outfile)
{
	std::list<Contract *>::iterator i;
	for (i = this->contracts.begin(); i != this->contracts.end(); i++)
		outfile.write((const char*)(*i), sizeof(Contract));
}
/*-------------------------------------------------------------------------+
|																		   |						 
|	Contract implementation										           |						 
|																		   |						 
+-------------------------------------------------------------------------*/
Contract::Contract()
{
	this->daysUntilNextBuy = 0;
	this->frequency = 0;
	this->itemType = NO_ITEM;
	this->quantityToBuy = 0;
    this->itemID = 0;
}
/*----------------------------------------------------------------------------------------------*/


