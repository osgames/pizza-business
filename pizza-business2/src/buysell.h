#ifndef _JS_BUYSELL_H
#define _JS_BUYSELL_H
/*----------------------------------------------------------------------------------------------*/
#include <string>
#include <fstream>

#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
class CAdvertisement;
class CIngredient;
/*----------------------------------------------------------------------------------------------*/
class CAdCategory
{
public:
	void GetName(char* buffer) { strcpy(buffer, m_Name); }
	void SetName(char* name) { strcpy(m_Name, name); }

public:
	List<CAdvertisement> AdList;

private:
	char m_Name[256];
};
/*----------------------------------------------------------------------------------------------*/
class CAdvertisement
{
public:
	CAdvertisement()
	{
		SetDescription("");
		SetPrice(0.0f);
		SetId(0);
	}
	~CAdvertisement() {}

public:
	void GetDescription(char *buffer) { strcpy(buffer, m_Data.strDescription); }
	void SetDescription(char *desc) { strcpy(m_Data.strDescription, desc); }

	short GetFactor() { return m_Data.iFactor; }
	void SetFactor(short factor) { m_Data.iFactor = factor; }

	short GetId() { return m_Data.iId; }
	void SetId(short id) { m_Data.iId = id; }

	float GetPrice() { return m_Data.fPrice; }
	void  SetPrice(float price) { m_Data.fPrice = price; }

private:
	struct {
		char strDescription[1024];
		float fPrice;
		short iFactor;
		short iId;
	} m_Data;
};
/*----------------------------------------------------------------------------------------------*/
class CIngCategory
{
public:
	void GetName(char* buffer) { strcpy(buffer, m_Name); }
	void SetName(char* name) { strcpy(m_Name, name); }

public:
	List<CIngredient> IngList;

private:
	char m_Name[256];
};
/*----------------------------------------------------------------------------------------------*/
class CIngredient
{
public:
	CIngredient(CIngCategory *pIngCategory)
	{
		this->m_pCategory = pIngCategory;

		this->SetName("");
		this->SetDescription("");
		this->SetVenderID(0);
		this->SetId(0);
	}
	~CIngredient() {}

public:
	CIngCategory* GetCategory() { return m_pCategory; }

	void GetName(char* buffer) { strcpy(buffer, m_Data.strName); }
	void SetName(char* name) { strcpy(m_Data.strName, name); }

	void GetDescription(char* buffer) { strcpy(buffer, m_Data.strDescription); }
	void SetDescription(char* desc) { strcpy(m_Data.strDescription, desc); }

	short GetId() { return m_Data.iId; }
	void SetId(short id) { m_Data.iId = id; }

	float GetPrice() { return m_Data.fPrice; }
	void SetPrice(float price) { m_Data.fPrice = price; }

	short GetQuality() { return m_Data.iQuality; }
	void SetQuality(short quality) { m_Data.iQuality = quality; }

	short GetWeight() { return m_Data.iWeight; }
	void SetWeight(short weight) { m_Data.iWeight = weight; }

	short GetVenderID() { return m_Data.iVenderID; }
	void SetVenderID(short id) { m_Data.iVenderID = id; }

private:
	struct {
		char strName[256];
		char strDescription[1024];
		float fPrice;
		short iQuality;
		short iWeight;
		short iVenderID;
		short iId;
	} m_Data;

	CIngCategory *m_pCategory;
};
/*----------------------------------------------------------------------------------------------*/
struct AdItem {
	CAdvertisement *pAd;
	unsigned int quantity;
};

struct AdFileData {
	short iId;
	unsigned int quantity;
};
/*----------------------------------------------------------------------------------------------*/
// NOTE: the following structures are used for organizational purposes of the
// ingredients in the CRestaurant class
struct IngItem {
	short life_left;
};

struct Ingredient {
	CIngredient *pIngredient;
	List<IngItem> IngList;
};

struct IngCat {
	CIngCategory *pCategory;
	List<Ingredient> ingredients;
};

struct IngFileData {
	short id;
	short life_left;
};
/*----------------------------------------------------------------------------------------------*/
#endif



