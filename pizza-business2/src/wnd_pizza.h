#ifndef _JS_WND_PIZZA_H
#define _JS_WND_PIZZA_H
/*----------------------------------------------------------------------------------------------*/
#include <wx/wx.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/spinctrl.h>
#include <wx/listbox.h>
#include <wx/statline.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#undef new 
#include "pizzas.h"
#include "datarest.h"
/*----------------------------------------------------------------------------------------------*/
class CPizzasDlg : public wxDialog
{
public:
	CPizzasDlg(wxWindow* parent, CRestaurant* pRestaurant);
	~CPizzasDlg();
	//void SetRestaurant(CRestaurant *pRestaurant) { m_pRestaurant = pRestaurant; }

public:
	CPizzaObject* GetSelectedPizza();
	void RefreshPizzaListBox();

protected:
	void OnPaint(wxPaintEvent &e);
	void OnBuyIngsClicked(wxMouseEvent &e);
	void OnEditClicked(wxMouseEvent &e);
	void OnOkClicked(wxMouseEvent &e);
	void OnDeleteClicked(wxMouseEvent &e);
	void OnAutoPriceChecked(wxCommandEvent &e);
	void OnSpinControl(wxSpinEvent &e);
	void OnNewPizzaClicked(wxCommandEvent &e);

private:
	wxCheckBox* m_pAutoPricingCB;
	wxSpinCtrl* m_pAutoPricingSpin;
	wxListBox* m_pPizzasListBox;
	wxStaticLine* m_pStaticLine;
	wxStaticText* enableAuto;
	wxStaticText* percentIncrease;
	wxButton* m_pNewButton;
	wxButton* m_pEditButton;
	wxButton* m_pDeleteButton;
	wxButton* m_pOkButton;
	wxButton* m_pBuyIngsButton;
	wxBoxSizer* mainSizer;
	wxBoxSizer* topSizer;
	wxBoxSizer* midButtonsSizer;
	wxBoxSizer* editBtnSizer;

private:
	CRestaurant *m_pRestaurant;

	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	PD_CHECKBOX = 3,
	PD_SPINCTRL = 4, 
	PD_LISTBOX = 5,
	PD_BUTTON_NEW = 6,
	PD_BUTTON_EDIT = 7,
	PD_BUTTON_DELETE = 8,
	PD_BUTTON_OK = 9,
	PD_BUTTON_BUYING = 10
};
/*----------------------------------------------------------------------------------------------*/
class CPizzaEditDlg : public wxDialog
{
public:
	CPizzaEditDlg(wxWindow* parent, CRestaurant *pRestaurant, CPizzaObject* pPizza);
	~CPizzaEditDlg();

protected:
	// Event handling functions
	void OnPaint(wxPaintEvent &e);
	void OnOkClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);
	void OnAddIngClicked(wxMouseEvent &e);
	void OnRemoveIngClicked(wxMouseEvent &e);

private:
	wxStaticBox* m_pBox;
	wxTextCtrl* m_pPizzaName;
	wxTextCtrl* m_pPriceEdit;
	wxStaticLine* m_pLine;
	wxButton* m_pOkButton;
	wxButton* m_pCancelButton;
	wxButton* m_pAddIngButton;
	wxButton* m_pRemoveIngButton;
	wxStaticText* pricePizza;
	wxStaticText* autoPriceEnabled;
	wxStaticText* productionPrice;
	wxStaticText* pizzaName;
	wxBoxSizer* mainSizer;
	wxStaticBoxSizer *boxSizer;
	wxBoxSizer *mainInsideBoxSizer, *topInsideBoxSizer, *bottomBtnSizer;
	wxListBox *m_ingListBox;
    wxBoxSizer* pizzaNameSizer;

private:
	void RefreshIngListbox();

private:
	CRestaurant* m_pRestaurant;
	CPizzaObject *m_pPizza;

	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	PED_EDITBOX = 5,
	PED_BUTTON_OK = 6,
	PED_BUTTON_CANCEL = 7,
	PED_BUTTON_ADDING = 9,
	PED_BUTTON_REMING = 10,
	PED_PIZZANAME = 11
};
/*----------------------------------------------------------------------------------------------*/
class CPizzaIngDlg : public wxDialog
{
public:
	CPizzaIngDlg(wxWindow* parent, CRestaurant *pRestaurant, CPizzaObject* pPizza);
	~CPizzaIngDlg();

protected:
	// Event handling functions
	void OnPaint(wxPaintEvent &e);
	void OnOkClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);

private:
	void PopulateListBox();

private:
	wxListBox *m_ingListBox;
	wxButton* m_pOkButton;
	wxButton* m_pCancelButton;
    wxBoxSizer* mainSizer;
    wxBoxSizer* btnSizer;

private:
	CPizzaObject* m_pPizza;
	CRestaurant * m_pRestaurant;
	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	PID_LISTBOX = 1,
	PID_BUTTON_OK = 2,
	PID_BUTTON_CANCEL = 3
};
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
#endif

