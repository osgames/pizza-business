#include "staff.h"
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CEmployee implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CEmployee::CEmployee()
{
	m_EmployedAt = 0;
	this->SetTitle(TITLE_UNEMPLOYED);
	this->SetSalary(0.0f);
	this->SetCompetency(0);
	this->SetDaysEmployeed(0);
	this->ClearStatistics();
}
/*----------------------------------------------------------------------------------------------*/
bool CEmployee::ClearStatistics()
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			return this->CChef::ClearStatistics();
		case TITLE_WAITER:
			return this->CWaiter::ClearStatistics();
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CEmployee::GetStatistic(int id, int index)
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			return this->CChef::GetStats(id, index);
		case TITLE_WAITER:
			return this->CWaiter::GetStats(id, index);
	}
	return 0;	
}
/*----------------------------------------------------------------------------------------------*/
void CEmployee::LoadFromFile(std::ifstream &infile)
{
	// read the data from the file
	infile.read((char*) &this->m_Data, sizeof(m_Data));

	// read the statistics
	this->CManager::LoadFromFile(infile);
	this->CWaiter::LoadFromFile(infile);
	this->CChef::LoadFromFile(infile);
}
/*----------------------------------------------------------------------------------------------*/
void CEmployee::SaveToFile(std::ofstream &outfile)
{
	// write the data to the file
	outfile.write((const char*) &this->m_Data, sizeof(m_Data));

	// write the statsistics
	this->CManager::SaveToFile(outfile);
	this->CWaiter::SaveToFile(outfile);
	this->CChef::SaveToFile(outfile);
}
/*----------------------------------------------------------------------------------------------*/
bool CEmployee::SetStatistic(int id, unsigned int statistic, int index)
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			return this->CChef::SetStats(id, statistic, index);
		case TITLE_WAITER:
			return this->CWaiter::SetStats(id, statistic, index);
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/
void CEmployee::ShiftStats()
{
	switch(this->GetTitle())
	{
		case TITLE_CHEF:
			this->CChef::ShiftStats();
			break;

		case TITLE_WAITER:
			this->CWaiter::ShiftStats();
			break;
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CChef implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
bool CChef::ClearStatistics() 
{
	// clear the statistic array
	for(unsigned short i = 0 ; i < 8 ; i++)
		stats[i].pizzas_cooked = 0;

	return true;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CChef::GetStats(int id, int index)
{
	switch(id)
	{
		case PIZZAS_COOKED:
			return stats[index].pizzas_cooked;
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CChef::SetStats(int id, unsigned int statistic, int index)
{
	switch(id)
	{
		case PIZZAS_COOKED:
			stats[index].pizzas_cooked = statistic;
			return true;
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/
void CChef::ShiftStats()
{
	// the first array item has the totals
	// so dont overwrite it or shift it
	stats[0].pizzas_cooked += stats[7].pizzas_cooked;

	// shift the array back by one leftward
	tagSTATS temp;
	for(unsigned short i = 7 ; i > 1 ; i--)
	{
		temp = stats[i-1];
		stats[i-1] = stats[i];
	}

	// clear the 7th array item
	stats[7].pizzas_cooked = 0;
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CWaiter implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
bool CWaiter::ClearStatistics()
{
	// clear the statistic array
	for(unsigned short i = 0 ; i < 8 ; i++)
	{
		stats[i].pizzas_ordered = 0;
		stats[i].pizzas_served = 0;
	}
	return true;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CWaiter::GetStats(int id, int index)
{
	switch(id)
	{
		case PIZZAS_ORDERED:
			return stats[index].pizzas_ordered;
		case PIZZAS_SERVED:
			return stats[index].pizzas_served;
	}
	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool CWaiter::SetStats(int id, unsigned int statistic, int index)
{
	switch(id)
	{
		case PIZZAS_ORDERED:
			stats[index].pizzas_ordered = statistic;
			return true;
		case PIZZAS_SERVED:
			stats[index].pizzas_served = statistic;
			return true;
	}
	return false;
}
/*----------------------------------------------------------------------------------------------*/
void CWaiter::ShiftStats()
{
	// the first array item has the totals
	// so dont overwrite it or shift it
	stats[0].pizzas_ordered += stats[7].pizzas_ordered;
	stats[0].pizzas_served += stats[7].pizzas_served;

	// shift the array back by one leftward
	tagSTATS temp;
	for(unsigned short i = 7 ; i > 1 ; i--)
	{
		temp = stats[i-1];
		stats[i-1] = stats[i];
	}

	// clear the 7th array item
	stats[7].pizzas_ordered = 0;
	stats[7].pizzas_served = 0;
}
/*----------------------------------------------------------------------------------------------*/



