#ifndef _JS_CAPP_H
#define _JS_CAPP_H
/*---------------------------------------------------------------------------*/
// For compilers that support precompilation, includes "wx.h".
#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
// Include your minimal set of headers here, or wx.h
#include <wx/wx.h>
#endif

#include "sim_main.h"
#include "logger.h"
/*---------------------------------------------------------------------------*/
class CApplication : public wxApp
{
public:
	virtual bool OnInit();

public:
	CLogFile m_LogFile;
	CSimulator m_Simulator;
};

/*---------------------------------------------------------------------------*/
// this we get us the wxGetApp function
DECLARE_APP(CApplication);
/*---------------------------------------------------------------------------*/
#endif


