#ifdef _ENT_EDITOR
#include <windows.h>
#endif

#include <fstream>	
#include <string>
#include <cstdio>
#include "data.h"

using namespace std;
/*----------------------------------------------------------------------------------------------*/
typedef struct tagPBD_SUBHEADER {
	unsigned int count;
}PBD_SUBHEADER;

typedef struct tagPBD_CAT_HEADER {
	char name[256];
	unsigned int count;
}PBD_CAT_HEADER;
/*----------------------------------------------------------------------------------------------*/
bool Entities_LoadFile(char* filename, PBD_DATA &data)
{
	unsigned int c = 0; // counter

	// open the file
	ifstream infile;
	infile.open(filename, ios::in|ios::binary);

	// error opening file
	if(infile.fail() != 0)
		return false;

	// read the header
	infile.read((char*)&data.Header, sizeof(FileHeader));
	// read the age
	if(data.dwFlags & DATA_AGE)
		infile.read((char*)&data.Age, sizeof(MinMax));
	else
		infile.seekg(sizeof(MinMax), ios::cur);
	
	// read the competency
	if(data.dwFlags & DATA_COMPETENCY)
		infile.read((char*)&data.Competency, sizeof(MinMax));
	else
		infile.seekg(sizeof(MinMax), ios::cur);

	// read the header for the firstname list
	if((data.dwFlags & DATA_FIRSTNAMES) == DATA_FIRSTNAMES)
	{
		PBD_SUBHEADER fname_header;
		infile.read((char*)&fname_header, sizeof(PBD_SUBHEADER));
		// read the firstname objects in list
		for(c = 1 ; c <= fname_header.count ; c++)
		{
			CharName *pItem = new CharName;
			infile.read((char*)pItem, sizeof(CharName));
			data.FirstnameList.AddItem(pItem);
		}
	}
	else
		infile.seekg(data.Header.offset_lastnames, ios::beg);

	// read the header for the lastname list
	if((data.dwFlags & DATA_LASTNAMES) == DATA_LASTNAMES)
	{
		PBD_SUBHEADER lname_header;
		infile.read((char*)&lname_header, sizeof(PBD_SUBHEADER));
		// read the lastname objects in list
		for(c = 1 ; c <= lname_header.count ; c++)
		{
			CharName *pItem = new CharName;
			infile.read((char*)pItem, sizeof(CharName));
			data.LastnameList.AddItem(pItem);
		}
	}
	else
		infile.seekg(data.Header.offset_desc, ios::beg);

	// read the header for the description list
	if((data.dwFlags & DATA_DESC) == DATA_DESC)
	{
		PBD_SUBHEADER desc_header;
		infile.read((char*)&desc_header, sizeof(PBD_SUBHEADER));
		// read the firstname objects in list
		for(c = 1 ; c <= desc_header.count ; c++)
		{
			CharDesc *pItem = new CharDesc;
			infile.read((char*)pItem, sizeof(CharDesc));
			data.DescList.AddItem(pItem);
		}
	}
	else
		infile.seekg(data.Header.offset_chair_price, ios::beg);

	// read the chair price
	if((data.dwFlags & DATA_CHAIRPRICE) == DATA_CHAIRPRICE)
		infile.read((char*)&data.Chair, sizeof(ChairPrice));
	else
		infile.seekg(data.Header.offset_ovens, ios::beg);

	// read the header for the oven list
	if((data.dwFlags & DATA_OVENS) == DATA_OVENS)
	{
		PBD_SUBHEADER oven_header;
		infile.read((char*)&oven_header, sizeof(PBD_SUBHEADER));
		// read the oven objects in list
		for(c = 1 ; c <= oven_header.count ; c++)
		{
			OvenData *pItem = new OvenData;
			infile.read((char*)pItem, sizeof(OvenData));
			data.OvenList.AddItem(pItem);
		}
	}
	else
		infile.seekg(data.Header.offset_tables, ios::beg);

	// read the header for the table list
	if((data.dwFlags & DATA_TABLES) == DATA_TABLES)
	{
		PBD_SUBHEADER table_header;
		infile.read((char*)&table_header, sizeof(PBD_SUBHEADER));
		// read the table objects in list
		for(c = 1 ; c <= table_header.count ; c++)
		{
			TableData *pItem = new TableData;
			infile.read((char*)pItem, sizeof(TableData));
			data.TableList.AddItem(pItem);
		}
	}
	else
		infile.seekg(data.Header.offset_ads, ios::beg);

	// read the header for the AdCategory list
	if((data.dwFlags & DATA_ADS) == DATA_ADS)
	{
		PBD_SUBHEADER ad_cat_header;
		infile.read((char*)&ad_cat_header, sizeof(PBD_SUBHEADER));
		// read each AdCategory object
		for(c = 1 ; c <= ad_cat_header.count ; c++)
		{
			AdCategory *pCat = new AdCategory;
			data.AdCatList.AddItem(pCat);
			// read the Cat Header
			PBD_CAT_HEADER cat_header;
			infile.read((char*)&cat_header, sizeof(PBD_CAT_HEADER));
			strcpy(pCat->name, cat_header.name);
			// read each AdData object
			for(unsigned int i = 1 ; i <= cat_header.count ; i++)
			{
				AdData *pData = new AdData;
				pCat->Ads.AddItem(pData);
				infile.read((char*)pData, sizeof(AdData));
			}
		}
	}
	else
		infile.seekg(data.Header.offset_ingredients, ios::beg);

	// read the header for the IngCategory list
	if((data.dwFlags & DATA_INGREDIENTS) == DATA_INGREDIENTS)
	{
		PBD_SUBHEADER ing_cat_header;
		infile.read((char*)&ing_cat_header, sizeof(PBD_SUBHEADER));
		// read each IngCategory object
		for(c = 1 ; c <= ing_cat_header.count ; c++)
		{
			IngCategory *pCat = new IngCategory;
			data.IngCatList.AddItem(pCat);
			// read the Cat Header
			PBD_CAT_HEADER cat_header;
			infile.read((char*)&cat_header, sizeof(PBD_CAT_HEADER));
			strcpy(pCat->name, cat_header.name);
			// read each IngData object
			for(unsigned int i = 1 ; i <= cat_header.count ; i++)
			{
				IngData *pData = new IngData;
				pCat->Ingredients.AddItem(pData);
				infile.read((char*)pData, sizeof(IngData));
			}
		}
	}
	else
		infile.seekg(data.Header.offset_recipes, ios::beg);	


	// read the header for the Recipe list
	if((data.dwFlags & DATA_RECIPES) == DATA_RECIPES)
	{
		PBD_SUBHEADER recipe_cat_header;
		infile.read((char*)&recipe_cat_header, sizeof(PBD_SUBHEADER));
		// read each IngCategory object
		for(c = 1 ; c <= recipe_cat_header.count ; c++)
		{
			RecipeData *pCat = new RecipeData;
			data.RecipeList.AddItem(pCat);
			// read the Cat Header
			PBD_CAT_HEADER cat_header;
			infile.read((char*)&cat_header, sizeof(PBD_CAT_HEADER));
			strcpy(pCat->name, cat_header.name);
			// read each IngData object
			for(unsigned int i = 1 ; i <= cat_header.count ; i++)
			{
				short *id = new short;
				pCat->Ingredients.AddItem(id);
				infile.read((char*)id, sizeof(short));
			}
		}
	}
	//else
	//	infile.seekg(data.Header.offset_recipes, ios::beg);

	// close the file
	infile.close();

	return true;
}
/*----------------------------------------------------------------------------------------------*/
#ifdef _ENT_EDITOR
#ifdef _WINDOWS
void Entities_SaveFile(char* filename, PBD_DATA &data)
{
	unsigned int c = 0 ; // counter

	// create the file
	HANDLE hFile = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 
		FILE_ATTRIBUTE_NORMAL, NULL);

	if(hFile == INVALID_HANDLE_VALUE)
	{
		char error[2048];
		sprintf(error, "Could not open file: \"%s\"", filename); 
		MessageBox(0, error, "Error", MB_OK);
		return;
	}

	LONG offset = 0;
	DWORD written;

	// write the header
	WriteFile(hFile, &data.Header, sizeof(FileHeader), &written, NULL);
	data.Header.offset_ages = (offset += written);

	// write the age
	WriteFile(hFile, &data.Age, sizeof(MinMax), &written, NULL);
	data.Header.offset_compentency = (offset += written);

	// write the compentency
	WriteFile(hFile, &data.Competency, sizeof(MinMax), &written, NULL);
	data.Header.offset_firstnames = (offset += written);

	// write the header for the firstname list
	PBD_SUBHEADER fname_header;
	fname_header.count = data.FirstnameList.GetCount();
	WriteFile(hFile, &fname_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_lastnames = (offset += written);
	// write the firstname objects in list
	for(c = 1 ; c <= data.FirstnameList.GetCount() ; c++)
	{
		CharName *pItem = data.FirstnameList.GetItem(c);
		WriteFile(hFile, pItem, sizeof(CharName), &written, NULL);
		data.Header.offset_lastnames = (offset += written);
	}

	// write the header for the lastname list
	PBD_SUBHEADER lname_header;
	lname_header.count = data.LastnameList.GetCount();
	WriteFile(hFile, &lname_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_desc = (offset += written);
	// write the firstname objects in list
	for(c = 1 ; c <= data.LastnameList.GetCount() ; c++)
	{
		CharName *pItem = data.LastnameList.GetItem(c);
		WriteFile(hFile, pItem, sizeof(CharName), &written, NULL);
		data.Header.offset_desc = (offset += written);
	}

	// write the header for the description list
	PBD_SUBHEADER desc_header;
	desc_header.count = data.DescList.GetCount();
	WriteFile(hFile, &desc_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_chair_price = (offset += written);
	// write the firstname objects in list
	for(c = 1 ; c <= data.DescList.GetCount() ; c++)
	{
		CharDesc *pItem = data.DescList.GetItem(c);
		WriteFile(hFile, pItem, sizeof(CharDesc), &written, NULL);
		data.Header.offset_chair_price = (offset += written);
	}

	// write the chair price
	WriteFile(hFile, &data.Chair, sizeof(ChairPrice), &written, NULL);
	data.Header.offset_ovens = (offset += written);

	// write the header for the oven list
	PBD_SUBHEADER oven_header;
	oven_header.count = data.OvenList.GetCount();
	WriteFile(hFile, &oven_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_tables = (offset += written);
	// write the oven objects in list
	for(c = 1 ; c <= data.OvenList.GetCount() ; c++)
	{
		OvenData *pItem = data.OvenList.GetItem(c);
		WriteFile(hFile, pItem, sizeof(OvenData), &written, NULL);
		data.Header.offset_tables = (offset += written);
	}

	// write the header for the table list
	PBD_SUBHEADER table_header;
	table_header.count = data.TableList.GetCount();
	WriteFile(hFile, &table_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_ads = (offset += written);
	// write the table objects in list
	for(c = 1 ; c <= data.TableList.GetCount() ; c++)
	{
		TableData *pItem = data.TableList.GetItem(c);
		WriteFile(hFile, pItem, sizeof(TableData), &written, NULL);
		data.Header.offset_ads = (offset += written);
	}

	// write the header for the AdCategory list
	PBD_SUBHEADER ad_cat_header;
	ad_cat_header.count = data.AdCatList.GetCount();
	WriteFile(hFile, &ad_cat_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_ingredients = (offset += written);
	// write each category
	for(c = 1 ; c <= data.AdCatList.GetCount() ; c++)
	{
		AdCategory *pCat = data.AdCatList.GetItem(c);
		// write the category header
		PBD_CAT_HEADER cat_header;
		strcpy(cat_header.name, pCat->name);
		cat_header.count = pCat->Ads.GetCount();
		WriteFile(hFile, &cat_header, sizeof(PBD_CAT_HEADER), &written, NULL);
		data.Header.offset_ingredients = (offset += written);
		// write each advertisement object
		for(unsigned int i = 1 ; i <= pCat->Ads.GetCount() ; i++)
		{
			AdData *pData = pCat->Ads.GetItem(i);
			WriteFile(hFile, pData, sizeof(AdData), &written, NULL);
			data.Header.offset_ingredients = (offset += written);
		}
	}

	// write the header for the IngCategory list
	PBD_SUBHEADER ing_cat_header;
	ing_cat_header.count = data.IngCatList.GetCount();
	WriteFile(hFile, &ing_cat_header, sizeof(PBD_SUBHEADER), &written, NULL);
	data.Header.offset_recipes = (offset += written);
	// write each category
	short ingredient_id = 0;
	for(c = 1 ; c <= data.IngCatList.GetCount() ; c++)
	{
		IngCategory *pCat = data.IngCatList.GetItem(c);
		// write the category header
		PBD_CAT_HEADER cat_header;
		strcpy(cat_header.name, pCat->name);
		cat_header.count = pCat->Ingredients.GetCount();
		WriteFile(hFile, &cat_header, sizeof(PBD_CAT_HEADER), &written, NULL);
		data.Header.offset_recipes = (offset += written);
		// write each ingredient object
		for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
		{
			IngData *pData = pCat->Ingredients.GetItem(i);
			pData->ingredient_id = ingredient_id;
			ingredient_id++;
			WriteFile(hFile, pData, sizeof(IngData), &written, NULL);
			data.Header.offset_recipes = (offset += written);
		}
	}

	// write the header for the RecipeData
	PBD_SUBHEADER recipe_cat_header;
	recipe_cat_header.count = data.RecipeList.GetCount();
	WriteFile(hFile, &recipe_cat_header, sizeof(PBD_SUBHEADER), &written, NULL);
	// write each category
	for(c = 1 ; c <= data.RecipeList.GetCount() ; c++)
	{
		RecipeData *pCat = data.RecipeList.GetItem(c);
		// write the category header
		PBD_CAT_HEADER cat_header;
		strcpy(cat_header.name, pCat->name);
		cat_header.count = pCat->Ingredients.GetCount();
		WriteFile(hFile, &cat_header, sizeof(PBD_CAT_HEADER), &written, NULL);
		// write each recipe ingredient ids
		for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
		{
			short *id = pCat->Ingredients.GetItem(i);
			WriteFile(hFile, (char*)id, sizeof(short), &written, NULL);
		}
	}

	// move the cursor to the beginning of the file
	// and write the header again
	SetFilePointer(hFile, 0, 0, FILE_BEGIN);
	WriteFile(hFile, &data.Header, sizeof(FileHeader), &written, NULL);
	
	// close the file
	CloseHandle(hFile);
}
#endif
#endif
/*----------------------------------------------------------------------------------------------*/
bool AdIdAvailable(List<AdCategory> *pCatList, short id)
{
	for(unsigned int c = 1 ; c <= pCatList->GetCount() ; c++)
	{
		AdCategory *pCat = pCatList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCat->Ads.GetCount() ; i++)
		{
			AdData *pData = pCat->Ads.GetItem(i);
			
			if(pData->ad_id == id)
				return false;
		}
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
short GetNextAvailableAdID(List<AdCategory> *pCatList)
{
	if(pCatList == 0) return 0;

	for(unsigned short c = 0 ; c < 256 ; c++)
	{
		if(AdIdAvailable(pCatList, c) == true)
			return c;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
IngData* GetIngredientByID(List<IngCategory> *pCatList, short id)
{
	if(pCatList == 0) return 0;

	for(unsigned int c = 1 ; c <= pCatList->GetCount() ; c++)
	{
		IngCategory *pCat = pCatList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
		{
			IngData *pData = pCat->Ingredients.GetItem(i);
			
			if(pData->ingredient_id == id)
				return pData;
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool IngIdAvailable(List<IngCategory> *pCatList, short id)
{
	for(unsigned int c = 1 ; c <= pCatList->GetCount() ; c++)
	{
		IngCategory *pCat = pCatList->GetItem(c);
		for(unsigned int i = 1 ; i <= pCat->Ingredients.GetCount() ; i++)
		{
			IngData *pData = pCat->Ingredients.GetItem(i);
			
			if(pData->ingredient_id == id)
				return false;
		}
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
short GetNextAvailableIngID(List<IngCategory> *pCatList)
{
	if(pCatList == 0) return 0;

	for(unsigned short c = 0 ; c < 256 ; c++)
	{
		if(IngIdAvailable(pCatList, c) == true)
			return c;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
short GetNextAvailableTableID(List<TableData> *pTableList)
{
	if(pTableList == 0) return 0;

	for(unsigned short c = 0 ; c < 256 ; c++)
	{
		if(TableIdAvailable(pTableList, c) == true)
			return c;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool TableIdAvailable(List<TableData> *pTableList, short id)
{
	for(unsigned int c = 1 ; c <= pTableList->GetCount() ; c++)
	{
		TableData *pData = pTableList->GetItem(c);
			
		if(pData->table_id == id)
			return false;
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
short GetNextAvailableOvenID(List<OvenData> *pOvenList)
{
	if(pOvenList == 0) return 0;

	for(unsigned short c = 0 ; c < 256 ; c++)
	{
		if(OvenIdAvailable(pOvenList, c) == true)
			return c;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
bool OvenIdAvailable(List<OvenData> *pOvenList, short id)
{
	for(unsigned int c = 1 ; c <= pOvenList->GetCount() ; c++)
	{
		OvenData *pData = pOvenList->GetItem(c);
			
		if(pData->oven_id == id)
			return false;
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/


