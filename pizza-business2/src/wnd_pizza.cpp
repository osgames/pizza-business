#include "wnd_pizza.h"
#include "pbutils.h"
#include "pbengine.h"
/*----------------------------------------------------------------------------------------------*/
// CPizzasDlg event table
BEGIN_EVENT_TABLE(CPizzasDlg, wxDialog)
	EVT_PAINT(CPizzasDlg::OnPaint)
	EVT_BUTTON(PD_BUTTON_EDIT, CPizzasDlg::OnEditClicked)
	EVT_BUTTON(PD_BUTTON_OK, CPizzasDlg::OnOkClicked)
	EVT_BUTTON(PD_BUTTON_DELETE, CPizzasDlg::OnDeleteClicked)
	EVT_BUTTON(PD_BUTTON_BUYING, CPizzasDlg::OnBuyIngsClicked)
	EVT_BUTTON(PD_BUTTON_NEW, CPizzasDlg::OnNewPizzaClicked)
	EVT_CHECKBOX(PD_CHECKBOX, CPizzasDlg::OnAutoPriceChecked)
	EVT_SPINCTRL(PD_SPINCTRL, CPizzasDlg::OnSpinControl)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
// CPizzaEditDlg event table
BEGIN_EVENT_TABLE(CPizzaEditDlg, wxDialog)
	EVT_PAINT(CPizzaEditDlg::OnPaint)
	EVT_BUTTON(PED_BUTTON_OK, CPizzaEditDlg::OnOkClicked)
	EVT_BUTTON(PED_BUTTON_CANCEL, CPizzaEditDlg::OnCancelClicked)
	EVT_BUTTON(PED_BUTTON_ADDING, CPizzaEditDlg::OnAddIngClicked)
	EVT_BUTTON(PED_BUTTON_REMING, CPizzaEditDlg::OnRemoveIngClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
// CPizzaIngDlg event table
BEGIN_EVENT_TABLE(CPizzaIngDlg, wxDialog)
	EVT_PAINT(CPizzaIngDlg::OnPaint)
	EVT_BUTTON(PID_BUTTON_OK, CPizzaIngDlg::OnOkClicked)
	EVT_BUTTON(PID_BUTTON_CANCEL, CPizzaIngDlg::OnCancelClicked)
END_EVENT_TABLE()
/*----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------+
|																		                         |						 
|	CPizzasDlg implementation											                         |						 
|																		                         |						 
+-----------------------------------------------------------------------------------------------*/

CPizzasDlg::CPizzasDlg(wxWindow* parent, CRestaurant* pRestaurant):
			wxDialog(parent, -1, _("Restaurant's Pizzas"), wxPoint(200, 150), 
				wxSize(450, 410), wxDEFAULT_DIALOG_STYLE, "dialogBox")
{
	this->m_pRestaurant = pRestaurant;

	this->Centre();

	//this->m_pAutoPricingCB = new wxCheckBox(this, PD_CHECKBOX, "", wxDefaultPosition, wxSize(15, 15));
	//this->m_pAutoPricingSpin = new wxSpinCtrl(this, PD_SPINCTRL, "5", wxDefaultPosition, wxSize(60, 25));
	this->m_pPizzasListBox = new wxListBox(this, PD_LISTBOX, wxDefaultPosition, wxSize(425, 250));
	this->m_pNewButton  = new wxButton(this, PD_BUTTON_NEW, _("Add Pizza..."), wxDefaultPosition);
	this->m_pEditButton = new wxButton(this, PD_BUTTON_EDIT, _("Edit Pizza..."), wxDefaultPosition);
	this->m_pDeleteButton = new wxButton(this, PD_BUTTON_DELETE, _("Remove Pizza"), wxDefaultPosition);
	this->m_pStaticLine = new wxStaticLine(this, -1, wxPoint(10, 340), wxSize(430, 1), wxLI_HORIZONTAL);
	this->m_pOkButton = new wxButton(this, PD_BUTTON_OK, _("Ok"), wxDefaultPosition);
	this->m_pBuyIngsButton = new wxButton(this, PD_BUTTON_BUYING, _("Buy All Ingredients For Pizza "));

	// set the auto-pricing editbox
	/*this->m_pAutoPricingSpin->SetValue(this->m_pRestaurant->GetAutoPricePercentage());
    this->m_pAutoPricingSpin->SetRange(1, 20000);*/
	// enable the auto-princing checkbox?
	/*if(this->m_pRestaurant->IsAutoPricingEnabled() == true)
	{
		this->m_pAutoPricingCB->SetValue(true);
		this->m_pAutoPricingSpin->Enable();
	}
	else
	{
		this->m_pAutoPricingCB->SetValue(false);
		this->m_pAutoPricingSpin->Disable();
	}*/

	/*this->enableAuto = new wxStaticText(this, -1, _("Enable auto-pricing of pizzas with a"), wxPoint(35, 8));
	this->percentIncrease = new wxStaticText(this, -1, _("% increase"), wxPoint(295, 8));*/

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	//this->topSizer = new wxBoxSizer(wxHORIZONTAL);
	//this->topSizer->Add(this->m_pAutoPricingCB, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10);
	//this->topSizer->Add(this->enableAuto, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10);
	//this->topSizer->Add(this->m_pAutoPricingSpin, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10);
	//this->topSizer->Add(this->percentIncrease, 0, wxALIGN_CENTER_VERTICAL);

	//this->mainSizer->Add(this->topSizer, 0, wxALL, 10);

	this->mainSizer->Add(this->m_pPizzasListBox, 1, wxEXPAND|wxALL, 10);

	this->midButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
    this->midButtonsSizer->Add(this->m_pBuyIngsButton, 0, wxRIGHT|wxALIGN_LEFT, 10);
    this->midButtonsSizer->Add(1, 10, 1);
	this->midButtonsSizer->Add(this->m_pNewButton, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->midButtonsSizer->Add(this->m_pEditButton, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->midButtonsSizer->Add(this->m_pDeleteButton, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(midButtonsSizer, 0, wxRIGHT|wxLEFT|wxBOTTOM|wxEXPAND, 10);

	this->mainSizer->Add(this->m_pStaticLine, 0, wxEXPAND|wxRIGHT|wxLEFT|wxBOTTOM, 10);
	this->mainSizer->Add(this->m_pOkButton, 0, wxALIGN_CENTER_HORIZONTAL);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);

	mainSizer->Fit(this);
	mainSizer->SetSizeHints(this);

	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
CPizzasDlg::~CPizzasDlg()
{
	//this->m_pAutoPricingCB->Destroy();
	//this->m_pAutoPricingSpin->Destroy();
	this->m_pPizzasListBox->Destroy();
	this->m_pNewButton->Destroy();
	this->m_pEditButton->Destroy();
	this->m_pDeleteButton->Destroy();
	this->m_pOkButton->Destroy();
	this->m_pBuyIngsButton->Destroy();
	this->m_pStaticLine->Destroy();
	//this->enableAuto->Destroy();
	//this->percentIncrease->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
CPizzaObject* CPizzasDlg::GetSelectedPizza()
{
	int iSel = this->m_pPizzasListBox->GetSelection();
	if (iSel >= 0 && iSel <= this->m_pPizzasListBox->GetCount())
	{
		CPizzaObject *pPizzaObject = (CPizzaObject*)this->m_pPizzasListBox->GetClientData(iSel);
		return pPizzaObject;
	}
	else
		return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnBuyIngsClicked(wxMouseEvent &e)
{
	if(this->m_pRestaurant == NULL)
		return;

	// get the restaurant object from the listbox
	CPizzaObject *pPizzaObject = this->GetSelectedPizza();
	if(pPizzaObject)
	{
		// purchase all the ingredients for the pizza
		// if the restaurant has the money
		CPizza *pPizza = pPizzaObject->GetPizza();
		float pizzaCost = pPizza->GetProductionCost();
		if(this->m_pRestaurant->GetBudget() >= pizzaCost)
		{
			char name[256];
			pPizza->GetName(name);
			// contruct the prompt text to include
			// pizza's name, production cost, and list of ingredients
			wxString prompt;
			prompt.Printf("Are you sure you want to purchase the following ingredients for $%.2f:\n", pizzaCost);
			unsigned int count = pPizza->GetIngredientCount();
			for(unsigned int c = 1 ; c <= count ; c++)
			{
				char ingName[256];
				CIngredient *pIng = pPizza->GetIngredient(c);
				pIng->GetName(ingName);
				
				prompt = prompt + "\n\t" + ingName;
			}

			// prompt the player for comfirmation
			wxMessageDialog dialog(this, prompt, _(name), wxYES_NO);
			if(dialog.ShowModal() == wxID_YES)
			{
				count = pPizza->GetIngredientCount();
				for(unsigned int i = 1 ; i <= count ; i++)
				{
					this->m_pRestaurant->Ingredient_Buy(pPizza->GetIngredient(i), 1);
				}
			}
		}
	}
	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnPaint(wxPaintEvent &e)
{
	wxPaintDC paint(this);
	e.Skip();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnEditClicked(wxMouseEvent &e)
{
	// get the restaurant object from the listbox
	CPizzaObject *pPizza = this->GetSelectedPizza();
	if(pPizza)
	{
		CPizzaEditDlg dlg(this, this->m_pRestaurant, pPizza);
		dlg.ShowModal();
	}
	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnDeleteClicked(wxMouseEvent &e)
{
	CPizzaObject *pPizza = GetSelectedPizza();
	if(pPizza)
	{
		char name[256];
		pPizza->GetPizza()->GetName(name);

		wxString prompt;
		prompt = "Are you sure you want to delete the following pizzas:\n\n";
		prompt += _(name);
		if(wxYES == wxMessageBox(prompt, _("Confimation"), wxYES_NO))
			this->m_pRestaurant->RemovePizzaFromList(pPizza->GetPizza());

		this->RefreshPizzaListBox();
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnOkClicked(wxMouseEvent &e)
{
	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnAutoPriceChecked(wxCommandEvent &e)
{
	if (this->m_pAutoPricingCB->GetValue() == TRUE)
	{
		this->m_pRestaurant->EnableAutoPricing(true);
		this->m_pAutoPricingSpin->Enable();
	}
	else
	{
		this->m_pRestaurant->EnableAutoPricing(false);
		this->m_pAutoPricingSpin->Disable();
	}
	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnNewPizzaClicked(wxCommandEvent &e)
{
	// create a new CPizzaObject object
	CPizzaObject Pizza;

	CPizzaEditDlg dlg(this, this->m_pRestaurant, &Pizza);
	if(dlg.ShowModal() == wxOK)
		this->m_pRestaurant->AddPizzaToList(Pizza.GetPizza());
	
	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::OnSpinControl(wxSpinEvent &e)
{
	this->m_pRestaurant->SetAutoPricePercentage(this->m_pAutoPricingSpin->GetValue());
	this->RefreshPizzaListBox();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzasDlg::RefreshPizzaListBox()
{
	this->m_pPizzasListBox->Clear();
	
	for(unsigned int c = 1 ; c <= this->m_pRestaurant->m_PizzaList.GetCount() ; c++)
	{
		CPizzaObject *pPizzaObject = this->m_pRestaurant->m_PizzaList.GetItem(c);
		// get the price of the pizza
		float pizza_price = pPizzaObject->GetPrice();
		// get the name of the pizza
		char pizza_name[256];
		CPizza *pPizza = pPizzaObject->GetPizza();
		pPizza->GetName(pizza_name);
		// adjust the price if auto-pricing is enabled
		if(this->m_pRestaurant->IsAutoPricingEnabled() == true)
		{
			float production_cost = pPizza->GetProductionCost();
			pizza_price = production_cost + (production_cost * (this->m_pRestaurant->GetAutoPricePercentage()*.01));
		}
		// add the item to the listbox
		wxString text;
		text << _(pizza_name) << " - $" << ToString(pizza_price, 2).c_str();

		if(this->m_pRestaurant->IsPizzaSupported(pPizzaObject) == false)
			text = text + " - * Not enough ingredients *";

		this->m_pPizzasListBox->Append(text, pPizzaObject);
	}
}
/*-----------------------------------------------------------------------------------------------+
|																		                         |						 
|	CPizzaEditDlg implementation											                     |						 
|																		                         |						 
+-----------------------------------------------------------------------------------------------*/
CPizzaEditDlg::CPizzaEditDlg(wxWindow* parent, CRestaurant* pRestaurant, CPizzaObject* pPizza):
				wxDialog(parent, -1, "", wxPoint(200, 150), 
				wxSize(400, 365), wxDEFAULT_DIALOG_STYLE, "dialogBox")
{
	this->m_pRestaurant = pRestaurant;
	this->m_pPizza = pPizza;

	this->Centre();

	// set the caption to the name of the pizza
	char pizza_name[256];
	this->m_pPizza->GetPizza()->GetName(pizza_name);
	this->SetTitle(_(pizza_name));

	this->pizzaName = new wxStaticText(this, -1, _("Pizza Name:"), wxDefaultPosition);

	this->m_pPizzaName = new wxTextCtrl(this, PED_PIZZANAME, _(pizza_name), wxDefaultPosition/*wxPoint(70, 0)*/, wxSize(300, 25));
	if(strlen(pizza_name) < 1)
		this->SetTitle(_("New Pizza"));

	this->m_pPriceEdit = new wxTextCtrl(this, PED_EDITBOX, "", wxDefaultPosition, wxSize(70, 25));

	this->m_pBox = new wxStaticBox(this, -1, _("Pricing"), wxDefaultPosition, wxSize(380, 105));

	this->m_pLine = new wxStaticLine(this, -1, wxPoint(10, 304), wxSize(380, 1), wxLI_HORIZONTAL);

	this->m_pOkButton = new wxButton(this, PED_BUTTON_OK, _("Ok"), wxDefaultPosition);

	this->m_pCancelButton = new wxButton(this, PED_BUTTON_CANCEL, _("Cancel"), wxDefaultPosition);

	this->m_ingListBox = new wxListBox(this, -1, wxDefaultPosition, wxSize(380, 150));

	// create the add ingredient button
	this->m_pAddIngButton = new wxButton(this, PED_BUTTON_ADDING, _("Add Ingredient"), wxDefaultPosition);
	this->m_pRemoveIngButton = new wxButton(this, PED_BUTTON_REMING, _("Remove Ingredient"), wxDefaultPosition);

	// set the price of the pizza in the editbox
	float pizza_price = this->m_pPizza->GetPrice();
	
	wxString price;
	price = ToString(pizza_price, 2).c_str();
	
	this->m_pPriceEdit->SetValue(price);
	
	if (this->m_pRestaurant->IsAutoPricingEnabled())
		this->m_pPriceEdit->Disable();

	this->pricePizza = new wxStaticText(this, -1, _("Price of pizza: ($):"), wxPoint(20, 33));
	
	this->autoPriceEnabled = new wxStaticText(this, -1, "", wxPoint(240, 33));
	if (this->m_pRestaurant->IsAutoPricingEnabled())
	{
		this->autoPriceEnabled->SetLabel(_("Auto-price enabled"));
	}

	this->RefreshIngListbox();

	wxString temp;
	float production_cost = this->m_pPizza->GetPizza()->GetProductionCost();

	temp << _("Production cost:") << " " << ToString(production_cost, 2).c_str();

	this->productionPrice = new wxStaticText(this, -1, temp, wxPoint(20, 77));	

	this->mainSizer = new wxBoxSizer(wxVERTICAL);
	this->boxSizer = new wxStaticBoxSizer(this->m_pBox, wxHORIZONTAL);
	this->mainInsideBoxSizer = new wxBoxSizer(wxVERTICAL);
	this->topInsideBoxSizer = new wxBoxSizer(wxHORIZONTAL);
    this->pizzaNameSizer = new wxBoxSizer(wxHORIZONTAL);

    this->pizzaNameSizer->Add(this->pizzaName, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 10); 
    this->pizzaNameSizer->Add(this->m_pPizzaName, 1);

    this->mainSizer->Add(this->pizzaNameSizer, 0, wxRIGHT|wxLEFT|wxTOP|wxEXPAND, 10);
	
	this->topInsideBoxSizer->Add(this->pricePizza, 0, wxRIGHT, 10);
	this->topInsideBoxSizer->Add(this->m_pPriceEdit, 0, wxRIGHT, 30);
	this->topInsideBoxSizer->Add(this->autoPriceEnabled);
	
	this->mainInsideBoxSizer->Add(this->topInsideBoxSizer, 0, wxEXPAND|wxBOTTOM, 20);
	this->mainInsideBoxSizer->Add(this->productionPrice);

	this->boxSizer->Add(this->mainInsideBoxSizer, 1, wxEXPAND|wxALL, 10);

	this->mainSizer->Add(this->boxSizer, 0, wxEXPAND|wxRIGHT|wxLEFT|wxBOTTOM, 10);

    this->mainSizer->Add(this->m_ingListBox, 1, wxEXPAND|wxRIGHT|wxLEFT, 10);

	this->mainSizer->Add(this->m_pLine, 0, wxEXPAND|wxBOTTOM|wxLEFT|wxRIGHT, 10);
	
	this->bottomBtnSizer = new wxBoxSizer(wxHORIZONTAL);
    this->bottomBtnSizer->Add(this->m_pAddIngButton, 0, wxALIGN_RIGHT|wxRIGHT, 10);
    this->bottomBtnSizer->Add(this->m_pRemoveIngButton, 0, wxALIGN_RIGHT|wxRIGHT, 25);
	this->bottomBtnSizer->Add(this->m_pOkButton, 0, wxRIGHT|wxALIGN_RIGHT, 10);
	this->bottomBtnSizer->Add(this->m_pCancelButton, 0, wxALIGN_RIGHT);

	this->mainSizer->Add(this->bottomBtnSizer, 0, wxALIGN_RIGHT|wxRIGHT|wxLEFT|wxBOTTOM, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);
	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);
}
/*----------------------------------------------------------------------------------------------*/
CPizzaEditDlg::~CPizzaEditDlg()
{
	this->m_pPizzaName->Destroy();
	this->m_pPriceEdit->Destroy();
	this->m_pBox->Destroy();
	this->m_pLine->Destroy();
	this->m_pOkButton->Destroy();
	this->m_pCancelButton->Destroy();
	this->autoPriceEnabled->Destroy();
	this->pricePizza->Destroy();
	this->productionPrice->Destroy();
	this->m_ingListBox->Destroy();
	this->m_pAddIngButton->Destroy();
	this->m_pRemoveIngButton->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnAddIngClicked(wxMouseEvent &e)
{
	//wxMessageBox("Add Ingredient", "Clicked");
	CPizzaIngDlg dialog(this, this->m_pRestaurant, this->m_pPizza);
	dialog.ShowModal();

	this->RefreshIngListbox();
	// update the production cost
	wxString temp;
	float production_cost = this->m_pPizza->GetPizza()->GetProductionCost();
	temp << _("Production cost:") << " " << ToString(production_cost, 2).c_str();
	this->productionPrice->SetLabel(temp);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnOkClicked(wxMouseEvent &e)
{
	if(this->m_pPizzaName->GetLineLength(0) < 1)
	{
		wxMessageBox(_("Be sure to enter a name for the pizza."), _("Oops!"));
	}
	else
	{
		// set the new price of the pizza
		wxString text;
		text = this->m_pPriceEdit->GetValue();
		this->m_pPizza->SetPrice(StringTo<double>(text.c_str()));

		// set the new name of the pizza
		text = this->m_pPizzaName->GetValue();
		this->m_pPizza->GetPizza()->SetName((char*)text.c_str());

		this->EndModal(wxOK);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnPaint(wxPaintEvent &e)
{
	wxPaintDC paint(this);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::OnRemoveIngClicked(wxMouseEvent &e)
{
	int iSel = this->m_ingListBox->GetSelection();
	if(iSel >= 0)
	{
		CIngredient *pIng = (CIngredient*)this->m_ingListBox->GetClientData(iSel);
		if(pIng)
		{
			// remove the ingredint from the pizza
			this->m_pPizza->GetPizza()->RemoveIngredient(pIng);

			// update the list of ingredients in the listbox
			this->RefreshIngListbox();

			// update the production cost
			wxString temp;
			float production_cost = this->m_pPizza->GetPizza()->GetProductionCost();
			temp << _("Production cost:") << " " << ToString(production_cost, 2).c_str();
			this->productionPrice->SetLabel(temp);
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaEditDlg::RefreshIngListbox()
{
	if(m_ingListBox == NULL || m_pPizza == NULL || m_pRestaurant == NULL)
		return;

	this->m_ingListBox->Clear();

	// fill the list box with the required pizza ingredients
	unsigned int count = this->m_pPizza->GetPizza()->GetIngredientCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngredient* pIng = this->m_pPizza->GetPizza()->GetIngredient(c);

		char name[256];
		pIng->GetName(name);

		wxString ingredient(name);
		if(m_pRestaurant->GetIngTypeCount(pIng->GetId()) < 1)
			// NOTE: temporary way to show a shortage
			ingredient = ingredient + "*********";
		this->m_ingListBox->Append(ingredient, (void*) pIng);
	}
}
/*-----------------------------------------------------------------------------------------------+
|																		                         |						 
|	CPizzaIngDlg implementation																	 |						 
|																		                         |						 
+-----------------------------------------------------------------------------------------------*/
CPizzaIngDlg::CPizzaIngDlg(wxWindow* parent, CRestaurant *pRestaurant, CPizzaObject* pPizza) :
wxDialog(parent, -1, _("Add Ingredients"), wxPoint(200, 150), wxSize(300, 300), wxDEFAULT_DIALOG_STYLE, "dialogBox")
{
	this->m_pPizza = pPizza;
	this->m_pRestaurant = pRestaurant;
	this->m_ingListBox = new wxListBox(this, PID_LISTBOX, wxDefaultPosition, wxSize(300, 225),
		0, 0, wxLB_EXTENDED);
	this->PopulateListBox();

	this->m_pOkButton = new wxButton(this, PID_BUTTON_OK, _("Add"), wxPoint(30, 235));
	this->m_pCancelButton = new wxButton(this, PID_BUTTON_CANCEL, _("Cancel"), wxPoint(140, 235));

    this->mainSizer = new wxBoxSizer(wxVERTICAL);
    this->mainSizer->Add(this->m_ingListBox, 0, wxEXPAND);
    
    this->btnSizer = new wxBoxSizer(wxHORIZONTAL);
    this->btnSizer->Add(this->m_pOkButton, 0, wxRIGHT|wxALIGN_RIGHT, 10);
    this->btnSizer->Add(this->m_pCancelButton, 0, wxALIGN_RIGHT);

    this->mainSizer->Add(this->btnSizer, 0, wxALIGN_RIGHT|wxALL, 10);

	this->SetAutoLayout(true);
	this->SetSizer(this->mainSizer);

	this->mainSizer->Fit(this);
	this->mainSizer->SetSizeHints(this);

	this->Center();
}
/*----------------------------------------------------------------------------------------------*/
CPizzaIngDlg::~CPizzaIngDlg()
{
	this->m_ingListBox->Destroy();
	this->m_pOkButton->Destroy();
	this->m_pCancelButton->Destroy();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaIngDlg::OnOkClicked(wxMouseEvent &e)
{
	// add the selected ingredients to the pizza
	wxArrayInt selection;
	int count = this->m_ingListBox->GetSelections(selection);
	for(int c = 0 ; c < count ; c++)
	{
		int index = selection.Item(c);

		CIngredient *pIngredient = (CIngredient*)this->m_ingListBox->GetClientData(index);
		this->m_pPizza->GetPizza()->AddIngredient(pIngredient);
	}

	this->EndModal(wxOK);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaIngDlg::OnCancelClicked(wxMouseEvent &e)
{
	this->EndModal(wxCANCEL);
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaIngDlg::OnPaint(wxPaintEvent &e)
{
	wxPaintDC paint(this);
	e.Skip();
}
/*----------------------------------------------------------------------------------------------*/
void CPizzaIngDlg::PopulateListBox()
{
	if(this->m_pPizza == NULL) return;

	CDataPool *pDataPool = &this->m_pRestaurant->GetOwner()->m_pGameEngine->m_DataPool;
	pDataPool->m_IngList.GetCount();

	unsigned int count = pDataPool->m_IngList.GetCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CIngCategory *pIngCategory = pDataPool->m_IngList.GetItem(c);
		unsigned int iCount = pIngCategory->IngList.GetCount();
		for(unsigned int i = 1 ; i <= iCount ; i++)
		{
			CIngredient *pIngredient = pIngCategory->IngList.GetItem(i);

			// add the ingredient to the listbox only if it is not
			// already include in the pizza
			if(!this->m_pPizza->GetPizza()->IsIngredientIncluded(pIngredient))
			{
				char name[256];
				pIngredient->GetName(name);

				this->m_ingListBox->Append(name, (void*)pIngredient);
			}
		}	
	}
}
/*----------------------------------------------------------------------------------------------*/



