#ifndef _JS_WND_BANK_H
#define _JS_WND_BANK_H
/*----------------------------------------------------------------------------------------------*/
#include "datarest.h"
#include <wx/wx.h>
#include <wx/stattext.h>
#include <wx/listbox.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/statline.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/intl.h>
/*----------------------------------------------------------------------------------------------*/
/// The main banking dialog
class CBankDlg : public wxDialog
{
public:
	/// The bank dialog constructor
	/// \param parent A pointer to the parent window of this dialog
	/// \param pRestaurant A pointer to the restaurant object from which to get necessary data
	CBankDlg(wxWindow* parent, CRestaurant *pRestaurant);
	~CBankDlg();

protected:
	// Event handling functions
	/// Event handling function for when the OK button is clicked
	void OnOkClicked(wxMouseEvent &e);
	/// Event handling function for when the Pay Off Loans button is clicked
	void OnPayOffLoansClicked(wxMouseEvent &e);
	/// Event handling function for when the Apply For Loans button is clicked
	void OnApplyForLoansClicked(wxMouseEvent &e);

private:
	/// Populates the loan list box
	void RefreshLoanBox();

//private: // internal processing
	//void InitBankServices(CTelnet &telnet);
	// called from InitBankServices
	//void InitLoanProcess(CTelnet &telnet);
	//void InitPayLoanProcess(CTelnet &telnet);
	//void PrintWelcomeText(CTelnet &telnet, bool bConnect);

private:
	wxStaticText *outstandingLoansText;
	wxListBox *loans;
	wxStaticText *totalDebt;
	wxButton *applyForLoan;
	wxButton *payOffLoans;
	wxStaticLine *line;
	wxButton *ok;
	wxBoxSizer *mainSizer;
	wxBoxSizer *middleBtnSizer;
	CRestaurant *m_pRestaurant;


	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	BD_BUTTON_APPLYFORLOAN = 23,
	BD_BUTTON_PAYOFFLOANS,
	BD_BUTTON_OK
};
/*----------------------------------------------------------------------------------------------*/
/// The apply for loan dialog
class CApplyLoanDlg : public wxDialog
{
public:
	CApplyLoanDlg(wxWindow* parent, CRestaurant* pRestaurant);
	~CApplyLoanDlg();
	/// Gets the amount specified in the dialog's edit box
	/// \returns A double that is the amount of the loan specified
	double GetAmount();

protected:
	void OnApplyClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);

private:
	wxStaticText *numLoans;
	wxStaticText *enterAmountText;
	wxTextCtrl* amountOfLoan;
	wxStaticLine* line;
	wxButton* apply;
	wxButton* cancel;
	double amount;
	CRestaurant* m_pRestaurant;
	wxBoxSizer *mainSizer, *middleSizer, *bottomBtnSizer;

	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	ALD_BUTTON_APPLY = 56,
	ALD_BUTTON_CANCEL
};
/*----------------------------------------------------------------------------------------------*/
class CPayLoanDlg : public wxDialog
{
public:
	CPayLoanDlg(wxWindow* parent, CRestaurant* pRestaurant);
	~CPayLoanDlg();
	/// Gets the amount specified in the dialog's edit box
	/// \returns A double that is the amount of the loan specified
	double GetAmount();

protected:
	void OnPayClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);

private:
	wxStaticText *numLoans;
	wxStaticText *enterAmountText;
	wxTextCtrl* amountToPay;
	wxStaticLine* line;
	wxButton* pay;
	wxButton* cancel;
	double amount;
	CRestaurant* m_pRestaurant;
	wxBoxSizer *mainSizer, *middleSizer, *bottomBtnSizer; 

	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum {
	PLD_BUTTON_PAY = 88,
	PLD_BUTTON_CANCEL
};
/*----------------------------------------------------------------------------------------------*/
#endif

