#!/bin/sh

for file in `ls *.cpp *.h`; 
do echo -e "\n" >>$file;
done;

export VARS=`ls *.cpp`
g++ $VARS `wx-config --static --cxxflags --libs` -o pbgame
echo "Compiling pbgame ..."

if [ -r pbgame ]; then
	echo "Compilation successful!"
fi
