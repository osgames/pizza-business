#ifndef _JS_STAFF_H
#define _JS_STAFF_H
/*----------------------------------------------------------------------------------------------*/
#include <vector>
#include <fstream>
#include "list.h"
using namespace ListH;
/*----------------------------------------------------------------------------------------------*/
// NOTE: placement of the enumeration values need to stay in this order to insure that
// the proper calculations are made for the salary
enum Emp_Title { TITLE_UNEMPLOYED, TITLE_WAITER, TITLE_CHEF, TITLE_MANAGER, TITLE_IRRELEVANT };
/*----------------------------------------------------------------------------------------------*/
// NOTE: we can not include "sim_main.h" because that file includes "restdata.h" and that file
// includes this file. so we must declare SimChair here. don't believe me, try it.
class SimChair;	// declared and defined in sim_main.*
class CRestaurant;
/*----------------------------------------------------------------------------------------------*/
// chef stats id (0-9)
#define PIZZAS_COOKED	0
// waiter stats id (10-19)
#define PIZZAS_ORDERED	10
#define PIZZAS_SERVED	11
// manager stats id (20-?)
#define STAFF_HIRE		20
#define STAFF_FIRE		22
#define INGRED_EMPTY	23
// statistic index
#define INDEX_STATS_TOTALS	0
#define INDEX_STATS_CURRENT	7

const short employee_stats_count = 8;
/*----------------------------------------------------------------------------------------------*/
class CChef
{
public: // construction/destruction
	CChef() {}
	~CChef() {}

	virtual void LoadFromFile(std::ifstream &infile) {
		infile.read((char*) &stats[0], sizeof(tagSTATS)*employee_stats_count);
	}
	virtual void SaveToFile(std::ofstream &outfile) {
		outfile.write((const char*) &stats[0], sizeof(tagSTATS)*employee_stats_count);
	}

public: // functions
	virtual bool ClearStatistics();
	virtual unsigned int GetStats(int id, int index);
	virtual bool SetStats(int id, unsigned int statistic, int index);
	virtual void ShiftStats();
	
private: // data
	struct tagSTATS {
		unsigned int pizzas_cooked;
	}stats[employee_stats_count];
};
/*----------------------------------------------------------------------------------------------*/
class CManager
{
public: // construction/destruction
	CManager() {}
	~CManager() {}

	virtual void LoadFromFile(std::ifstream &infile) {
		infile.read((char*) &stats[0], sizeof(tagSTATS)*employee_stats_count);
	}
	virtual void SaveToFile(std::ofstream &outfile) {
		outfile.write((const char*) &stats[0], sizeof(tagSTATS)*employee_stats_count);
	}

private: // data
	struct tagSTATS {
		
	}stats[employee_stats_count];
};
/*----------------------------------------------------------------------------------------------*/
class CWaiter
{
public: // construction/destruction
	CWaiter() {}
	~CWaiter() {}

	virtual void LoadFromFile(std::ifstream &infile) {
		infile.read((char*) &stats[0], sizeof(tagSTATS)*employee_stats_count);
	}
	virtual void SaveToFile(std::ofstream &outfile) {
		outfile.write((const char*) &stats[0], sizeof(tagSTATS)*employee_stats_count);
	}

public: // functions
	virtual bool ClearStatistics();
	virtual unsigned int GetStats(int id, int index);
	virtual bool SetStats(int id, unsigned int statistic, int index);
	virtual void ShiftStats();

	std::vector<SimChair>::iterator GetCurrentCustomer() { return m_CurrentCustomer; }
	void SetCurrentCustomer(std::vector<SimChair>::iterator customer) { m_CurrentCustomer = customer; }

private: // data
	std::vector<SimChair>::iterator m_CurrentCustomer;

	struct tagSTATS {
		unsigned int pizzas_served;
		unsigned int pizzas_ordered;
	}stats[employee_stats_count];
};
/*----------------------------------------------------------------------------------------------*/
// NOTE: Each employee has access to the data and functions of each job position.
// To ensure that he has 'appropriate access' to the functions is determined by
// the title which is held in the 'm_Title' member variable.
class CEmployee : public CWaiter, public CManager, public CChef
{
public: // construction/destruction
	CEmployee();
	~CEmployee() {}

public:	// file I/O functions
	void LoadFromFile(std::ifstream &infile);
	void SaveToFile(std::ofstream &outfile);

public: // data access
	short GetAge() { return m_Data.iAge; }
	void SetAge(short age) { m_Data.iAge = age; }

	int GetCompetency() { return m_Data.iCompetency; }
	void SetCompetency(int competency) { m_Data.iCompetency = competency; }

	unsigned int GetDaysEmployed() { return m_Data.iDays_employed; }
	void SetDaysEmployeed(unsigned int num_days) { m_Data.iDays_employed = num_days; }

	void GetDescription(char *buffer) { strcpy(buffer, m_Data.strDescription); }
	void SetDescription(char *desc) { strcpy(m_Data.strDescription, desc); }

	void GetName(char *buffer) { strcpy(buffer, m_Data.strName); }
	void SetName(char *name) { strcpy(m_Data.strName, name); }

	float GetSalary() { return m_Data.fSalary; }
	void SetSalary(float salary) { m_Data.fSalary = salary; }

	bool ClearStatistics();
	unsigned int GetStatistic(int id, int index = INDEX_STATS_CURRENT);
	bool SetStatistic(int id, unsigned int statistic, int index = INDEX_STATS_CURRENT);

	Emp_Title GetTitle() { return m_Data.Title; }
	void SetTitle(Emp_Title title) { m_Data.Title = title; }

	void ShiftStats();		// called at the end of each turn
public:
	CRestaurant *m_EmployedAt;

protected: // data
	// attributes
	struct {
		char strName[256];
		float fSalary;
		Emp_Title Title;
		short iAge;
		char strDescription[1024];
		int iCompetency;
		// personel stats
		unsigned int iDays_employed;
	} m_Data;
};
/*----------------------------------------------------------------------------------------------*/
#endif

