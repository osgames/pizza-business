#ifndef _JS_WND_CONTRACTS_H_
#define _JS_WND_CONTRACTS_H_
/*----------------------------------------------------------------------------------------------*/
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/stattext.h>
#include <wx/statline.h>
#include <wx/sizer.h>
#undef new
#include "datapool.h"
/*----------------------------------------------------------------------------------------------*/
class CContractsDlg : public wxDialog
{
public:
	CContractsDlg(wxWindow *parent, ContractManager *pContractManager/*CRestaurant *pRestaurant*/, CDataPool *myData);
	~CContractsDlg();

protected:
	void OnOkClicked(wxMouseEvent &e);
	void OnAddContractClicked(wxMouseEvent &e);
	void OnEditContractClicked(wxMouseEvent &e);
    void OnRemoveContractClicked(wxMouseEvent &e);

private:
	void RefreshContractsList();

private:
    //CRestaurant *m_pRestaurant;
    ContractManager *contractManager;
	wxButton *addContract, *editContract, *deleteContract, *ok;
	wxListCtrl *contractList;
	wxStaticText *currentContracts;
	wxStaticLine *line;
	wxBoxSizer *mainSizer, *middleBtnSizer;
	CDataPool *m_pData;
	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum
{
	CD_BUTTON_ADDCONTRACT = 13,
	CD_BUTTON_EDITCONTRACT,
	CD_BUTTON_DELETECONTRACT,
	CD_BUTTON_OK
};
/*----------------------------------------------------------------------------------------------*/
class CContractEditor : public wxDialog
{
public:
	CContractEditor(wxWindow *parent, Contract *contract, wxString dlgType, CDataPool *data);
	~CContractEditor();

protected:
	void OnOkClicked(wxMouseEvent &e);
	void OnCancelClicked(wxMouseEvent &e);
	void OnRadioButton(wxMouseEvent &e);

private:
	void RefreshComboBox();
	wxRadioButton *ingredients;
	wxRadioButton *advertisements;
	wxComboBox *contractItemSelector;
	wxTextCtrl *quantity, *frequency;
	wxStaticBox *radioBox;
	wxStaticText *selectItem, *enterQuantity, *buyThisItem, *days;
	wxButton *ok, *cancel;
	wxStaticBoxSizer *staticBoxSizer;
	wxBoxSizer *mainSizer, *middle1Sizer, *middle2Sizer, *bottomBtnSizer; 
	wxStaticLine *line;
	wxString myDlgType;
	Contract *currContract;
	CDataPool *m_pData;
	DECLARE_EVENT_TABLE()
};
/*----------------------------------------------------------------------------------------------*/
enum
{
	CE_BUTTON_OK = 56,
	CE_BUTTON_CANCEL,
	CE_RADIOBUTTON_ADS,
	CE_RADIOBUTTON_INGS
};
/*----------------------------------------------------------------------------------------------*/
#endif

