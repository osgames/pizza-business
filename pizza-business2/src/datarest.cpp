#include "datarest.h"
#include "pbengine.h"
using namespace std;
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	COwner implementation																		 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
COwner::COwner()
{
	this->SetName("");
	this->SetBudget(0.0f);

	m_Data.iRestaurantCount = 0;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int COwner::AddRestaurant(CRestaurant *pRestaurant)
{
	this->m_StoreList.AddItem(pRestaurant);

	// return the number of restaurants
	m_Data.iRestaurantCount = m_StoreList.GetCount();
	return m_Data.iRestaurantCount;
}
/*----------------------------------------------------------------------------------------------*/
CRestaurant* COwner::GetRestaurant(unsigned int index)
{
	return m_StoreList.GetItem(index);
}
/*----------------------------------------------------------------------------------------------*/
unsigned int COwner::GetRestaurantCount()
{
	m_Data.iRestaurantCount = m_StoreList.GetCount();
	return m_Data.iRestaurantCount;
}
/*----------------------------------------------------------------------------------------------*/
void COwner::LoadFromFile(std::ifstream &infile)
{
	// read the owner data from the file
	infile.read((char*) &this->m_Data, sizeof(m_Data));

	// read each restaurant from the file
	for(unsigned int c = 1 ; c <= m_Data.iRestaurantCount ; c++)
	{
		CRestaurant *pRestaurant = new CRestaurant(this);
		pRestaurant->LoadFromFile(infile);

		pRestaurant->InitIngredientList(&this->m_pGameEngine->m_DataPool.m_IngList);
		this->AddRestaurant(pRestaurant);
	}
}
/*----------------------------------------------------------------------------------------------*/
void COwner::SaveToFile(ofstream &outfile)
{
	this->GetRestaurantCount();				// updates the restaurant count in data structure

	// write the owner data to the file
	outfile.write((const char*) &this->m_Data, sizeof(m_Data));

	// write each restaurant to the file
	unsigned int rest_count = this->m_StoreList.GetCount();
	for(unsigned int c = 1 ; c <= rest_count ; c++)
	{
		CRestaurant *pRestaurant = m_StoreList.GetItem(c);
		pRestaurant->SaveToFile(outfile);
	}
}
/*-----------------------------------------------------------------------------------------------+
|																								 |
|	CRestaurant implementation																	 |
|																								 |
+-----------------------------------------------------------------------------------------------*/
CRestaurant::CRestaurant(COwner *pOwner)
{
	this->SetOwner(pOwner);
	this->SetName("");
	this->SetBudget(10000.0);
	this->m_Data.m_NumChairs = 0;
	this->Toppings_SetSelectable(false);
	this->SetAutoPricePercentage(5);
	m_Data.m_bEnableAutoPricing = false;

	// employees removed from list should not be deleted
	this->m_EmpList.SetDeleteFlag(false);

	// tables removed from the list should not be deleted
	this->m_TableList.SetDeleteFlag(false);

	// ovens removed from the list should not be deleted
	this->m_OvenList.SetDeleteFlag(false);

    // Setup the contract manager
    this->contractManager.SetRestaurant(this);

    // Initialize the expenses structure
    this->expenses.ads = 0;
    this->expenses.chairs = 0;
    this->expenses.ingredients = 0;
    this->expenses.ovens = 0;
    this->expenses.tables = 0;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::AddAdvertismentToList(CAdvertisement *pAd, unsigned int quantity)
{
	// find the ad in the list and add to the count
	for(unsigned int c = 1 ; c <= this->m_AdsList.GetCount() ; c++)
	{
		AdItem *pTestAd = this->m_AdsList.GetItem(c);
		if(pTestAd->pAd == pAd)
		{
			pTestAd->quantity += quantity;
			return;
		}
	}

	// if still in the function, then add a new AdItem to the list
	AdItem *pNewItem = new AdItem;
	pNewItem->pAd = pAd;
	pNewItem->quantity = quantity;
	this->m_AdsList.AddItem(pNewItem);
}
/*----------------------------------------------------------------------------------------------*/
Ingredient* CRestaurant::AddIngredientToList(CIngredient *pIng, unsigned int quantity)
{
	// find the category
	CIngCategory *pCat = pIng->GetCategory();
	unsigned int inglist_count = this->m_IngList.GetCount();
	for(unsigned int c = 1 ; c <= inglist_count ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);
		if(pIngCat->pCategory == pCat)
		{
			// now find the ingredient
			for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
			{
				Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
				
				if(pIngredient->pIngredient == pIng)
				{
					// create new IngItem and add them to the list
					for(unsigned int t = 0 ; t < quantity ; t++)
					{
						IngItem *pItem = new IngItem;
						pIngredient->IngList.AddItem(pItem);
						pItem->life_left = pIng->GetQuality();
					}
					return pIngredient;
				}
			}
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::AddPizzaToList(CPizza *pPizza)
{
	if(!pPizza) return;

	CPizzaObject *pObject = new CPizzaObject(pPizza);
	this->m_PizzaList.AddItem(pObject);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Advertisement_Buy(CAdvertisement *pAd, unsigned int quantity)
{
	// be sure the budget is enough
	double total_price = (double)pAd->GetPrice() * (double)quantity;
	if(this->GetBudget() < total_price)
		return;

	this->AddAdvertismentToList(pAd, quantity);
	this->SubractFromBudget(total_price);

    // Update the expenses object
    this->expenses.ads += total_price;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::BuyChair(int quantity, float price)
{
	// be sure the budget is enough
	if(this->GetBudget() < (double)price * (double)quantity)
		return;

	// be sure quanity is within the table support
	if(this->GetChairCount() + quantity > this->GetChairSupportCount())
		return;

	// add to the chair count
	m_Data.m_NumChairs += quantity;

	// subtract from budget
	this->SubractFromBudget((double)price * (double)quantity);

    // Update the expenses object
    this->expenses.chairs += (price * quantity);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::BuyOven(COven *pOven)
{
	// be sure budget is enough
	if(this->GetBudget() < (double)pOven->GetPrice())
		return;

	// add item to list
	this->m_OvenList.AddItem(pOven);

	// subtract from budget
	this->SubractFromBudget(pOven->GetPrice());

    // Update the expenses object
    this->expenses.ovens += pOven->GetPrice();
}
/*----------------------------------------------------------------------------------------------*/
bool CRestaurant::BuyPizza(CPizzaObject *pPizza)
{
	if(!pPizza) return false;

	// add the cost of the pizza to the budget
	this->AddToBudget(pPizza->GetPrice());
	// add the cost of the pizza to the budget
	this->AddToBudget(pPizza->GetPrice());
	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::BuyTable(CTable *pTable)
{
	// be sure budget is enough
	if(this->GetBudget() < (double)pTable->GetPrice())
		return;

	// add item to list
	this->m_TableList.AddItem(pTable);

	// subtract from budget
	this->SubractFromBudget(pTable->GetPrice());

    // Update the expenses object
    this->expenses.tables += pTable->GetPrice();
}
/*----------------------------------------------------------------------------------------------*/
bool CRestaurant::CookPizza(CPizzaObject *pPizza)
{
	if(this->IsPizzaSupported(pPizza) == false)
		return false;

	// use the ingredients
	for(unsigned int c = 1 ; c <= pPizza->GetPizza()->GetIngredientCount() ; c++)
	{
		CIngredient *pIngredient = pPizza->GetPizza()->GetIngredient(c);
		this->Ingredient_Use(pIngredient);
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::DecrementAds()
{
	// subtract 1 from each advertisement quantity
	// if quantity equals 0, then remove ad from the list
	for(unsigned int c = 1 ; c <= this->m_AdsList.GetCount() ; c++)
	{
		AdItem *pAd = this->m_AdsList.GetItem(c);
		pAd->quantity--;
	
		if(pAd->quantity < 1)
		{
			this->m_AdsList.DeleteItem(c);
			c = 1;	// reset the counter
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::DecrementIngLife()
{
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pCat = this->m_IngList.GetItem(c);

		for(unsigned int i = 1 ; i <= pCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIng = pCat->ingredients.GetItem(i);

			// decrement the life of ingredient item
			for(unsigned int u = 1 ; u <= pIng->IngList.GetCount() ; u++)
			{
				IngItem *pItem = pIng->IngList.GetItem(u);
				pItem->life_left--;

				// delete item from list if life is less than 1
				if(pItem->life_left < 1)
				{
					pIng->IngList.DeleteItem(u);
					u--;
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::DoEndTurnStuff()
{
	this->SubtractPayroll();									// payroll
	this->DecrementAds();										// advertisements
	this->DecrementIngLife();									// ingredient's lifespan
    // If a manager is hired, do contract stuff
    Emp_Title title = TITLE_MANAGER;
    List<CEmployee> list;
    this->GetEmployees(&list, title);
    if (list.GetCount() > 0)
        this->contractManager.DecrementContractCounters();
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::FireEmployee(CEmployee *pEmployee)
{
	if(pEmployee == 0) return;
	// be sure employee is employeed
	if(pEmployee->m_EmployedAt == 0)
		return;

	pEmployee->m_EmployedAt = 0;
	pEmployee->ClearStatistics();
	pEmployee->SetDaysEmployeed(1);
	pEmployee->SetTitle(TITLE_UNEMPLOYED);

	// find matching pointer int the restaurant's employee list
	for(unsigned int c = 1 ; c <= this->m_EmpList.GetCount() ; c++)
	{
		if(pEmployee == this->m_EmpList.GetItem(c))
		{
			// found the index; delete item
			this->m_EmpList.DeleteItem(c);
			break;
		}
	}

}
/*----------------------------------------------------------------------------------------------*/
double CRestaurant::GetCurrentPopularity()
{
	// if first turn, popularity equals 0
	int count = this->m_StatsList.GetCount();
	if(count > 0)
	{
		DayStats *pStats = this->m_StatsList.GetItem(1);
		if(pStats)
			return pStats->popularity;
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::GetEmployees(List<CEmployee> *pList, Emp_Title title)
{
	if(!pList) return;

	// be sure the delete flag is false for the list object
	pList->SetDeleteFlag(false);

	for(unsigned int c = 1 ; c <= this->m_EmpList.GetCount() ; c++)
	{
		CEmployee *pEmp = this->m_EmpList.GetItem(c);
		Emp_Title position = pEmp->GetTitle();
		
		if(title == TITLE_IRRELEVANT || title == position)
			pList->AddItem(pEmp);
	}
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CRestaurant::GetChairSupportCount()
{
	unsigned int count = 0;

	for(unsigned int c = 1 ; c <= this->m_TableList.GetCount() ; c++)
	{
		CTable *pTable = this->m_TableList.GetItem(c);

		count += pTable->GetChairsSupported();
	}

	return count;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CRestaurant::GetIngTypeCount(short id)
{
	// find the ingredient, return the count in inventory
	
	// 1st, find the category in the inventory
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);
		// 2nd, find the ingredient in thecategory
		for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
			
			// if the ingredient was found, return the count
			if(pIngredient->pIngredient->GetId() == id)
				return pIngredient->IngList.GetCount();
		}
	}

	return 0;
}
/*----------------------------------------------------------------------------------------------*/
unsigned int CRestaurant::GetTotalIngredientCount()
{
	unsigned int total_count = 0;

	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);

		for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
		{
			Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
			total_count += pIngredient->IngList.GetCount();
		}
	}

	return total_count;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::HireEmployee(CEmployee *pEmployee, Emp_Title title)
{
	if(pEmployee == 0) return;
	// be sure employee is unemployeed
	if(pEmployee->m_EmployedAt != 0)
		return;

	// if hiring a manager, be sure
	// there is not one already
	if(title == TITLE_MANAGER)
	{
		List<CEmployee> List;
		this->GetEmployees(&List, TITLE_MANAGER);
		if(List.GetCount() > 0)
			return;
	}

	// add employee to list; set employee data
	this->m_EmpList.AddItem(pEmployee);
	pEmployee->m_EmployedAt = this;
	pEmployee->SetTitle(title);
	
	// reset stats
	pEmployee->ClearStatistics();
	pEmployee->SetDaysEmployeed(0);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Ingredient_Buy(CIngredient *pIng, unsigned int quantity)
{
	if(pIng == 0)
		return;

	float expense = pIng->GetPrice() * (float)quantity;

	// be sure within budget
	if(this->GetBudget() >= expense)
	{
		this->AddIngredientToList(pIng, quantity);
		// sub from budget
		this->SubractFromBudget((double)expense);
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Ingredient_Sell(CIngredient *pIng, unsigned int quantity)
{
	if(!pIng) return;

	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pCat = this->m_IngList.GetItem(c);
		if(pCat->pCategory == pIng->GetCategory())
		{
			// find the ingredient, delete the quantity of the product,
			// and credit the restaurant's account
			for(unsigned int i = 1 ; i <= pCat->ingredients.GetCount() ; i++)
			{
				Ingredient *pIngredient = pCat->ingredients.GetItem(i);
				if(pIngredient->pIngredient == pIng)
				{
					// delete the quantity number of ingredients
					if(quantity > pIngredient->IngList.GetCount())
						quantity = pIngredient->IngList.GetCount();

					double money_recieved = 0.0f;
					// the ingredients are arranged from oldest to the newest
					// so just the delete the ones at the beginning
					for(unsigned int d = 1 ; d <= quantity ; d++)
					{
						IngItem *pItem = pIngredient->IngList.GetItem(1);
						
						// calculate the money that will be recieved from the transaction
						float percentage = ((float)pItem->life_left / (float)pIngredient->pIngredient->GetQuality());
						money_recieved += (double)percentage * (double)pIngredient->pIngredient->GetPrice();
							
						pIngredient->IngList.DeleteItem(1);
					}

					// credit the restaurant's account
					this->AddToBudget(money_recieved);

					return; // no need to continue with function
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::Ingredient_Use(CIngredient *pIng)
{
	if(pIng == 0) return;

	// find the ingredient
	// 1st, find the category in the inventory
	CIngCategory *pCat = pIng->GetCategory();
	for(unsigned int c = 1 ; c <= this->m_IngList.GetCount() ; c++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(c);
		if(pIngCat->pCategory == pCat)
		{
			// 2nd, find the ingredient in thecategory
			for(unsigned int i = 1 ; i <= pIngCat->ingredients.GetCount() ; i++)
			{
				Ingredient *pIngredient = pIngCat->ingredients.GetItem(i);
				
				// if the ingredient was found, pop the last item off the list
				// since they should be arrange by newest first
				if(pIngredient->pIngredient->GetId() == pIng->GetId())
				{
					unsigned int count = pIngredient->IngList.GetCount();
					pIngredient->IngList.DeleteItem(count);
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::InitIngredientList(List<CIngCategory> *pCatList)
{
	if(pCatList == 0) return;

	// create the category; add it to the list
	for(unsigned int c = 1 ; c <= pCatList->GetCount() ; c++)
	{
		CIngCategory *pCategory = pCatList->GetItem(c);

		IngCat *pNewCat = new IngCat;
		this->m_IngList.AddItem(pNewCat);
		pNewCat->pCategory = pCategory;
	
		for(unsigned int i = 1 ; i <= pCategory->IngList.GetCount() ; i++)
		{
			CIngredient *pIngredient = pCategory->IngList.GetItem(i);

			Ingredient *pNewIng = new Ingredient;
			pNewCat->ingredients.AddItem(pNewIng);
			pNewIng->pIngredient = pIngredient;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::InitPizzaList(List<CPizza> *pPizzaList)
{
	for(unsigned int c = 1 ; c <= pPizzaList->GetCount() ; c++)
		this->AddPizzaToList(pPizzaList->GetItem(c));
}
/*----------------------------------------------------------------------------------------------*/
bool CRestaurant::IsPizzaSupported(CPizzaObject *pPizza)
{
	// go through the ingredients necessary to create the pizza
	// and if the restaurant does not have a certain ingredient
	// in stock, return false
	// else return true

	for(unsigned int c = 1 ; c <= pPizza->GetPizza()->GetIngredientCount() ; c++)
	{
		CIngredient *pIngredient = pPizza->GetPizza()->GetIngredient(c);
		if(this->GetIngTypeCount(pIngredient->GetId()) == false)
			return false;
	}

	return true;
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::LoadFromFile(std::ifstream &infile)
{
	// initialize the ingredient list
	this->InitIngredientList(&this->m_pOwner->m_pGameEngine->m_DataPool.m_IngList);
	
	// clear the pizza list
	this->m_PizzaList.DeleteAllItems();

	// create an account at the bank
	// for the new restaurant
	this->m_pBankAccount = this->m_pOwner->m_pGameEngine->m_Bank.CreateAccount(this);


	// counter variable
	unsigned int c = 0;

    char tempBuffer[300];

	// read the restaurant data from the file
	infile.read((char*) &this->m_Data, sizeof(this->m_Data));

	// read the employees from the file
	for(c = 1 ; c <= m_Data.iEmployeeCount ; c++)
	{
		CEmployee *pEmployee = new CEmployee;
		this->HireEmployee(pEmployee, TITLE_CHEF);	// the job title is irrelevant at the moment

		pEmployee->LoadFromFile(infile);
	}

	// read the advertisements from the file
	for(c = 1 ; c <= m_Data.iAdsCount ; c++)
	{
		AdFileData ad_data;
		infile.read((char*)&ad_data, sizeof(AdFileData));

		CAdvertisement *pAd = this->m_pOwner->m_pGameEngine->m_DataPool.GetAdvertisementByID(ad_data.iId);
		this->AddAdvertismentToList(pAd, ad_data.quantity);
	}

	// read the ovens from the file
    COven *pOven, *tempOven;
	for(c = 1 ; c <= m_Data.iOvenCount ; c++)
	{
		pOven = new COven;
        pOven->ReadFromFile(infile);
		tempOven = this->GetOwner()->m_pGameEngine->m_DataPool.GetOvenByID(pOven->GetID());
        tempOven->GetName(tempBuffer);
    	pOven->SetName(tempBuffer);
        strcpy(tempBuffer, "");
        pOven->SetPrice(tempOven->GetPrice());
        this->m_OvenList.AddItem(pOven);
	}

	// read the tables from the file
    CTable *pTable, *tempTable;
	for(c = 1 ; c <= m_Data.iTableCount ; c++)
	{
		pTable = new CTable;
		pTable->ReadFromFile(infile);
        tempTable = this->GetOwner()->m_pGameEngine->m_DataPool.GetTableByID(pTable->GetID());
        tempTable->GetName(tempBuffer);
        pTable->SetName(tempBuffer);
        strcpy(tempBuffer, "");
        pTable->SetPrice(tempTable->GetPrice());
		this->m_TableList.AddItem(pTable);
	}

	// read the ingredients
	for(c = 1 ; c <= m_Data.iIngCount ; c++)
	{
		IngFileData ing_data;
		infile.read((char*) &ing_data, sizeof(IngFileData));

		CIngredient *pIng = this->m_pOwner->m_pGameEngine->m_DataPool.GetIngredientByID(ing_data.id);
		Ingredient *pIngredient = this->AddIngredientToList(pIng, 1);
		
		// the last one in the list is the one just place to the list
		IngItem *pItem = pIngredient->IngList.GetItem(pIngredient->IngList.GetCount());
		pItem->life_left = ing_data.life_left;
	}

	// read the list of pizzas fromt the file
	for(c = 1 ; c <= this->m_Data.iPizzaCount ; c++)
	{
		CPizzaObject *pPizzaObject = new CPizzaObject;
		pPizzaObject->LoadFromFile(infile, this->m_pOwner->m_pGameEngine->m_DataPool);
		this->m_PizzaList.AddItem(pPizzaObject);
	}

	// read the loan from the bank
	this->m_pBankAccount->ReadFromFile(infile, m_Data.iLoanCount);

	// read the daily statistics from the file
	this->m_StatsList.DeleteAllItems();
	for(c = 1 ; c <= m_Data.iStatCount ; c++)
	{
		DayStats *pNewStats = new DayStats;
		infile.read((char*) pNewStats, sizeof(DayStats));

		this->m_StatsList.AddItem(pNewStats);
	}

	// read the contracts from the file
	this->contractManager.LoadFromFile(infile, this->m_Data.iContractCount);

	// end of file
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::RemovePizzaFromList(CPizza *pPizza)
{
	unsigned int count = this->m_PizzaList.GetCount();
	for(unsigned int c = 1 ; c <= count ; c++)
	{
		CPizzaObject *pPizzaObject = this->m_PizzaList.GetItem(c);
		CPizza *pizza = pPizzaObject->GetPizza();
		if(pPizza == pizza)
		{
			this->m_PizzaList.DeleteItem(c);
			return;
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SaveToFile(std::ofstream &outfile)
{
	// counter variable
	unsigned int c = 0, count = 0;

	// update the reserved values in the data structure
	this->m_Data.iEmployeeCount = this->m_EmpList.GetCount();
	this->m_Data.iAdsCount = this->m_AdsList.GetCount();
	this->m_Data.iOvenCount = this->m_OvenList.GetCount();
	this->m_Data.iTableCount = this->m_TableList.GetCount();
	this->m_Data.iIngCount = this->GetTotalIngredientCount();
	this->m_Data.iLoanCount = this->m_pBankAccount->m_LoanList.GetCount();
	this->m_Data.iStatCount = this->m_StatsList.GetCount();
	this->m_Data.iPizzaCount = this->m_PizzaList.GetCount();
	this->m_Data.iContractCount = this->contractManager.GetContracts().size();

	// write the restaurant data to the file
	outfile.write((const char*) &this->m_Data, sizeof(this->m_Data));

	// write the employees to the file
	count = this->m_EmpList.GetCount();
	for(c = 1 ; c <= count ; c++)
	{
		CEmployee *pEmployee = m_EmpList.GetItem(c);
		pEmployee->SaveToFile(outfile);
	}

	// write the advertisements to the file
	// NOTE: advertisement data is handled differently
	//	     the advertisement object does not do its own saving
	count = this->m_AdsList.GetCount();
	for(c = 1 ; c <= count ; c++)
	{
		AdItem *pItem = m_AdsList.GetItem(c);
		
		AdFileData data;
		data.iId = pItem->pAd->GetId();
		data.quantity = pItem->quantity;

		outfile.write((const char*) &data, sizeof(AdFileData));
	}

	// write the ovens to the file
	count = this->m_OvenList.GetCount();
	for(c = 1 ; c <= count ; c++)
	{
		COven *pOven = m_OvenList.GetItem(c);
		pOven->SaveToFile(outfile);
	}

	// write the tables to the file
	count = this->m_TableList.GetCount();
	for(c = 1 ; c <= count ; c++)
	{
		CTable *pTable = m_TableList.GetItem(c);
		pTable->SaveToFile(outfile);
	}

	// write the ingredients to the file
	unsigned int cat_count = this->m_IngList.GetCount();
	for(unsigned int iCat = 1 ; iCat <= cat_count ; iCat++)
	{
		IngCat *pIngCat = this->m_IngList.GetItem(iCat);
		count = pIngCat->ingredients.GetCount();
		for(c = 1 ; c <= count ; c++)
		{
			Ingredient *pIng = pIngCat->ingredients.GetItem(c);
			
			for(unsigned int i = 1 ; i <= pIng->IngList.GetCount() ; i++)
			{
				IngItem* pItem = pIng->IngList.GetItem(i);

				IngFileData ing_data;
				ing_data.id = pIng->pIngredient->GetId();
				ing_data.life_left = pItem->life_left;

				outfile.write((const char*) &ing_data, sizeof(IngFileData));
			}
		}
	}

	// write the list of pizzas to the file
	for(c = 1 ; c <= this->m_Data.iPizzaCount ; c++)
	{
		CPizzaObject *pPizzaObject = this->m_PizzaList.GetItem(c);
		pPizzaObject->SaveToFile(outfile);
	}

	// write the list of loans from the bank
	this->m_pBankAccount->SaveToFile(outfile);

	// write the daily statistics to the file
	count = this->m_StatsList.GetCount();
	for(c = 1 ; c <= count ; c++)
	{
		DayStats *pStats = this->m_StatsList.GetItem(c);
		outfile.write((const char*) pStats, sizeof(DayStats));
	}

	// write the contracts to the file
	this->contractManager.SaveToFile(outfile);

	// end of file
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SellChair(int quantity, float fChairPrice)
{
	// delete quantity number of chairs

	unsigned int chair_count = this->GetChairCount();
	if(static_cast<unsigned int>(quantity) > chair_count)
		quantity = chair_count;

	// delete the chairs
	unsigned int chairs_left = chair_count - quantity;
	this->SetChairCount(chairs_left);

	// add 25% of the price of each chair to the budget
	double money_recieved = ((/*chair_count - */quantity) * fChairPrice) * 0.25;
	this->AddToBudget(money_recieved);
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SellOven(COven *pOven)
{
	if(!pOven) return;

	// find the oven
	for(unsigned int c = 1 ; c <= this->m_OvenList.GetCount() ; c++)
	{
		COven *pTestOven = this->m_OvenList.GetItem(c);
		if(pTestOven == pOven)
		{
			// delete item from the list
			this->m_OvenList.DeleteItem(c);
			// add 25% of the price to the budget
			this->AddToBudget(pOven->GetPrice() * 0.25);

			return;	// no need to continue with the function
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
void CRestaurant::SellTable(CTable *pTable)
{
	if(!pTable) return;

	for(unsigned int c = 1 ; c <= this->m_TableList.GetCount() ; c++)
	{
		CTable *pTestTable = this->m_TableList.GetItem(c);

		if(pTestTable == pTable)
		{
			// delete table from the list
			this->m_TableList.DeleteItem(c);
			// add 25% of the price to the budget
			this->AddToBudget(pTable->GetPrice() * 0.25);

			return;	// no need to continue with function
		}
	}
}
/*----------------------------------------------------------------------------------------------*/
// NOTE: this function is only called by the simulation after the player
// decides to end his turn.
void CRestaurant::SubtractPayroll()
{
	for(unsigned int c = 1 ; c <= this->m_EmpList.GetCount() ; c++)
	{
		CEmployee *pEmp = this->m_EmpList.GetItem(c);
		// increment the employees #days of employement
		pEmp->SetDaysEmployeed(1+pEmp->GetDaysEmployed());

		// subtract emplyee salary from budget
		float salary = pEmp->GetSalary();
		this->SubractFromBudget((double)salary);
	}
}
/*----------------------------------------------------------------------------------------------*/


